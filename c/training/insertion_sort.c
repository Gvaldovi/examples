/* Insertion sort. O(n^2) because the for loop and worst case the while loop. */
#include <stdio.h>
#include "stdint-gcc.h"

uint8_t n[] = {2,8,5,3,9,1};

int main (void)
{
    for (uint8_t i = 1;i < sizeof(n);i++)
    {
        // printf("i = %d\n",i);
        uint8_t j = i;
        while (j > 0 && n[j-1] > n[j])
        {
            uint8_t tmp = n[j];
            n[j] = n[j-1];
            n[j-1] = tmp;
            j--;

            printf("n = ");
            for (uint8_t k = 0;k < sizeof(n);k++)
                printf("%d ",n[k]);
            printf("\n");
        }
    }
    return 0;
}