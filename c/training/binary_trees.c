/* 1.- Given a binary tree print its height, its greater path, its left side view and its top-bottom level view.
   2.- Print preOrder, postOrder and inOrder.
   3.- Print the same as 2 but iterative */
#include <stdio.h>
#include <stdlib.h>
#include "stdint-gcc.h"

struct node 
{
    uint8_t         data;
    struct node*    right;
    struct node*    left;
};

struct node* newNode(uint8_t data)
{
    /* Create a new node n */
    struct node* n = (struct node*)malloc(sizeof(struct node));
    /* Assign data and initialize left and right */
    n->data = data;
    n->left = NULL;
    n->right = NULL;
    return n;
}

uint8_t getHeight(struct node* n)
{
    if (n == NULL)
        return 0;

    uint8_t leftSize = getHeight(n->left);
    uint8_t rightSize = getHeight(n->right);

    if (leftSize > rightSize)
        return ++leftSize;
    else
        return ++rightSize;
}

uint8_t* getGreaterPath(struct node* n, uint8_t *idx, uint8_t height)
{
    if (n == NULL)
    {
        /* We are in the bottom of a branch. Create a buffer to start filling it. */
        uint8_t *h = (uint8_t*)malloc(height);
        /* Start index will be 0 */
        *idx = 0;
        /* Return the newly created buffer */
        return h;
    }

    /* Create local indexes for buffers right and left */
    uint8_t leftIdx=0, rightIdx=0;
    uint8_t *leftBuffer = getGreaterPath(n->left,&leftIdx,height);
    uint8_t *rightBuffer = getGreaterPath(n->right,&rightIdx,height);

    if (leftIdx > rightIdx)
    {   /* Left index is greater than right */
        free(rightBuffer);                  // Delete right buffer
        leftBuffer[leftIdx++] = n->data;    // Copy node's data to left buffer and increment left index
        *idx = leftIdx;                     // Copy left index to idx
        return leftBuffer;                  // return left buffer
    }
    else
    {
        /* Right index is greater or equal to left. Do the same as other condition */
        free(leftBuffer);
        rightBuffer[rightIdx++] = n->data;
        *idx = rightIdx;
        return rightBuffer;
    }
}

void printLeftSide(struct node *n,uint8_t *f,uint8_t l)
{
    if (n == NULL)
        return;

    if (*f < l)
    {
        /* This is the first node to print by level */
        printf("%d, ",n->data);
        *f = l;
    }

    printLeftSide(n->left, f, l+1);
    printLeftSide(n->right, f, l+1);
}

void printLevel(struct node *n, uint8_t l)
{
    if (n == NULL)
        return;

    if (l == 1)
        printf("%d, ",n->data); // Print the current node
    else
    {
        /* Go down into the tree to find the correct level to print */
        printLevel(n->left, l-1);
        printLevel(n->right, l-1);
    }
}

#define PRE_ORDER       0u
#define POST_ORDER      1u
#define IN_ORDER        2u
void printOrder(struct node* n, uint8_t order)
{
    if (n == NULL)
        return;

    
    if (order == PRE_ORDER)
    {
        /* PreOrder is NLR */
        printf("%d, ",n->data);
        printOrder(n->left, order);
        printOrder(n->right, order);
    }
    else if (order == POST_ORDER)
    {
        /* PostOrder is LRN */
        printOrder(n->left, order);
        printOrder(n->right, order);
        printf("%d, ",n->data);
    }
    else // IN_ORDER
    {
        /* InOrder is LNR */
        printOrder(n->left, order);
        printf("%d, ",n->data);
        printOrder(n->right, order);
    }
}

int main(void)
{
    struct node* root = newNode(10);
    printf("Root data: %d\n", root->data);
    /* Add more leafs in the following way */
    //      10
    //  7       14
    //    9   11
    //          12
    root->left = newNode(7);
    root->right = newNode(14);
    root->left->right = newNode(9);
    root->right->left = newNode(11);
    root->right->left->right = newNode(12);

    /***************
     * 1.- Given a binary tree print its height, its greater path, its left side view and its top-bottom level view.
     **************/
    printf("*** SECTION 1\n");

    /* Print its height */
    printf("Height of tree: %d\n",getHeight(root));

    /* Print its greater path */
    uint8_t height = getHeight(root); // Get the height of the tree. This is for allocating memory no more than this height.
    uint8_t idx = 0;    // Create an index for the Path buffer
    uint8_t *path = getGreaterPath(root,&idx,height);
    printf("Greater path of tree: ");
    for (uint8_t i = 0;i < idx;++i)
    {
        printf("%d, ",path[i]);
    }
    printf("\n");
    free(path);

    /* Print left side view */
    uint8_t flag = 0,level = 1; // Create a flag to print only the first node and a level to go down to every level.
    printf("Printing left side view: ");
    /* Flag must be sent as reference because it will be shared by all the nodes in the same level */
    printLeftSide(root,&flag,level);
    printf("\n");

    /* Print the top-bottom level view */
    printf("Print top-bottom level view: ");
    for (level = 1;level <= height;++level)
    {
        /* Print level by level */
        printLevel(root,level);
    }
    printf("\n");

    /***************
     * 2.- Print preOrder, postOrder and inOrder.
     **************/
    printf("\n*** SECTION 2\n");
    printf("Print PreOrder: ");
    printOrder(root, PRE_ORDER);
    printf("\n");

    printf("Print PostOrder: ");
    printOrder(root, POST_ORDER);
    printf("\n");

       printf("Print InOrder: ");
    printOrder(root, IN_ORDER);
    printf("\n");
    return 0;
}