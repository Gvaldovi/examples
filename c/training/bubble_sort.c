/* Bubble sort. O(n^2) worst case. It can be improved breaking the main for loop when no swap. */
#include <stdio.h>
#include "stdint-gcc.h"

uint8_t n[] = {2,8,5,3,9,1};

int main (void)
{
    for (uint8_t i = 0;i < sizeof(n)-1;++i)
    {
        uint8_t b = 0;
        for (uint8_t j = 0;j < sizeof(n)-i-1;++j)
        {
            if (n[j] > n[j+1])
            {
                uint8_t tmp = n[j];
                n[j] = n[j+1];
                n[j+1] = tmp;
                b = 1;
            }
        }

        printf("n = ");
        for (uint8_t k = 0;k < sizeof(n);k++)
            printf("%d ",n[k]);
        printf("\n");
        if(b == 0)
            break;
    }
    return 0;
}