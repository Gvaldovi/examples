In order to do unit test with cpputest follow the next steps:
- Clone cpputest repository one level above this project: git clone https://github.com/cpputest/cpputest.git
- cd cpputest
- Execute the following:
$ cd cpputest
$ autoreconf . -i
$ ./configure
$ make tdd
$ export CPPUTEST_HOME=$(pwd).
- cd cpputest_test/t: open the Makefile and write the right paths. There is no need to uncomment the variable CPPUTEST_HOME because it was exported in previous step.
- The file MakefileWorker.mk is a helper that only needs some variables to work, for example where are the src and includes. Otherwise you should create a Makefile for testing that compiles the project and its testing files.
- cd ..
- make main: compile only the main project
- make all: compile the tests and src files, then run the tests
