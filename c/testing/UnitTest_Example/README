
UnitTest_Example contains a running example of how to test a C module using 
CppUTest.

The module contains a single function, bin2bcd(), that receives a uint16_t
value from 0 to 9999 and computes the 4 digits that represent this value, 
stored in a uint8_t array.

The folder structure contains two folders:

	UnitTest_Example
	|-- UnitTest_Program
	`-- UnitTest_Tester

UnitTest_Program should contain the module to be tested (.c and .o) and a main
program that uses the module (if the .o file is absent, the make utility
should be able to create the file). UnitTest_Tester contains the code for tests, 
that verify the correctnes of the module (stored in UnitTest_Program).

Both folders contain a makefile to compile and run each program. Both makefiles
accept the 'run' argument, that runs the example program in UnitTest_Program and
the tests, in UnitTest_Tester.

The c file with the unit tests contains a final test (disabled with a #if 0
preprocessor directive) that will fail with the distributed version of the
conversion module. This test verifies the return value when bin2bcd() is called
with an invalid value. That situation is not yet implemented in the code. This
would correspond to a newly created test, which should fail when it is added
to the unit test files. The developer must then add the necessary code for
the test to pass. 

Developed by
Pedro Fonseca
pf@ua.pt

