/*!
 * \file conversion.c
 *
 * \brief routines for 7-segment display
 * \author pf@ua.pt
 *
 * Routines for 7-segment display.
 * Course: Embedded Systems Programming
 * University of Aveiro
 * 2012
 */

#include "conversion.h"

int8_t bin2bcd(uint16_t val, uint8_t *Result)
{
	int i=0;
	int8_t returnVal = 0;  /* default value */

	while(val > 0){
		Result[i] = val - (val/10)*10;
		val = val/10;
		i++;
	}

	return returnVal; // TODO: compute return value
}
