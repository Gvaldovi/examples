/*!
 * \file conversion.h
 *
 * \brief routines for 7-segment display
 * \author pf@ua.pt
 *
 * Routines for 7-segment display.
 * Course: Embedded Systems Programming
 * University of Aveiro, 2012
 *
 * \note The routines in this file may contain errors!...
 *
 */

#ifndef CONVERSION_H_
#define CONVERSION_H_

#include <stdint.h>
/*!
 * \fn bin2bcd(uint16_t val, uint8_t *Result)
 *
 * \brief Binary to BCD conversion
 *
 * \arg val	Value to be converted
 * \arg Result Array with resulting digits (one binary coded digit per array position)
 *
 * \return Code for conversion result (0 if SUCCESS, -1 if FAIL)
 */

int8_t bin2bcd(uint16_t val, uint8_t *Result);

#endif /* CONVERSION_H_ */
