pub fn run() {
    let mut count = 1;

    // Infinite loop
    loop {
        count += 1;
        println!("Count: {}", count);

        if count >= 5 {
            break;
        }
    }

    // While loop (FizzBuzz)
    count = 1;
    while count <= 10 {
        if (count % 3 == 0) && (count % 5 == 0) {
            println!("FizzBuzz");
        }
        else if count % 3 == 0 {
            println!("Fizz");
        }
        else if count % 5 == 0 {
            println!("Buzz");
        }
        else {
            println!("{}", count);
        }
        count += 1;
    }

    // for loop
    for x in 0..5 {
        println!("Number: {}",x);
    }
}