pub fn run() {
    // Print to console
    println!("Hello from print.rs file. ");

    // Positional arguments
    println!("{} is {} years old", "Yari", 6);

    // Reuse
    println!("{0} and {1} are siblings, {0} is {2} years old.","Mane", "Yari", 5);

    // Formatting
    println!("b{0:b} is binary, 0x{0:x} is hex and 0o{0:o} is octal.",255);
}