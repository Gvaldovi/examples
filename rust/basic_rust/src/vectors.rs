
use std::mem;

pub fn run() {
    let mut numbers: Vec<i32> = vec![1,2,3,4,5];

    // Print all array
    println!("Original vector: {:?}", numbers);

    // Re-assign value
    numbers[2] = 56;

    // Add on to vector
    numbers.push(5);
    numbers.push(6);

    // Pop off last value
    numbers.pop();

    // Print all array
    println!("Modified vector: {:?}", numbers);

    // Get single val
    println!("Single value: {}", numbers[2]);

    // Get array lenght
    println!("Vector lenght: {}", numbers.len());

    // Vector are stack allocates
    println!("Vector occupies {} bytes", mem::size_of_val(&numbers));

    // Get slice
    let slice: &[i32] = &numbers[2..5];
    println!("Slice: {:?}", slice);

    // Loop through vector values
    for x in numbers.iter() {
        println!("Number: {}", x);
    }

    // Loop & mutate
    for x in numbers.iter_mut() {
        *x *= 3;
    }
    println!("Numbers Vec: {:?}", numbers);
 }