/* Primitive str = Immutable fixed-lenght string somewhere in memory */
/* String = Growable, heap-allocated data structure - Use when you need to modify or own string data */

pub fn run() {
    let hello = "Hello";
    let mut world = String::from("World ");

    // Get lenght. Works for either type
    println!("Lenght: {}", hello.len());

    // Push for one char. Only for String type
    world.push('G');
    world.push_str("valdovi");

    // Capacity in bytes
    println!("Capacity: {}", world.capacity());

    // Empty?
    println!("Is empty: {}", world.is_empty());

    // Contains sub-string?
    println!("Contains 'dovi': {}", world.contains("dovi"));

    // Replace
    println!("Replace: {}", world.replace("World", "Mundo"));

    // Loop through string by whitespace
    for word in world.split_whitespace() {
        println!("{}", word);
    }

    // Create string with capacity
    let mut s = String::with_capacity(10);
    s.push('a');
    s.push('b');
    println!("{}", s);
    // Assertion testing
    assert_eq!(2, s.len());
    assert_eq!(10, s.capacity());

    println!("{} {}", hello, world);
}