// Used to create custom data types

// Standard struct
struct Color {
    red: u8,
    green: u8,
    blue: u8,
}

// Tuple struct
struct Ids(u8, u8, u8);

// Class struct
struct Person {
    first_name: String,
    last_name: String,
}

impl Person {
    // Create a new Person
    fn new(first: &str, last: &str) -> Person {
        Person {
            first_name: first.to_string(),
            last_name: last.to_string(),
        }
    }

    // Get full name
    fn full_name(&self) -> String {
        format!("My name is {} {}", self.first_name, self.last_name)
    }

    // Set last name
    fn set_last_name(&mut self, last: &str) {
        self.last_name = last.to_string();
    }

    // Return a tuple
    fn tuple(self) -> (String, String) {
        (self.first_name, self.last_name)
    }
}

pub fn run() {
    // Standard struct
    let mut c = Color {
        red: 255,
        green: 0,
        blue: 0,
    };
    c.green = 200;
    println!("Color: {} {} {}", c.red, c.green, c.blue);

    // Tuple struct
    let mut n = Ids(23, 45, 67);
    n.2 = 12;
    println!("Ids: {} {} {}", n.0, n.1, n.2);

    // Class struct
    let mut p = Person::new("Gerardo", "Valdovinos");
    println!("New person: {} {}", p.first_name, p.last_name);
    println!("Get full name: {}", p.full_name());
    p.set_last_name("Villalobos");
    println!("Get full name: {}", p.full_name());
    println!("Person Tuple: {:?}", p.tuple());
}