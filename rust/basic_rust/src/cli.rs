use std::env;

pub fn run () {
    // The first string is the path of the executable. following strings separated by space are arguments.
    // For example: cargo run cmd1 cmd2
    let args: Vec<String> = env::args().collect();
    println!("Args: {:?}", args);

    let command = args[1].clone();
    println!("Command: {}", command);
}