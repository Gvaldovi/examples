/* Convension for variable names in Rust is snake_var (not CamelCase) */

pub fn run() {
    let name = "Gera";
    let mut age = 38;
    println!("{} is {}",name,age);
    age = 39;
    println!("This year {} will be {}",name,age);

    // Const 
    const ID: i32 = 001;
    println!("My ID is {}",ID);

    // Assign multiple variables
    let (my_name, my_age) = ("Pau", 25);
    println!("Hello my name is {}, and I'm {} years.", my_name, my_age);
}