// Arrays - Fixed list where elements are the same data types

use std::mem;

pub fn run() {
    let mut numbers: [i32; 5] = [1,2,3,4,5];

    // Print all array
    println!("{:?}", numbers);

    // Re-assign value
    numbers[2] = 56;

    // Get single val
    println!("Single value: {}", numbers[2]);

    // Get array lenght
    println!("Array lenght: {}", numbers.len());

    // Array are stack allocates
    println!("Array occupies {} bytes", mem::size_of_val(&numbers));

    // Get slice
    let slice: &[i32] = &numbers[2..5];
    println!("Slice: {:?}", slice);
}