// Conditionals, used to check a condition

pub fn run() {
    let age = 18;
    let check_id: bool = false;

    // if/else
    if age >= 15 && check_id == true{
        println!("Is older than 15");
    }
    else {
        println!("Is younger");
    }

    // Short hand if
    let kid = if age < 18 {true} else {false};
    println!("Is that person a kid?, {}", kid);
}