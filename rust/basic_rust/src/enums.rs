// Enums are types which have a definite value

enum Movement {
    Up,
    Down,
    Left,
    Right
}

pub fn run() {
    let avatar1 = Movement::Up;
    let avatar2 = Movement::Down;
    let avatar3 = Movement::Left;
    let avatar4 = Movement::Right;

    avatar_move(avatar1);
    avatar_move(avatar2);
    avatar_move(avatar3);
    avatar_move(avatar4);
}

fn avatar_move(m: Movement) {
    // match is like switch/case
    match m {
        Movement::Up => println!("Avatar moving up"),
        Movement::Down => println!("Avatar moving down"),
        Movement::Left => println!("Avatar moving left"),
        Movement::Right => println!("Avatar moving right")
    }
}