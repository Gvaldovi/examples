pub fn run() {
    greeting("Hello", "Gerardo");

    let sum = add(5,6);
    println!("Sum: {}", sum);

    // Closure
    let n3 = 10;
    let sum_closure = |n1: i32, n2:i32|n1 + n2 + n3;
    println!("Sum closure: {}", sum_closure(1, 2));
}

fn greeting(greet: &str, name: &str) {
    println!("{} {}, nice to meet you", greet, name);
}

fn add(a: i32, b: i32) -> i32 {
    a + b
}