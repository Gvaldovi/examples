/* This main file will call other feature files.
 * cargo build: build the project.
 * cargo run: build and run the executable. */

// mod print;
// mod var;
// mod types;
// mod strings;
// mod tuples;
// mod arrays;
// mod vectors;
// mod conditionals;
// mod loops;
// mod function;
// mod pointer_ref;
// mod structs;
// mod enums;
mod cli;

fn main() {
    // Functions
    // println!("Hello, world!");
    function();
    
    // print::run();
    // var::run();
    // types::run();
    // strings::run();
    // tuples::run();
    // arrays::run();
    // vectors::run();
    // conditionals::run();
    // loops::run();
    // function::run();
    // pointer_ref::run();
    // structs::run();
    // enums::run();
    cli::run();
}

fn function() {
    println!("Hola desde la funcion no declarada antes");
}
