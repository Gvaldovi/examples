use std::env;
// use std::num::Wrapping;
// use std::i64;
// use hex::FromHex;

fn main () {
    // The first string is the path of the executable. following strings separated by space are arguments.
    // For example: cargo run cmd1 cmd2
    let args: Vec<String> = env::args().collect();
    println!("Args: {:?}", args);

    if args[1] == "b5" {
        // TEST b5 62 06 04 04 00 FF 87 00 00    94 F5
        println!("Checksum for UBX");
        let cmd: &[String] = &args[3..];
        // println!("UBX: {:?}", cmd);
        
        let mut ck_a: u8 = 0;
        let mut ck_b: u8 = 0;
        
        for n in cmd.iter() {
            let x = u8::from_str_radix(&n, 16);
            let y = x.unwrap();
            // println!("{:02x}", y);
            
            /* Needs overflow. Solution make u32 instead of u8 */
            // ck_a = (ck_a + y);
            // ck_b = (ck_b + ck_a);
            /* Wrapping add. Not very readable */
            ck_a = ck_a.wrapping_add(y);
            ck_b = ck_b.wrapping_add(ck_a);
            /* Use #[wrappit] on a function */
        }
        println!("UBX packet checksum: {:02X}, {:02X}", ck_a, ck_b);
    } else if args[1].contains("$") {
        println!("Checksum for NMEA");
    }
    // let command = args[1].clone();
    // println!("Command: {}", command);
}
