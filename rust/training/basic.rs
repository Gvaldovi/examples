// Compile it with: rustc basic.rs. It will generate the executable "basic".
// The right way of create/compile a file is with cargo new, cargo build and cargo run. Cargo will generate
// also the project structure with Cargo.lock, Cargo.toml and target folder.
fn function() {
    println!("Hola desde la funcion");
}

fn main() {
    println!("Hello, world!");
    function();
}
