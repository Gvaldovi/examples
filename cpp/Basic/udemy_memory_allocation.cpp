#include <iostream>

int main(void) {
    using namespace std;

    /* Allocating memory for a variable using new */
    // int *p = new int(23);
    // cout << *p << endl;
    // *p = 45;
    // cout << *p << endl;
    // delete p;

    /* New for an array. For loop initialization */
    // int *p = new int[5];
    // for (int i=0;i<5;i++) {
    //     p[i] = i;
    //     cout << p[i] << endl;
    // }
    // delete []p;

    /* New for an array. Uniform initialization */
    // int *p = new int[4]{4,3,2,1};
    // cout << p[1] << endl;
    // delete []p;
    
    /* 2D arrays */
    // without memory allocation
    // int matrix[3][3]{
    //     1,2,3,
    //     4,5,6,
    //     7,8,9
    // };
    // cout << matrix[2][1] << endl;
    // for (int i=0;i<3;i++) {
    //     for (int x : matrix[i]) {
    //         cout << x << ", ";
    //     }
    //     cout << endl;
    // }
    // With memory allocation
    int *p1 = new int[3]{1,2,3};
    int *p2 = new int[3]{4,5,6};
    int *p3 = new int[3]{7,8,9};
    int **pMatrix = new int*[3];
    pMatrix[0] = p1;
    pMatrix[1] = p2;
    pMatrix[2] = p3;
    // Print a value and the complete matrix
    cout << pMatrix[2][1] << endl;
    for (int i=0;i<3;i++) {
        for (int j=0;j<3;j++) {
            cout << pMatrix[i][j] << ", ";
        }
        cout << endl;
    }
    // Free memory
    delete []p1;
    delete []p2;
    delete []pMatrix;


    return 0;
}