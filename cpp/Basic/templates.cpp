#include <iostream>

/* A template is for allowing a function to accept arguments of different type
This without the necessity of create several functions for overloading */
template<typename T>
T Max(T x, T y) {
    return x > y ? x : y;
}

template<typename T, int size>
class Stack {
    T m_Buffer[size];
    int m_Top{-1};
public:
    void Push(const T &elem) {
        m_Buffer[++m_Top] = elem;
    }
    void Pop() {
        --m_Top;
    }
    const T& Top()const {
        return m_Buffer[m_Top];
    }
    bool IsEmpty() {
        return m_Top == -1;
    }
};

int main(void) {
    /* Template basics */
    // auto num = Max(3.3f, 5.6f);
    // std::cout << num << std::endl;
    // auto num2 = Max(8,9);
    // std::cout << num2 << std::endl;

    /* Class template */
    Stack<float, 10> s;
    s.Push(3);
    s.Push(6);
    s.Push(8);
    s.Push(4.6);
    s.Push(2);
    while (!s.IsEmpty()) {
        std::cout << s.Top() << " ";
        s.Pop(); 
    }
    
    return 0;
}