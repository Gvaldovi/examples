#include <iostream>

class Account {
private:
    /* data */
    std::string m_Name;
    int m_AccNo;
    static int s_ANGenerator;
protected:
    float m_Balance; // This variable is protected to allow access from child classes
public:
    /* List initialization */
    Account(const std::string &name, float balance):m_Name(name), m_Balance(balance) {
        /* Constructor */
        m_AccNo = ++s_ANGenerator;
    }
    ~Account() {/* Destructor */}
    const std::string GetName() const {
        return m_Name;
    }
    float GetBalance() const {
        return m_Balance;
    }
    int GetAccNo() const {
        return m_AccNo;
    }
    /* Virtual keyword is used to tell the compiler that the method to be used is from child not from base */
    virtual void AccumulateInterest() { }
    virtual void Witdraw(float amount) {
        if (amount < m_Balance) {
            m_Balance -= amount;
        } else {
            std::cout << "Insufficient Balance" << std::endl;
        }
    }
    void Deposit(float amount) {
        m_Balance += amount;
    }
    virtual float GetInterestRate() const {
        return 0.0f;
    }
};

/* Initialization of the s_ANGenerator */
int Account::s_ANGenerator = 1000;

/* Inheritance from Account into Savings */
class Savings : public Account {
private:
    float m_Rate;
public:
    /* List initialization of base class and m_Rate */
    Savings(const std::string &name, float balance,float rate):Account(name,balance),m_Rate(rate) {
        /* Parametrized constructor */
    }
    ~Savings() {/* Destructor */} 

    /* Functions implemented inside the class */
    float GetInterestRate()const {
        return m_Rate;
    }
    void AccumulateInterest() {
        m_Balance += (m_Balance * m_Rate);
        // std::cout << "Acc Interest from child class" << std::endl;
    }
};

/* Inheritance from Account into Checking */
class Checking : public Account {
private:
    /* data */
public:
    /* Run directly the base constructor. This is useful if child constructor only initializes base constructor */
    using Account::Account;
    /* Run base constructor from child constructor through list initialization */
    // Checking(const std::string &name, float balance):Account(name,balance) {
    //     /* Constructor. It is not initializing anything but the parent constructor */
    // }
    ~Checking() {/* Destructor */}
    void Witdraw(float amount) {
        if((m_Balance - amount) >= 50) {
            Account::Witdraw(amount); // Execution of base class to withdraw
        } else {
            std::cout << "Insufficient founds to withdraw" << std::endl;
        }
    }
    void Check() {
        std::cout << "Method only on Cheking class" << std::endl;
    }
};


/* A Base class can point to all its child class, but it needs to be a reference or a pointer */
void Transact(Account *pAcc) { // pAcc can be a ponter to a Savings or Checking object
    std::cout << "Transaction started" << std::endl;
    std::cout << "Initial balance: " << pAcc->GetBalance() << std::endl;
    pAcc->Deposit(100);
    pAcc->AccumulateInterest();
    pAcc->Witdraw(170);

    /* Only if the child object is Checking */
    Checking *pChe = dynamic_cast<Checking*>(pAcc);
    if(pChe != nullptr) {
        pChe->Check();
    }

    std::cout << "Interest rate: " << pAcc->GetInterestRate() << std::endl;
    std::cout << "Final balance: " << pAcc->GetBalance() << std::endl;
}

int main(void) {
    /* Testing inheritance */
    // Checking acc("Bob",100);
    // std::cout << "Initial balance " << acc.GetBalance() << std::endl;
    // acc.Deposit(200);
    // acc.Witdraw(4000);
    // std::cout << "Balance " << acc.GetBalance() << std::endl;

    /* Testing capabilities on pointers to objects */
    Checking acc("Bob",100);
    Transact(&acc); // Transact receives a base pointer and from it can reach childs methods

    return 0;
}