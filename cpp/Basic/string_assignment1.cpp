#include <iostream>
#include <string>

std::string ToUpper(const std::string &str) {
    std::string r;
    for (auto x : str) {
        r += toupper(x);
    }
    return r;
}

std::string ToLower(const std::string &str) {
    std::string r;
    for (auto x : str) {
        r += tolower(x);
    }
    return r;
}

enum class Case{SENSITIVE, INSENSITIVE};
 
size_t Find(
    const std::string &source,         //Source string to be searched
    const std::string &search_string,  //The string to search for
    Case searchCase = Case::INSENSITIVE,//Choose case sensitive/insensitive search
    size_t offset = 0 ) {                //Start the search from this offset
        //Implementation
        size_t idx = 0;
        if (searchCase == Case::SENSITIVE) {
            idx=source.find(search_string,offset);
            if (idx > search_string.length()) {
                idx = source.npos;
            }
        } else {
            std::string sourcelow = ToLower(source);
            std::string searchlow = ToLower(search_string);
            idx=sourcelow.find(searchlow,offset);
            if (idx > searchlow.length()) {
                idx = sourcelow.npos;
            }
        }
        return idx;
        /*
        return position of the first character 
        of the substring, else std::string::npos
        */
}

int main (void) {
    // Assignment 1
    // std::string s1 = "hola mundo";

    // std::string sUpper = ToUpper(s1);
    // std::cout << sUpper << std::endl;

    // std::string sLower = ToLower(sUpper);
    // std::cout << sLower << std::endl;

    // Assignment 2
    std::string s1{"abcdefg"};
    std::string s2{"CDE"};
    std::cout << "Find s2 in s1 at: " << Find(s1,s2,Case::INSENSITIVE,0) << std::endl;

    return 0;
}

