#include <iostream>

void Add(int a,int b, int &result) ;    //Add two numbers and return the result through a reference parameter
void Factorial(int a, int &result) ;    //Find factorial of a number and return that through a reference parameter
void Swap(int &a, int &b) ;            //Swap two numbers through reference arguments

int main() {
    using namespace std;

    int x{4};
    int y{5};
    int z{};
    
    Add(x,y,z);
    cout << "Add(x,y,z): " << z << endl;
    Factorial(x,z);
    cout << "Factorial(x,z): " << z << endl;
    Swap(x,y);
    cout << "Swap(x,y): x=" << x << ", y=" << y << endl;


    return 0;
}

void Add(int a,int b, int &result) {
    result = a + b;
}

void Factorial(int a, int &result) {
    result = 1;
    for (int i = 1;i <= a;i++) {
        result *= i;
    }
}

void Swap(int &a, int &b) {
    int tmp = a;
    a = b;
    b = tmp;
}