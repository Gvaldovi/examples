// Implement the following functions using pointer arguments only
#include <iostream>

int Add(int *a, int *b) ;    //Add two numbers and return the sum
void AddVal(int *a, int *b, int *result); //Add two numbers and return the sum through the third pointer argument
void Swap(int *a, int *b) ;  //Swap the value of two integers
void Factorial(int *a, int *result);       //Generate the factorial of a number and return that through the second pointer argument

int main() {
    using namespace std;

    int a{3};
    int b{5};
    int r{};
    cout << "Add(&a,&b): " << Add(&a,&b) << endl;
    AddVal(&a,&b,&r);
    cout << "AddVal(&a,&b,&r): " << r << endl;
    Swap(&a,&b);
    cout << "Swap(&a,&b): a=" << a << ", b=" << b << endl;
    Factorial(&b,&r);
    cout << "Factorial(&b,&r): " << r << endl;
    return 0;
}

int Add(int *a, int *b) {
    return *a + *b;
}

void AddVal(int *a, int *b, int *result) {
    *result = *a + *b;
}

void Swap(int *a, int *b) {
    int tmp = *a;
    *a = *b;
    *b = tmp;
}

void Factorial(int *a, int *result) {
    int tmp{1};

    for (int i = 1;i <= *a;i++) {
        tmp *= i;
    }

    *result = tmp;
}