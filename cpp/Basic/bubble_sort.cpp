#include <iostream>

int main(void) {
    using namespace std;

    // int l[]{5,6,2,1,4,7,3};
    int l[]{7,6,2,3,4,5,1};
    int lSize = sizeof(l)/sizeof(l[0]);
    
    /* Use of one while loop and other for loop. Same behavior as two for loops. */
    // while (--lSize) {
    //     int swap = 0;
    //     for (int j = 0;j < lSize;j++) {
    //         cout << "lSize: " << lSize << ", j: " << j << endl;

    //         if (l[j] > l[j+1]) {
    //             int tmp = l[j];
    //             l[j] = l[j+1];
    //             l[j+1] = tmp;
    //             swap = 1;
    //         }
    //     }

    //     for (const auto &ref : l)
    //         cout << ref << ",";
    //     cout << endl;

    //     if (swap == 0)
    //         lSize = 1;
    // }


    /* Use of two for loop */
    for (int i = 0;i < lSize - 1;i++) {
        int swap = 0;
        for (int j = 0;j < lSize - 1 - i;j++) {
            cout << "i: " << i << ", j: " << j << endl;

            if (l[j] > l[j+1]) {
                int tmp = l[j];
                l[j] = l[j+1];
                l[j+1] = tmp;
                swap = 1;
            }
        }

        for (const auto &ref : l)
            cout << ref << ",";
        cout << endl;

        if (swap == 0)
            break;
    }

    return 0;
}
