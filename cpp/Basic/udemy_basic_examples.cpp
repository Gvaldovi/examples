#include <iostream>

// Function with default arguments
void createWindow(int width = 9, int height = 5, int tick = 8);

int main() {
    using namespace std;

    /* With namespace std */
    // cout << "Hello world " << "C++" << endl;

    /* Without namespace std */
    // std::cout << "Hello world " << "C++" << std::endl;

    /* Example of getting age */
    // cout << "Provide your age: ";
    // int age;
    // cin >> age;
    // cout << "Your age is: " << age << endl;

    /* Example of getting name */
    // cout << "Provide your name: ";
    // char buff[64];
    // // cin >> buff;                    // cin will stop on a space.
    // cin.getline(buff, 64, '\n');    // cin.getline() is for large strings
    // cout << "Your name is: " << buff << endl;

    /* Uniform initialization */
    // int a{};
    // int b{789};
    // char buffA[5]{};
    // char buffB[]{"Hello C++"};
    // cout << a << endl;
    // cout << b << endl;
    // cout << buffA << endl;
    // cout << buffB << endl;

    /* Null pointer, nullptr is better to NULL in pointers. */
    // int *ptr = nullptr;

    /* References are like another name for a variable, always initialized with a referent, cannot change a referent, reference is 
    pointing to the same address value. They are like a special pointers. */
    // int x{5};
    // int &ref{x};
    // std::cout << "x: " << x << std::endl;
    // std::cout << "ref: " << ref << std::endl;

    /* Auto variables. The compiler automatically select the type of the variable needed. */
    // auto x = 3;
    // auto y{5};
    // auto sum = x + 4.5f;

    /* Range for loops */
    // int l[]{4,3,21,1};
    // // for (int x : l) {
    // for (const auto &ref : l) {
    //     cout << ref << " ";
    // }
    // cout << endl;

    /* Default arguments */
    createWindow();

    return 0;
}

void createWindow(int width, int height, int tick) {
    using namespace std;
    cout << "width " << width << endl;
    cout << "height " << height << endl;
    cout << "tick " << tick << endl;
}