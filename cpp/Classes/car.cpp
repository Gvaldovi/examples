#include <iostream>
#include "car.h"

Car::Car() {
    std::cout << "Car()" << std::endl;
}

Car::Car(float amount) {
    std::cout << "Car(amount)" << std::endl;
    fuel = amount;
    speed = 0;
    passengers = 0;
}

Car::~Car() {
    std::cout << "~Car()" << std::endl;
}

void Car::FillFuel(float amount) {
    fuel = amount;
}

void Car::Accelerate() {
    speed++;
    fuel -= 0.5f;
}

void Car::Break() {
    speed = 0;
}

void Car::AddPassenger(int count) {
    passengers = count;
}

void Car::Dashboard() {
    using namespace std;
    cout << "Fuel: " << fuel << endl;
    cout << "Speed: " << speed << endl;
    cout << "Passengers: " << passengers << endl;
}