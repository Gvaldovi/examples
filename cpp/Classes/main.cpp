#include "car.h"

/***********************************************************************************/
/* Normally this method definitions are in another file (car.cpp),
 but in vscode the Run Code option only executes one file, not a project */

/* Constructor simple*/
// Car::Car() { // Normal constructor
Car::Car():Car(0) { // Delegate constructor
    std::cout << "Constructor: Car()" << std::endl;
}
/* Constructor one argument */
// Car::Car(float amount) { // Normal constructor
Car::Car(float amount):Car(amount,0) { // Delegate constructor
    std::cout << "Constructor: Car(amount)" << std::endl;
    // fuel = amount;       // This initialization is avoided when using a delegate constructor
    // speed = 0;
    // passengers = 0;
}
/* Constructor two arguments */
Car::Car(float amount, int pass) {
    std::cout << "Constructor: Car(amount,passengers)" << std::endl;
    fuel = amount;
    speed = 0;
    passengers = pass;
}
/* Destructor */
Car::~Car() {
    std::cout << "Destructor: ~Car()" << std::endl;
}

void Car::FillFuel(float amount) {
    fuel += amount;
}

void Car::Accelerate() {
    speed++;
    fuel -= 0.5f;
}

void Car::Break() {
    speed = 0;
}

void Car::AddPassenger(int count) {
    passengers = count;
}

void Car::Dashboard() {
    using namespace std;
    cout << "Fuel: " << fuel << endl;
    cout << "Speed: " << speed << endl;
    cout << "Passengers: " << passengers << "\n" << endl;
}
/***********************************************************************************/

int main(void) {
    Car car;            // Simple constructor
    // Car car(4);         // Constructor with one argument. Function overloading.
    // Car car(4,6);       // Constructor with two argument. Function overloading.
    car.Dashboard();
    car.FillFuel(4);
    car.Accelerate();
    car.Accelerate();
    car.Accelerate();
    car.Accelerate();
    car.Dashboard();

    return 0;
}