#pragma once
#include <iostream>

class Car {
    private:
    float fuel{0};
    float speed{0};
    int passengers{4};

    public:
    Car();                          // Constructor default
    Car(float amount);              // Constructor parametrized (with argument) overloaded
    Car(float amount, int pass);    // Constructor parametrized (with two arguments) overloaded
    ~Car();                         // Destructor

    // Methods
    void FillFuel(float amount);
    void Accelerate();
    void Break();
    void AddPassenger(int count);
    void Dashboard();
};
