# -*- coding: utf-8 -*-

"""
GUI for manage RFM23 radio
"""
import wx
import datetime
#Import local class
from RFM import Rfm


class RfmGui(wx.Frame):

    def __init__(self, parent, title):
        super(RfmGui, self).__init__(parent, title=title, size=(1100, 800))

        self.InitGUI()
        self.Centre()
        self.Show()

    def InitGUI(self):
        # GUI creation with wxFormBuilder
        bSizer = wx.BoxSizer( wx.VERTICAL )

        self.m_panel1 = wx.Panel( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
        bSizer11 = wx.BoxSizer( wx.VERTICAL )

        bSizer1 = wx.BoxSizer( wx.HORIZONTAL )

        bSizer1.SetMinSize( wx.Size( -1,50 ) )
        self.m_staticText11 = wx.StaticText( self.m_panel1, wx.ID_ANY, u"Puerto", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText11.Wrap( -1 )
        bSizer1.Add( self.m_staticText11, 0, wx.ALIGN_CENTER|wx.ALL, 10 )

        self.m_textCtrlPort = wx.TextCtrl( self.m_panel1, wx.ID_ANY, u"COM1", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_textCtrlPort.SetMaxSize( wx.Size( 60,-1 ) )

        bSizer1.Add( self.m_textCtrlPort, 0, wx.ALIGN_CENTER|wx.RIGHT, 50 )

        self.m_buttonInit = wx.Button( self.m_panel1, wx.ID_ANY, u"Iniciar", wx.DefaultPosition, wx.DefaultSize, 0 )
        bSizer1.Add( self.m_buttonInit, 0, wx.ALIGN_CENTER|wx.RIGHT, 15 )

        self.m_buttonStop = wx.Button( self.m_panel1, wx.ID_ANY, u"Parar", wx.DefaultPosition, wx.DefaultSize, 0 )
        bSizer1.Add( self.m_buttonStop, 0, wx.ALIGN_CENTER_VERTICAL|wx.RIGHT, 15 )

        self.m_buttonExit = wx.Button( self.m_panel1, wx.ID_ANY, u"Salir", wx.DefaultPosition, wx.DefaultSize, 0 )
        bSizer1.Add( self.m_buttonExit, 0, wx.ALIGN_CENTER_VERTICAL|wx.RIGHT, 50 )


        bSizer11.Add( bSizer1, 1, wx.ALIGN_CENTER_HORIZONTAL|wx.EXPAND|wx.RIGHT, 5 )

        self.m_staticline2 = wx.StaticLine( self.m_panel1, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.LI_HORIZONTAL )
        bSizer11.Add( self.m_staticline2, 0, wx.EXPAND |wx.ALL, 5 )

        bSizer5 = wx.BoxSizer( wx.VERTICAL )

        self.m_staticText4 = wx.StaticText( self.m_panel1, wx.ID_ANY, u"Direcciones", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText4.Wrap( -1 )
        bSizer5.Add( self.m_staticText4, 0, wx.ALL, 5 )


        bSizer11.Add( bSizer5, 1, wx.EXPAND, 5 )

        bSizer4 = wx.BoxSizer( wx.HORIZONTAL )

        self.m_staticText2 = wx.StaticText( self.m_panel1, wx.ID_ANY, u"Origen", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText2.Wrap( -1 )
        bSizer4.Add( self.m_staticText2, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )

        self.m_textCtrlOrig = wx.TextCtrl( self.m_panel1, wx.ID_ANY, u"00 00 00 00", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_textCtrlOrig.SetMaxSize( wx.Size( 70,-1 ) )

        bSizer4.Add( self.m_textCtrlOrig, 0, wx.ALIGN_CENTER_VERTICAL|wx.RIGHT, 30 )

        self.m_staticText3 = wx.StaticText( self.m_panel1, wx.ID_ANY, u"Destino", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText3.Wrap( -1 )
        bSizer4.Add( self.m_staticText3, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )

        self.m_textCtrlDest = wx.TextCtrl( self.m_panel1, wx.ID_ANY, u"11 11 11 11", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_textCtrlDest.SetMaxSize( wx.Size( 70,-1 ) )

        bSizer4.Add( self.m_textCtrlDest, 0, wx.ALL, 5 )


        bSizer11.Add( bSizer4, 1, wx.EXPAND, 10 )

        bSizer7 = wx.BoxSizer( wx.VERTICAL )

        self.m_staticText5 = wx.StaticText( self.m_panel1, wx.ID_ANY, u"Enviar", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText5.Wrap( -1 )
        bSizer7.Add( self.m_staticText5, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )

        self.m_textCtrl5 = wx.TextCtrl( self.m_panel1, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
        bSizer7.Add( self.m_textCtrl5, 0, wx.ALL|wx.EXPAND, 5 )


        bSizer11.Add( bSizer7, 1, wx.EXPAND, 5 )

        bSizer6 = wx.BoxSizer( wx.HORIZONTAL )

        self.m_buttonStartRfmTx = wx.Button( self.m_panel1, wx.ID_ANY, u"Iniciar RFM Tx", wx.DefaultPosition, wx.DefaultSize, 0 )
        bSizer6.Add( self.m_buttonStartRfmTx, 0, wx.ALL, 5 )

        self.m_buttonStartRfmRx = wx.Button( self.m_panel1, wx.ID_ANY, u"Iniciar RFM Rx", wx.DefaultPosition, wx.DefaultSize, 0 )
        bSizer6.Add( self.m_buttonStartRfmRx, 0, wx.ALL, 5 )

        self.m_buttonSend = wx.Button( self.m_panel1, wx.ID_ANY, u"Enviar", wx.DefaultPosition, wx.DefaultSize, 0 )
        bSizer6.Add( self.m_buttonSend, 0, wx.ALL, 5 )


        bSizer11.Add( bSizer6, 1, wx.EXPAND, 5 )

        self.m_staticline21 = wx.StaticLine( self.m_panel1, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.LI_HORIZONTAL )
        bSizer11.Add( self.m_staticline21, 0, wx.EXPAND |wx.ALL, 5 )

        self.m_buttonLogClear = wx.Button( self.m_panel1, wx.ID_ANY, u"Limpiar Log", wx.DefaultPosition, wx.DefaultSize, 0 )
        bSizer11.Add( self.m_buttonLogClear, 0, wx.ALL|wx.ALIGN_CENTER_HORIZONTAL, 5 )

        self.m_textCtrlLog = wx.TextCtrl( self.m_panel1, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.TE_MULTILINE )
        self.m_textCtrlLog.SetFont( wx.Font( 9, 75, 90, 90, False, "Courier New" ) )
        self.m_textCtrlLog.SetMinSize( wx.Size( -1,600 ) )

        bSizer11.Add( self.m_textCtrlLog, 1, wx.ALL|wx.EXPAND, 5 )


        self.m_panel1.SetSizer( bSizer11 )
        self.m_panel1.Layout()
        bSizer11.Fit( self.m_panel1 )
        bSizer.Add( self.m_panel1, 1, wx.EXPAND |wx.ALL, 5 )


        self.SetSizer( bSizer )
        self.Layout()

        self.Centre( wx.BOTH )

        # Bindings
        self.m_buttonInit.Bind(wx.EVT_BUTTON, self.Start)
        self.m_buttonStop.Bind(wx.EVT_BUTTON, self.Stop)
        self.m_buttonExit.Bind(wx.EVT_BUTTON, self.Exit)
        self.m_buttonLogClear.Bind(wx.EVT_BUTTON, self.Log_clear)

        self.m_buttonStartRfmTx.Bind(wx.EVT_BUTTON, self.StartRfmTx)
        self.m_buttonStartRfmRx.Bind(wx.EVT_BUTTON, self.StartRfmRx)
        self.m_buttonSend.Bind(wx.EVT_BUTTON, self.Send)

        # Start condition
        self.m_buttonStop.Disable()

    def Start(self, e):
        # Create a Rfm object
        port = self.m_textCtrlPort.GetValue()
        self.Rfm = Rfm(port)

        # Verify port status
        if self.Rfm.PortStatus():
            self.Log_frame(port + ' 8N1@57600', 'RFM START', True)
            # Initialize buttons
            self.m_buttonInit.Disable()
            self.m_buttonStop.Enable()
        else:
            self.Log_frame(port + ' Error in port', '', True)

        # Initialize variables
        self.Tx_enable = 0

    def Stop(self, e):
        self.Rfm.Exit()
        self.m_buttonInit.Enable()
        self.m_buttonStop.Disable()
        self.Log_frame('Port closed', '', True)

    def StartRfmTx(self, e):
        # Initialization of RFM23 Module
        self.Log_frame(self.Rfm.Read(0x03, 1), 'Read int status 1')   # Read status, clear interrupts
        self.Log_frame(self.Rfm.Response(), 'RFM', True)
        self.Log_frame(self.Rfm.Read(0x04, 1), 'Read int status 2')
        self.Log_frame(self.Rfm.Response(), 'RFM', True)

        self.Log_frame(self.Rfm.Write(0x05, [0]), 'Interrupt 1')      # No interrupts
        self.Log_frame(self.Rfm.Write(0x06, [0]), 'Interrupt 2')      # No interrupts

        self.Log_frame(self.Rfm.Write(0x07, [1]), 'Ready Mode')       # Disable LBD (Low Battery Detect), wakeup timer, use internal RC oscillator
                                                                      # Xton On: Ready mode
        self.Log_frame(self.Rfm.Write(0x09, [0x7F]), 'C = 12.5p')
        self.Log_frame(self.Rfm.Write(0x0A, [5]), 'Mcu clk 2MHz')     # Output clock for Mcu. Not used

        self.Log_frame(self.Rfm.Write(0x0B, [0xEA]), 'GPIO0 direct output')     # This pin controls RX antenna.
        self.Log_frame(self.Rfm.Write(0x0C, [0xEA]), 'GPIO1 direct output')     # This pin controls TX antenna.
        self.Log_frame(self.Rfm.Write(0x0D, [0xFD]), 'GPIO2 = 1')               # GPIO2 = 1. This pin will be later the Mcu clk.
        self.Log_frame(self.Rfm.Write(0x0E, [0]), 'GPIOs output control')       # Control for GPIO0 and GPIO1.

        self.Log_frame(self.Rfm.Write(0x70, [0x20]), 'No Manchester')           # Disable manchester enc

        # Data Rate
        self.Log_frame(self.Rfm.Write(0x6E, [0x13]), 'Data rate')     # Data rate TX = (10^3 * Reg6E|Reg6F)/2^21
        self.Log_frame(self.Rfm.Write(0x6F, [0xA9]), '= 2.4Kbps')

        # Header
        self.Log_frame(self.Rfm.Write(0x30, [0x8C]), 'Packet handler')          # Enable packet handler, msb first, enable crc,
        self.Log_frame(self.Rfm.Write(0x32, [0xFF]), 'Header 4B')     # Broadcast and Header check 4 bytes.
        self.Log_frame(self.Rfm.Write(0x33, [0x42]), 'No se')
        self.Log_frame(self.Rfm.Write(0x34, [64]), '32b preamble')    # 32bit preamble

        self.Log_frame(self.Rfm.Write(0x36, [0x2D]), 'Sync word 3')
        self.Log_frame(self.Rfm.Write(0x37, [0xD4]), 'Sync word 2')
        self.Log_frame(self.Rfm.Write(0x38, [0x00]), 'Sync word 1')
        self.Log_frame(self.Rfm.Write(0x39, [0x00]), 'Sync word 0')

        self.Log_frame(self.Rfm.Write(0x3A, [0x11]), 'Tx header 3')
        self.Log_frame(self.Rfm.Write(0x3B, [0x22]), 'Tx header 2')
        self.Log_frame(self.Rfm.Write(0x3C, [0x33]), 'Tx header 1')
        self.Log_frame(self.Rfm.Write(0x3D, [0x44]), 'Tx header 0')

        self.Log_frame(self.Rfm.Write(0x3E, [20]), 'Packet lenght 20B')

        # Power, frequency and channel
        self.Log_frame(self.Rfm.Write(0x6D, [0x0F]), 'Max Tx power')  # The Tx power could change for every application

        self.Log_frame(self.Rfm.Write(0x79, [0]), 'FH channel')       # Frequency hopping channel select. This could change for every application
        self.Log_frame(self.Rfm.Write(0x7A, [0]), 'FH Step size')     # Frequency hopping step size. This could change for every application

        self.Log_frame(self.Rfm.Write(0x71, [0x22]), 'Fsk')           # Fsk, fd[8] =0, no invert for Tx/Rx data, fifo mode, txclk -->gpio
        self.Log_frame(self.Rfm.Write(0x72, [0x38]), 'F dev 45k')     # frequency deviation setting to 45k = 72*625
        self.Log_frame(self.Rfm.Write(0x73, [0]), 'No offset')
        self.Log_frame(self.Rfm.Write(0x74, [0]), 'No offset')

        #self.Log_frame(self.Rfm.Write(0x75, [0x35]), 'Freq band 900MHz')        # hbsel = 1: 480-930MHz. (20MHz*21)+480MHz = 21 = fb
        #self.Log_frame(self.Rfm.Write(0x76, [0x64]), 'No se')         # Frecuencia nominal de portadora .Puede fallar
        #self.Log_frame(self.Rfm.Write(0x77, [0]), 'No se')            # Frecuencia nominal de portadora .Puede fallar

    def StartRfmRx(self, e):
        # Initialization of RFM23 Module
        self.Log_frame(self.Rfm.Read(0x03, 1), 'Read int status 1')   # Read status, clear interrupts
        self.Log_frame(self.Rfm.Response(), 'RFM', True)
        self.Log_frame(self.Rfm.Read(0x04, 1), 'Read int status 2')
        self.Log_frame(self.Rfm.Response(), 'RFM', True)

        self.Log_frame(self.Rfm.Write(0x05, [0]), 'Interrupt 1')      # No interrupts
        self.Log_frame(self.Rfm.Write(0x06, [0]), 'Interrupt 2')      # No interrupts

        self.Log_frame(self.Rfm.Write(0x07, [1]), 'Ready Mode')       # Disable LBD (Low Battery Detect), wakeup timer, use internal RC oscillator
                                                                      # Xton On: Ready mode
        self.Log_frame(self.Rfm.Write(0x09, [0x7F]), 'C = 12.5p')
        self.Log_frame(self.Rfm.Write(0x0A, [5]), 'Mcu clk 2MHz')     # Output clock for Mcu. Not used

        self.Log_frame(self.Rfm.Write(0x0B, [0xEA]), 'GPIO0 direct output')     # This pin controls RX antenna.
        self.Log_frame(self.Rfm.Write(0x0C, [0xEA]), 'GPIO1 direct output')     # This pin controls TX antenna.
        self.Log_frame(self.Rfm.Write(0x0D, [0xFD]), 'GPIO2 = 1')               # GPIO2 = 1. This pin will be later the Mcu clk.
        self.Log_frame(self.Rfm.Write(0x0E, [0]), 'GPIOs output control')       # Control for GPIO0 and GPIO1.

        self.Log_frame(self.Rfm.Write(0x70, [0x20]), 'No Manchester')           # Disable manchester enc

        # Data Rate
        self.Log_frame(self.Rfm.Write(0x6E, [0x13]), 'Data rate')     # Data rate TX = (10^3 * Reg6E|Reg6F)/2^21
        self.Log_frame(self.Rfm.Write(0x6F, [0xA9]), '= 2.4Kbps')

        # Seccion de movedero del reloj

        # Header
        self.Log_frame(self.Rfm.Write(0x30, [0x8C]), 'Packet handler')          # Enable packet handler, msb first, enable crc,
        self.Log_frame(self.Rfm.Write(0x32, [0xFF]), 'Header 4B')     # Broadcast and Header check 4 bytes.
        self.Log_frame(self.Rfm.Write(0x33, [0x42]), 'No se')
        self.Log_frame(self.Rfm.Write(0x34, [64]), '32 bit preamble')           # 32 bit preamble
        self.Log_frame(self.Rfm.Write(0x35, [0x40]), '32b preamble detect')     # 32 bit preamble detection

        self.Log_frame(self.Rfm.Write(0x36, [0x2D]), 'Sync word 3')
        self.Log_frame(self.Rfm.Write(0x37, [0xD4]), 'Sync word 2')
        self.Log_frame(self.Rfm.Write(0x38, [0x00]), 'Sync word 1')
        self.Log_frame(self.Rfm.Write(0x39, [0x00]), 'Sync word 0')

        self.Log_frame(self.Rfm.Write(0x3F, [0x11]), 'Check header 3')
        self.Log_frame(self.Rfm.Write(0x40, [0x22]), 'Check header 2')
        self.Log_frame(self.Rfm.Write(0x41, [0x33]), 'Check header 1')
        self.Log_frame(self.Rfm.Write(0x42, [0x44]), 'Check header 0')
        self.Log_frame(self.Rfm.Write(0x43, [0xFF]), 'Check all bits')
        self.Log_frame(self.Rfm.Write(0x44, [0xFF]), 'Check all bits')
        self.Log_frame(self.Rfm.Write(0x45, [0xFF]), 'Check all bits')
        self.Log_frame(self.Rfm.Write(0x46, [0xFF]), 'Check all bits')

        # Power, frequency and channel
        self.Log_frame(self.Rfm.Write(0x79, [0]), 'FH channel')       # Frequency hopping channel select. This could change for every application
        self.Log_frame(self.Rfm.Write(0x7A, [0]), 'FH Step size')     # Frequency hopping step size. This could change for every application

        self.Log_frame(self.Rfm.Write(0x71, [0x22]), 'Fsk')           # Fsk, fd[8] =0, no invert for Tx/Rx data, fifo mode, txclk -->gpio
        self.Log_frame(self.Rfm.Write(0x72, [0x38]), 'F dev 45k')     # frequency deviation setting to 45k = 72*625
        self.Log_frame(self.Rfm.Write(0x73, [0]), 'No offset')
        self.Log_frame(self.Rfm.Write(0x74, [0]), 'No offset')

        #self.Log_frame(self.Rfm.Write(0x75, [0x35]), 'Freq band 900MHz')        # hbsel = 1: 480-930MHz. (20MHz*21)+480MHz = 21 = fb
        #self.Log_frame(self.Rfm.Write(0x76, [0x64]), 'No se')         # Frecuencia nominal de portadora .Puede fallar
        #self.Log_frame(self.Rfm.Write(0x77, [0]), 'No se')            # Frecuencia nominal de portadora .Puede fallar

    def Send(self, e):
        self.ReadyMode()

        self.Log_frame(self.Rfm.Write(0x0E, [0x02]), 'Enable Tx')
        self.Tx_enable = 1

        self.ClearTxFifo()

        # Setean el tamaño del paquete otra vez

        # Se copia el mensaje a buffer
        for i in range(20):
            self.Log_frame(self.Rfm.Write(0x7F, [i]), 'Write FIFO')

        self.Log_frame(self.Rfm.Write(0x05, [0x04]), 'Enable Packet int')

        # Leen otra vez los registros de interrupciones

        self.Log_frame(self.Rfm.Write(0x07, [0x09]), 'Send Packet')

        # Se espera la interrupcion de que hay nuevas interrupciones
        # Se apaga el Tx

    def ReadyMode(self):
        # Read interrupts
        self.Log_frame(self.Rfm.Read(0x03, 1), 'Read int status 1')   # Read status, clear interrupts
        self.Log_frame(self.Rfm.Response(), 'RFM', True)
        self.Log_frame(self.Rfm.Read(0x04, 1), 'Read int status 2')
        self.Log_frame(self.Rfm.Response(), 'RFM', True)
        self.Log_frame(self.Rfm.Write(0x07, [1]), 'Ready Mode')

    def ClearTxFifo(self):
        self.Log_frame(self.Rfm.Write(0x08, [0x03]), 'Clear TxRx Fifo 1')
        self.Log_frame(self.Rfm.Write(0x08, [0x00]), 'Clear TxRx Fifo 2')

    def Log_frame(self, lst, name='', dspace=False):
        if lst != '':
            # Review if type is str or list
            if type(lst) is str:
                st = lst
            else:
                size = len(lst)
                st = ''
                for i in lst:
                    st += str(hex(i)[2:].zfill(2)) + ' '
                st = st.upper()
                st += '-- '
                st += str(size)

            # Select one or two spaces
            if dspace is True:
                st += '\n\n'
            else:
                st += '\n'
            # Form data string
            date = datetime.datetime.now().strftime("%d/%b/%y %I:%M:%S")
            self.m_textCtrlLog.AppendText(date)
            self.m_textCtrlLog.AppendText('  ')
            self.m_textCtrlLog.AppendText(name)
            space = ' ' * (20 - len(name))
            self.m_textCtrlLog.AppendText(space)
            self.m_textCtrlLog.AppendText(st)
        else:
            self.m_textCtrlLog.AppendText('\n')

    def Log_clear(self, e):
        self.m_textCtrlLog.Clear()

    def Exit(self, e):
        print(self.Rfm.Exit())
        #self.timer.Stop()
        self.Close()


if __name__ == '__main__':
    app = wx.App()
    RfmGui(None, title='Rfm GUI')
    app.MainLoop()