"""
Communication protocol for handling RFM23 radio
This protocol transmit and receive commands through serial port
Added this line just to test an ssh key when pushing changes.
"""

import serial
import time


class Rfm(object):

    def __init__(self, port):

        # Set serial object
        try:
            self.Ser = serial.Serial(
                port=port,
                baudrate=57600,
                parity=serial.PARITY_NONE,
                stopbits=serial.STOPBITS_ONE,
                bytesize=serial.EIGHTBITS,
                timeout=1
            )
            print("Port open: ", self.Ser.isOpen())
        except:
            print("Error in Port")

    def PortStatus(self):
        try:
            return self.Ser.isOpen()
        except:
            return False

    def Read(self, cmd, size):
        frame = [cmd]
        for i in range(size):
            frame.append(0x00)
        frame.append(0x0D)
        frame.append(0x0A)

        # Write to port
        self.Ser.write(frame)
        return frame

    def Write(self, cmd, data):
        time.sleep(0.3)
        frame = [cmd + 128]
        for i in data:
            frame.append(i)
        frame.append(0x0D)
        frame.append(0x0A)

        # Write to port
        self.Ser.write(frame)
        return frame

    def Response(self):
        time.sleep(0.3)
        if self.Ser.inWaiting():
            resp = self.Ser.read(self.Ser.inWaiting())
            resp = resp[1:]
            return resp
        else:
            return 'No response'

    def Exit(self):
        try:
            self.Ser.close()
            print("Port open: ", self.Ser.isOpen())
        except:
            print('Port open: Never used')


# Prueba
#Rfm = Rfm('COM1')
#print(Rfm.Read(7, 1))
#print(Rfm.Response())
##print(Rfm.Read(8, 5))
##w = [1, 2, 3]
##print(Rfm.Write(8, w))
#Rfm.Exit()