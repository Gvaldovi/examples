from machine import Pin
from machine import Timer
from time import sleep_ms
import ubluetooth
#import binascii

ble_msg = ""


class BLE():
    def __init__(self, name):
        # Create internal objects for the onboard LED
        # blinking when no BLE device is connected
        # stable ON when connected
        self.led = Pin(2, Pin.OUT)
        self.timer1 = Timer(0)
        
        self.name = name
        self.ble = ubluetooth.BLE()
        #self.ble.config(mac=b'\x02\x01\x06\x02\x01\x06') # Mac address cannot be changed at least in micropython
        #self.ble.config(addr_mode=0)
        self.ble.active(True)
        self.disconnected()
        self.ble.irq(self.ble_irq)
        self.register()
        self.advertiser()

    def connected(self):
        self.led.value(1)
        self.timer1.deinit()

    def disconnected(self):        
        self.timer1.init(period=200, mode=Timer.PERIODIC, callback=lambda t: self.led.value(not self.led.value()))

    def ble_irq(self, event, data):
        global ble_msg
        
        if event == 1: #_IRQ_CENTRAL_CONNECT:
                       # A central has connected to this peripheral
            self.connected()

        elif event == 2: #_IRQ_CENTRAL_DISCONNECT:
                         # A central has disconnected from this peripheral.
            self.advertiser()
            self.disconnected()
        
        elif event == 3: #_IRQ_GATTS_WRITE:
                         # A client has written to this characteristic or descriptor.          
            buffer = self.ble.gatts_read(self.rx)
            ble_msg = buffer.decode('UTF-8').strip()
            
    def register(self):        
        # Nordic UART Service (NUS)
        NUS_UUID = '6E400001-B5A3-F393-E0A9-E50E24DCCA9E'
        RX_UUID = '6E400002-B5A3-F393-E0A9-E50E24DCCA9E'
        TX_UUID = '6E400003-B5A3-F393-E0A9-E50E24DCCA9E'
            
        BLE_NUS = ubluetooth.UUID(NUS_UUID)
        BLE_RX = (ubluetooth.UUID(RX_UUID), ubluetooth.FLAG_WRITE)
        BLE_TX = (ubluetooth.UUID(TX_UUID), ubluetooth.FLAG_NOTIFY)
            
        BLE_UART = (BLE_NUS, (BLE_TX, BLE_RX,))
        SERVICES = (BLE_UART, )
        ((self.tx, self.rx,), ) = self.ble.gatts_register_services(SERVICES)

    def send(self, data):
        self.ble.gatts_notify(0, self.tx, data + '\n')

    def advertiser(self):
        # Normal Beacon
        #name = bytes(self.name, 'UTF-8')
        #adv_data = bytearray('\x02\x01\x06') + bytearray((len(name) + 1, 0x09)) + name
        # iBeacon
        ibeacon_flags = bytearray([0x02,0x01,0x06])
        ibeacon_header = bytearray([0x1A,0xFF])
        ibeacon_company = bytearray([0x00,0x4C])
        ibeacon_type = bytearray([0x02])
        ibeacon_lenght = bytearray([0x15])
        ibeacon_uuid = bytearray([0xE2,0x0A,0x39,0xF4,0x73,0xF5,0x4B,0xC4,0xA1,0x2F,0x17,0xD1,0xAD,0x07,0xA9,0x61])
        ibeacon_major = bytearray([0x11,0x22])
        ibeacon_minor = bytearray([0x33,0x44])
        ibeacon_pwr = bytearray([0xC8])
        adv_data = ibeacon_flags+ibeacon_header+ibeacon_company+ibeacon_type+ibeacon_lenght+ibeacon_uuid+ibeacon_major+ibeacon_minor+ibeacon_pwr
        #adv_data = bytearray('\x02\x01\x06\x1A\xFF\x00\x4C\x02\x15\xE2\x0A\x39\xF4\x73\xF5\x4B\xC4\xA1\x2F\x17\xD1\xAD\x07\xA9\x61\x11\x22\x33\x44\xC8')

        self.ble.gap_advertise(100, adv_data)
        print(adv_data)
        a = " ".join("{:02x}".format(c) for c in adv_data)
        print(a)
        #print("\r\n")
        (t, addr) = self.ble.config("mac")
        print("type ",t)
        #print(type(addr))
        #print(addr)
        #print(addr.decode())
        #print(binascii.hexlify(addr).decode('ascii'))
        m = ":".join("{:02x}".format(c) for c in addr)
        print("mac ", m)
                # adv_data
                # raw: 0x02010209094553503332424C45
                # b'\x02\x01\x02\t\tESP32BLE'
                #
                # 0x02 - General discoverable mode
                # 0x01 - AD Type = 0x01
                # 0x02 - value = 0x02
                
                # https://jimmywongiot.com/2019/08/13/advertising-payload-format-on-ble/
                # https://docs.silabs.com/bluetooth/latest/general/adv-and-scanning/bluetooth-adv-data-basics


led = Pin(33, Pin.OUT)
#but = Pin(0, Pin.IN)
ble = BLE("ESP32BLE")

def buttons_irq(pin):
    led.value(not led.value())
    ble.send('LED state will be toggled.')
    print('LED state will be toggled.')
    
    global counter
    
    if counter >= 20:
        counter = 0
        
    counter = counter + 1
     
    ble.send('counter value is') 
    ble.send(str(counter))
    print('Counter Value', counter)
    
counter = 0
#but.irq(trigger=Pin.IRQ_FALLING, handler=buttons_irq)

while True:
    if ble_msg == 'Read Led':
        print(ble_msg)
        ble_msg = ""
        print('LED is ON.' if led.value() else 'LED is OFF')
        ble.send('LED is ON.' if led.value() else 'LED is OFF')
    elif ble_msg == 'Led On':
        print(ble_msg)
        ble_msg = ""
        print('Led is On')
        ble.send('Led is On')
        led.value(1)
    elif ble_msg == 'Led Blink':
        print(ble_msg)
        ble_msg = ""
        print('Led Blinking')
        ble.send('Led Blinking')
        ble.timer1.deinit()
        ble.timer1.init(period=1000, mode=Timer.PERIODIC, callback=lambda t: ble.led.value(not ble.led.value()))
    elif ble_msg == 'Led Off':
        print(ble_msg)
        ble_msg = ""
        print('Led is off')
        ble.send('Led is off')
        led.value(0)
        ble.timer1.deinit()
            
    sleep_ms(100)