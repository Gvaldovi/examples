# Get squares of numbers in l1 higher than 5
# map and filter takes two arguments: function and list of values to apply the function.
l1 = [2,4,6,8,10,12,14]

def square_of_num(n):
    return n**2
def greater_than(n):
    return n > 5
squares = list(map(square_of_num, filter(greater_than, l1)))
print(squares)

# With lambda functions. Lambda functions are private (anonymous) simple functions
squares1 = list(map(lambda n:n**2, filter(lambda n:n>5, l1)))
print(squares1)