class Node:
    def __init__(self, data):
        self.data = data
        self.next = None

def insertNodeAtTail(head, data):
    if head is None:
        # The linked list is empty. Put the first node
        head = Node(data)
    else:
        # Create a reference of head
        lastNode = head
        # Loop through lastNode up to lastNode.next is None
        while lastNode.next:
            lastNode = lastNode.next
        # Found the tail of head. Assign a new node to it with the data
        lastNode.next = Node(data)

    # Return the head of the list
    return head

def reverse(head):
    curr = head
    prev = next = None
    while curr is not None:
        # save next before change it
        next = curr.next
        # change next to prev
        curr.next = prev
        # move prev and curr one position to right
        prev = curr
        curr = next
    return prev

def printList(n):
    # Print n.data while n is not None (or while n)
    while n:
        print(n.data,end=' ')
        n = n.next
    print("")

if __name__ == "__main__":
    n = Node(4)

    # Insert nodes
    insertNodeAtTail(n, 1)
    insertNodeAtTail(n, 5)
    insertNodeAtTail(n, 7)
    insertNodeAtTail(n, 2)
    print("List: ",end='')
    printList(n)

    # Reverse linked list
    print("Reversed List: ",end='')
    printList(reverse(n))

