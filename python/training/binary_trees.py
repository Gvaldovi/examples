"""
Given a binary print its height, its greater path, its left side view and its top-bottom level view
"""
from tkinter import N


class Node:
    def __init__(self, data=None):
        self.data = data
        self.right = self.left = None

def getHeight(n):
    if n is None:
        return 0
    
    l = getHeight(n.left)
    r = getHeight(n.right)

    if l > r:
        return l+1
    else:
        return r+1

def getGreaterPath(n):
    if n is None:
        return []

    l = getGreaterPath(n.left)
    r = getGreaterPath(n.right)

    if len(l) > len(r):
        l.append(n.data)
        return l
    else:
        r.append(n.data)
        return r

def printLeftSideView(n,flag,level):
    if n is None:
        return

    if flag[0] < level:
        print(n.data)
        flag[0] = level

    printLeftSideView(n.left,flag,level+1)
    printLeftSideView(n.right,flag,level+1)
    
def printLevel(n,level):
    if n is None:
        return
    
    if level == 1:
        print(n.data)
    else:
        printLevel(n.left,level-1)
        printLevel(n.right,level-1)


def printTopBottomLevelView(n):
    h = getHeight(n)
    # for i in range(h,0,-1): # Print levels bottom to top
    for i in range(1,h+1): # Print levels top to bottom
        printLevel(n,i)

def printInOrderIterative(n):
    # Set current to root of binary tree
    current = n
    stack = [] # initialize stack
     
    while True:
        # Reach the left most Node of the current Node
        if current is not None:
             
            # Place pointer to a tree node on the stack
            # before traversing the node's left subtree
            stack.append(current)
         
            current = current.left
         
        # BackTrack from the empty subtree and visit the Node
        # at the top of the stack; however, if the stack is
        # empty you are done
        elif(stack):
            current = stack.pop()
            print(current.data) # Python 3 printing
         
            # We have visited the node and its left
            # subtree. Now, it's right subtree's turn
            current = current.right
        else:
            break
    
if __name__ == "__main__":
    n = Node(13)
    n.left = Node(7)
    n.right = Node(20)
    n.left.left = Node(4)
    n.right.right = Node(25)
    n.right.left = Node(19)
    n.right.right.right = Node(30)

    #         13
    #     7       20
    #  4       19    25
    #                   30

    # Print height
    # print(getHeight(n))

    # Print greater path
    # print(getGreaterPath(n))

    # Print left side view
    # flag = [0]
    # printLeftSideView(n,flag,1)

    # print top-bottom level view
    # printTopBottomLevelView(n)

    # print inOrder iteratively (not recursive)
    printInOrderIterative(n)

    