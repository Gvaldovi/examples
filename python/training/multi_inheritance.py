class Humano:
    def __init__(self,edad):
        self.edad = edad
        print('Hola soy un nuevo objeto y tengo',edad)
    def hablar(self,frase):
        print(frase)
    def caminar(self):
        print('Estoy caminando')
    def dime_edad(self):
        print('Te habia dicho que mi edad es',self.edad)
    def balon(self):
        print("Humano: balon cuadrado")


class Deportista:
    def __init__(self,deporte):
        self.deporte = deporte
        print('Mi deporte favorito es',deporte)
    def cual_deporte(self):
        print('Te dije que mi deporte es',self.deporte)
    def balon(self):
        print('Deportista: balon redondo')

# class Inge(Humano):
#     def __init__(self, edad, peso):
#         super().__init__(edad)
#         self.peso = peso
#         print('Soy el mismo objeto y tambien mi peso es',peso)
#     def estudiar(self,materia):
#         print('Debo estudiar', materia)
#     def dime_peso(self):
#         print('Te habia dicho que mi peso es',self.peso)

class Inge(Humano,Deportista):
    def __init__(self, edad, peso, deporte):
        Humano.__init__(self,edad)
        Deportista.__init__(self,deporte)
        self.peso = peso
        print('Soy el mismo objeto y tambien mi peso es',peso)
    def estudiar(self,materia):
        print('Debo estudiar', materia)
    def dime_peso(self):
        print('Te habia dicho que mi peso es',self.peso)

# Instancia
# gera = Humano(37)
# gera.hablar('Puedo hablar')
# gera.caminar()

# Herencia (se utiliza super().__init__())
# gera = Inge(37,78)
# gera.dime_edad()
# gera.dime_peso()
# gera.estudiar('Python')

# Herencia Multiple (se utiliza el nombre de la clase con ClassName.__init__(self))
gera = Inge(37,78,'Futbol')
gera.dime_peso()
gera.dime_edad()
gera.balon()
gera.cual_deporte()
