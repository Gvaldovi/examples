# Create a function that changes this "my_string_long" to this "MyStringLong", then implement a decorator for accept list of strings.
from functools import wraps

st = "my_string_long_ling"
l_st = [
    "red_hot_chilly_peppers",
    "inter_de_milan",
    "gerardo_valdovinos_villalobos",
    "albert_einstein"
]

def decorator(func):
    # The wraps is useful to maintain the doc and another important features of the main function
    @wraps(func)
    def inner(lista):
        """This is the inner"""
        # return [func(value) for value in lista]
        a = []
        for i in lista:
            a.append(func(i))
        return a
    return inner

@decorator
def camelcase(string):
    """Turn strings_like_this into StringsLikeThis"""
    return ''.join([word.capitalize() for word in string.split('_')])

result = camelcase(l_st)
print(result)
print(camelcase.__doc__)
