# Insertion sort. O(n^2) because the for loop and worst case the while loop.
n = [2,8,5,3,9,1]

for i in range(1,len(n)):
    j=i
    print(i)
    while j>0 and n[j-1] > n[j]:
        n[j-1],n[j] = n[j], n[j-1]
        j = j-1
        print(n)