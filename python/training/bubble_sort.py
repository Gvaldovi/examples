# Insertion sort. O(n^2) because the for loop and worst case the while loop.
n = [2,8,5,3,9,4]

for i in range(len(n)-1):
    for j in range(len(n)-i-1):
        swap = False
        if n[j] > n[j+1]:
            n[j],n[j+1] = n[j+1],n[j]
            swap = True
    print(n)
    if swap == False:
        break