"""Create a function which returns if a number is prime. The process is to review all the modules with the function all() in a range from 2 to num-1.
If there is an exact division like 4/2 then the module will be 0 so the all() will return FALSE.
The all() function returns True if all items in an iterable are true, otherwise it returns False.
If the iterable object is empty, the all() function also returns True.
"""
def is_prime(num):
    return all(num % i for i in range(2, num))
n = 7
# print("{} is prime?: {}".format(n,is_prime(n)))

# Return fibonacci (start with 0 and 1 and next number is the sum of previous two) list up to a number of element greater than 2
def fibonacci(num):
    fibo = [0, 1]
    if num == 0:
        return 'Error'
    elif num == 1:
        return [0]
    elif num == 2:
        return fibo
    else:
        for i in range(num-2):
            fibo.append(fibo[-2] + fibo[-1])
        return fibo
n = 5
# print("The first {} numbers of Fibonacci serie are:\n{}".format(n,fibonacci(n)))

# Create a function which returns if a number is fibonacci
def is_fibonacci(num):
    history = (0,1)
    while sum(history) < num:
        history = (history[1], sum(history))
    return sum(history) == num
number = 5 
# print("{} is fibonacci?, {}".format(number, is_fibonacci(number)))

# Fizzbuzz, print numbers from 1 to 100, but for multiples of 3 print Fizz, for multiples of 5 print Buzz and for multiples of both print FizzBuzz.
# num = 100
# for i in range(1,num+1):
#     if i%3 == 0 and i%5 == 0:
#         print("FizzBuzz",end=',')
#     elif i%3 == 0:
#         print("Fizz",end=',')
#     elif i%5 == 0:
#         print("Buzz",end=',')
#     else:
#         print(i,end=',')

# Given an integer, return the integer with reversed digits.
# Note: The integer could be either positive or negative.
def reverse(n):
    s = str(n)

    if s[0] == "-":
        return int('-' + s[:0:-1])
    else:
        return int(s[::-1])

# print(reverse(7894))
# print(reverse(-12345))

""" For a given sentence, return the average word length. 
    Note: Remember to remove punctuation first."""
def average(s):
    for p in "?.,!'":
        s = s.replace(p,'')
    words = s.split()
    return round(sum(len(word) for word in words)/len(words),2)
# print(average("Hola. mundo cruel dos para siempre?"))

""" Given two non-negative integers num1 and num2 represented as string, return the sum of num1 and num2.
    You must not use any built-in BigInteger library or convert the inputs to integer directly."""

#Notes:
#Both num1 and num2 contains only digits 0-9.
#Both num1 and num2 does not contain any leading zero.

"""
Find Duplicate Number
Given an array nums containing n+1 integers where each integer is between 1 and n (inclusive), prove
that at least one duplicate number must exist.
Assume that there is only one duplicate number, find the duplicate one.

Example 1:
Input: [1,3,4,2,2]
Output: 2

Example 2:
Input: [3,1,3,4,2]
Output: 3

Note: You must not modify the array (assume the array is read only).
You must use only constan, O(1) extra space.
Your runtime complexity should be less than O(n2).
There is only one duplicate number in the array, but it could be repeated more than once.
"""
def findDuplicate(nums):
    tortoise = nums[0]  # 3
    hare = nums[0]      # 3
    while True:
        tortoise = nums[tortoise]   # 4 2
        hare = nums[nums[hare]]     # 2 2
        if tortoise == hare:
            break

    ptr1 = nums[0]  # 3
    ptr2 = tortoise # 2
    while ptr1 != ptr2:
        ptr1 = nums[ptr1] # 4 2
        ptr2 = nums[ptr2] # 2 2
    return ptr1
# array = [3,1,3,4,2] # Original
array = [3,1,2,4,2]
# print(array)
# print(findDuplicate(array))

"""
Take a string in the form of "aaabbcccc" and transform it into "3a2b4c". This algorithm is called
Run Lenght Encoding.
"""
def runLenghtEnc(s):
    if not s or len(s) == 0:
        return "invalid input"

    output = ""
    prev = ""
    count = 0
    for i in s:
        if prev == "":
            prev = i
            count = 1
        elif prev == i:
            count += 1
        else:
            output += str(count) + prev
            prev = i
            count = 1

    output += str(count) + prev
    return output

# print(runLenghtEnc("abbcccdddd"))

"""
Given a grid of robot positions, indicate if it is a valid time series for
the number of robots specified if robots can only travel up to
1 index further than their position 1 step before.

Input format: array of arrays, of which each index can be 0 or 1.
An index corresponds to the physical location which is either occupied by a robot (1)
or empty (0)

Ex:
Grid: [[1,0,0,1],[0,1,1,0]] is a valid series for 2 robots because
the first robot moved from index 1 to index 1 and robot 2 moved from index 3 to index 2.
"""
def gridVerification(grid, numRobots):
    # if sum(grid[0]) != numRobots:
    #     return False
    # for r in range(1,len(grid)):
    #     for c in range(1,len(grid[0])):
    #         if grid[r-1][c-1] == 1
    #         print(grid[r][c])
    

    return True

# grid = [[1,0,0,1],[0,1,1,0]]
# grid = [[1,0,0,0,1],[1,0,1,0,0]]
grid = [[1,0,0,1,0],[1,0,1,0,0]]
# print("Grid ",grid,"is: {}".format(gridVerification(grid,2)))

"""
Pregunta examen muestra Amazon:
Amazon Fresh is running a promotion in which customers receive prizes for purchasing a secret combination of fruits. The 
combination will change each day, and the team running the promition wants to use a code list to make it easy to change the combination.
The code list contains groups of fruits. Both the order of the groups within the code list and the order of the fruits withing the groups matter.
However, between the groups of fruits, any number, and type of fruit is allowable. The term "anything" is used to allow for any type of fruit to
appear in that location within the group.
Example:
Input: codeList = [[apple,apple],[banana,anything,banana]], shoppingCart=[apple,banana,apple,bannana,orange,banana]
Output: 0
"""
def foo(codeList, shoppingCart):
    i = 0
    for G in codeList:
        j = 0
        out = 0
        while i < len(shoppingCart) and j < len(G):
            print("shop and G: ",shoppingCart[i], G[j])
            if shoppingCart[i] == G[j] or G[j] == "anything":
                i += 1
                j += 1
                out = 1
            elif shoppingCart[i] != G[j] and out == 1:
                j = 0
                out = 0
            elif shoppingCart[i] != G[j] and out == 0:
                i += 1
                j = 0
        print("i,j: ",i,j)
        if out == 0 or j < len(G):
            return 0
        print("correcto")
    return out

# codeList = [["apple","apple"],["banana","anything","banana"]]
# codeList = [["banana","apple"],["banana","anything","banana"]]
# codeList = [["anything"],["anything"],["banana"]]
# shoppingCart = ["apple","banana","apple","banana","orange","banana"]

# codeList = [["apple","apple"],["apple","apple","banana"]]
# codeList = [["apple","apple"],["apple","banana","apple"]]
# shoppingCart = ["apple","apple","apple","banana"]
# print("Output: ",foo(codeList,shoppingCart))

"""
(Youtube street coding challenge) Double sum: given a sum=9 you have to sum the two numbers of an array that is equal to sum
"""
suma = 17
arr = [1,3,5,6,11,23]
for i in range(len(arr)-1):
    for j in range(i,len(arr)):
        if arr[i] + arr[j] == suma:
            print("Found thenumbers, they are {} and {}".format(arr[i],arr[j]))
            break
    