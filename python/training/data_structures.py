"""
(HackerRank) Binary trees: it is a structure with a node root with two child nodes, left and right. If a value is less or equal to root then it is stored 
to the left side node, otherwise on the right one.
"""

class NodeH:
    def __init__(self, data):
        self.right = self.left = None
        self.data = data

    def insert(self, value):
        if value <= self.data:
            if self.left == None:
                self.left = NodeH(value)
            else:
                self.left.insert(value)
        else:
            if self.right == None:
                self.right = NodeH(value)
            else:
                self.right.insert(value)
    
    def contains(self, value):
        if value == self.data:
            return True

        elif value < self.data:
            if self.left == None:
                return False
            else:
                return self.left.contains(value)
        else:
            if self.right == None:
                return False
            else:
                return self.right.contains(value)

    def printInOrder(self):
        # left, root, right
        if self.left != None:
            self.left.printInOrder()
        print(self.data)
        if self.right != None:
            self.right.printInOrder()

    def printPreOrder(self):
        # root, left, right
        print(self.data)
        if self.left != None:
            self.left.printInOrder()
        if self.right != None:
            self.right.printInOrder()

    def printPostOrder(self):
        # left, right, root
        if self.left != None:
            self.left.printInOrder()
        if self.right != None:
            self.right.printInOrder()
        print(self.data)

# Function to find and return the
# longest path
def getHeight(node):
    # If root is null means there
    # is no binary tree so
    # return a empty vector
    if node == None:
        return []

    # Recursive call on node.right
    rvec = getHeight(node.right)
    # Recursive call on node.left
    lvec = getHeight(node.left)

    # Compare the size of the two vectors
    # and insert current node accordingly
    if len(lvec) > len(rvec):
        lvec.append(node.data)
    else:
        rvec.append(node.data)

    # Return the appropriate vector
    if len(lvec) > len(rvec):
        return lvec
    else:
        return rvec


"""
(Joma) Create a binary tree and print it in inOrder way. First using recursion and then an iterative solution
"""
class Node:
    def __init__(self, data=None):
        self.data = data
        self.left = self.right = None

def insert(n, val):
    if n == None:
        return

    if n.data == None:
        n.data = val
        return

    if val > n.data:
        if n.right == None:
            n.right = Node(val)
        else:
            insert(n.right, val)
    else:
        if n.left == None:
            n.left = Node(val)
        else:
            insert(n.left, val)

def printInOrder(n):
    # left, root, right
    if n.left != None:
        printInOrder(n.left)
    print(n.data)
    if n.right != None:
        printInOrder(n.right)

"""
(Geeks for geeks) Level order binary tree traversal. Prints the tree by layers from top to bottom.
"""
def height(n):
    if n is None:
        return 0
    else:
        # Compute the height of each subtree
        lheight = height(n.left)
        rheight = height(n.right)

        # Use the larger one
        if lheight > rheight:
            return lheight+1
        else:
            return rheight+1

def printCurrentLevel(n,level):
    if n is None:
        return
    if level == 1:
        print(n.data)
    elif level > 1:
        printCurrentLevel(n.left, level-1)
        printCurrentLevel(n.right, level-1)

def printLevelOrder(n):
    h = height(n)
    for i in range(1,h+1):
        printCurrentLevel(n,i)
        print("new level")


"""
(Amazon) Given a binary tree, print the left view of it.
"""
def leftSidePrint(n,flag, level):
    if n is None:
        return

    # print current data
    if flag[0] < level:
        print(n.data)
        flag[0] = level #1,2,3

    leftSidePrint(n.left, flag, level+1)
    leftSidePrint(n.right,flag, level+1)

#       5
#    1      6
#               8

if __name__ == "__main__":
    ### HackerRank example
    # n = NodeH(3)
    # values = [6,4,7,2,1,0,10,9,11]
    # for i in values:
    #     n.insert(i)

    # # n.printInOrder()
    # n.printPreOrder()
    # # n.printPostOrder()

    # number = 89
    # print("Does the tree contains {}?: {}".format(number, n.contains(number)))
    # print("Height of tree is: {}".format(getHeight(n)))



    ### Joma example
    n = Node(5)
    values = [6,1,15,7,10,3,9,11,2]
    for v in values:
        insert(n,v)
    # insert(n,5)
    # printInOrder(n)
    # print("Height of tree is: {}".format(getHeight(n)))



    ### Amazon example
    n = Node()
    printInOrder(n)
    print("Left side view of n:")
    flag = [0]
    leftSidePrint(n,flag,1)



    ### Geeks for Geeds example
    # printLevelOrder(n)
