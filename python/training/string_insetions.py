def is_prime(n):
    return all([n % i for i in range(2,n)])

number = 5   
print("{} is prime?, {}".format(number, is_prime(number)))
print("%d is prime?, %s" %(number, is_prime(number)))
print(f"{number} is prime?, %s" %(is_prime(number)))