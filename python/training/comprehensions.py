# list comprehension: get squares of numbers in l1 higher than 8
l1 = [2,4,6,8,10,12,14]
squares = [num**2 for num in l1 if num > 8]
# print(squares)

# Dict comprehension: from D get the fruits that start with "m", capitalize the name and multiply the count by 2.
# Note: for iterating both key and value of a dictionary you should use the dict.items() method.
D = {"naranja":4,
"manzana":14,
"mandarina":17,
"pera":5,
"melon":20,
"sandia":8}
di = {fruit.capitalize():count*2 for fruit, count in D.items() if fruit[0] == 'm'}
# print(di)

# Set comprehensions. Note: Set and dict comprehensions use the same {} but if python finds a ":" after the first value, then it will be a dict comprehension, otherwise it will be a set one.
num = 100
mult_3 = {i for i in range(1,num) if i%3 == 0}
mult_5 = {i for i in range(1,num) if i%5 == 0}
mult_3_5 = list(mult_3 & mult_5)
mult_3_5.sort()
# print(mult_3_5)
