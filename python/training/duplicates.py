# output the list "a" in the same order and without duplicates
a = [2,3,1,2,3,1,5,1,4,5]
l = []
for i in a:
    if i not in l:
        l.append(i)
print(l)

# output the list "a" without duplicates no matter the order
l1 = list(set(a))
print(l1)
