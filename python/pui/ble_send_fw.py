# Send BLE binary by packets to the BLE chip
import serial
import time

# Try to open the serial port
try:
    ser = serial.Serial("/dev/ttyUSB0", 115200)
    print('Port open: ', ser.is_open)
except:
    print('Port open: ', ser.is_open)
    exit()
time.sleep(1)

# Send the update start command

# Open the binary file
# file = "/home/eiktrynu/Documents/repos/ble_tlsr8273/tlsr8273/tlsr8273X.bin"
file = "/home/eiktrynu/Documents/repos/ble_tlsr8273/tlsr8273/tlsr8273.bin"
# file = "/home/eiktrynu/Documents/repos/ble_tlsr8273/tlsr8273/tlsr8273v1_1_10.bin"
with open(file,"rb") as f:
    pkt = f.read(256)
    n = 0
    while pkt:
        ser.write(pkt)
        # pktByte = list(pkt)
        # print(pktByte)
        n = n + 1
        print("Sent pkt {} with {} bytes".format(n, len(pkt)))
        time.sleep(0.2)

        # Is there a Rx message?
        if ser.in_waiting:
            time.sleep(0.2)
            print("Abort update")
            print(ser.read(ser.in_waiting))
            break
        else:
            pkt = f.read(256)

print("Finished update")
time.sleep(2)
if ser.in_waiting:
    time.sleep(0.2)
    print(ser.read(ser.in_waiting))

ser.close()
