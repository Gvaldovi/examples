# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'IK_gui.ui'
#
# Created by: PyQt5 UI code generator 5.11.3
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(386, 166)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.pushButton = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton.setGeometry(QtCore.QRect(10, 10, 75, 23))
        self.pushButton.setObjectName("pushButton")
        self.pushButton_2 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_2.setGeometry(QtCore.QRect(90, 10, 75, 23))
        self.pushButton_2.setObjectName("pushButton_2")
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 386, 21))
        self.menubar.setObjectName("menubar")
        self.menuCommunication = QtWidgets.QMenu(self.menubar)
        self.menuCommunication.setObjectName("menuCommunication")
        self.menuParity = QtWidgets.QMenu(self.menuCommunication)
        self.menuParity.setTabletTracking(True)
        self.menuParity.setFocusPolicy(QtCore.Qt.ClickFocus)
        self.menuParity.setAutoFillBackground(True)
        self.menuParity.setObjectName("menuParity")
        self.menuBaudRate = QtWidgets.QMenu(self.menuCommunication)
        self.menuBaudRate.setObjectName("menuBaudRate")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)
        self.actionEven = QtWidgets.QAction(MainWindow)
        self.actionEven.setCheckable(True)
        self.actionEven.setChecked(False)
        self.actionEven.setObjectName("actionEven")
        self.actionOdd = QtWidgets.QAction(MainWindow)
        self.actionOdd.setCheckable(True)
        self.actionOdd.setObjectName("actionOdd")
        self.action9600 = QtWidgets.QAction(MainWindow)
        self.action9600.setCheckable(True)
        self.action9600.setObjectName("action9600")
        self.action19200 = QtWidgets.QAction(MainWindow)
        self.action19200.setCheckable(True)
        self.action19200.setChecked(True)
        self.action19200.setObjectName("action19200")
        self.action115200 = QtWidgets.QAction(MainWindow)
        self.action115200.setCheckable(True)
        self.action115200.setObjectName("action115200")
        self.actionPort = QtWidgets.QAction(MainWindow)
        self.actionPort.setObjectName("actionPort")
        self.menuParity.addAction(self.actionEven)
        self.menuParity.addAction(self.actionOdd)
        self.menuBaudRate.addAction(self.action9600)
        self.menuBaudRate.addAction(self.action19200)
        self.menuBaudRate.addAction(self.action115200)
        self.menuCommunication.addAction(self.menuParity.menuAction())
        self.menuCommunication.addAction(self.menuBaudRate.menuAction())
        self.menuCommunication.addAction(self.actionPort)
        self.menubar.addAction(self.menuCommunication.menuAction())

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.pushButton.setText(_translate("MainWindow", "Start"))
        self.pushButton_2.setText(_translate("MainWindow", "Stop"))
        self.menuCommunication.setTitle(_translate("MainWindow", "Communication"))
        self.menuParity.setTitle(_translate("MainWindow", "Parity"))
        self.menuBaudRate.setTitle(_translate("MainWindow", "BaudRate"))
        self.actionEven.setText(_translate("MainWindow", "Even"))
        self.actionOdd.setText(_translate("MainWindow", "Odd"))
        self.action9600.setText(_translate("MainWindow", "9600"))
        self.action19200.setText(_translate("MainWindow", "19200"))
        self.action115200.setText(_translate("MainWindow", "115200"))
        self.actionPort.setText(_translate("MainWindow", "Port"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())

