import sys
from BLE_gui import Ui_MainWindow
from PyQt5 import QtCore, QtGui, QtWidgets

class MainWindow_exec():
	def __init__(self):
		app = QtWidgets.QApplication(sys.argv)

		MainWindow = QtWidgets.QMainWindow()
		self.ui = Ui_MainWindow()
		self.ui.setupUi(MainWindow)

		# Configuration
		self.Config()

		MainWindow.show()
		sys.exit(app.exec_())

	def Config(self):
		self.ui.pushButton.clicked.connect(self.Comm)
		self.ui.menuCommunication.aboutToShow.connect(self.UpdatePorts)

	def Comm(self):
		print("jalo")

	def UpdatePorts(self):
		print("Actualiza puertos")

if __name__ == "__main__":
	MainWindow_exec()