"""
Script que valida el firmware enviado desde la RF AMI hacia el medidor.
Cada trama se conforma como sigue:
    Modbus Id     - 30 30 30 30 30 30 30 30 30 30 30 30 30 30 30 30 00
    Function code - 15
    Page          - 00 44
    Page offset   - 00 E0
    Data size     - 00 10
    Data (32b)    - 7F 00 19 00 6F 41 5F 83 C1 4F 00 00 4F 93 FA 23 B0 12 E0 99 F2 F0 EF 00 89 07 21 53 30 41 B0 12
    CRC           - 88 86
"""
import wx
import serial
import time


class Firmware_Decoder(wx.Frame):

    def __init__(self, parent, title):
        super(Firmware_Decoder, self).__init__(parent, title=title, size=(500, 200))

        self.InitUI()
        self.Centre()
        self.Show()

    def InitUI(self):
        # GUI creation with wxFormBuilder
        bSizer = wx.BoxSizer( wx.VERTICAL )

        self.m_panel1 = wx.Panel( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
        bSizer11 = wx.BoxSizer( wx.VERTICAL )

        self.m_staticText10 = wx.StaticText( self.m_panel1, wx.ID_ANY, u"Extractor de firmware de las tramas enviadas desde la RF AMI hacia el medidor.", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText10.Wrap( -1 )
        bSizer11.Add( self.m_staticText10, 0, wx.ALL, 5 )

        self.m_staticText11 = wx.StaticText( self.m_panel1, wx.ID_ANY, u"El firmware se guarda en el archivo Firmware.txt y esta en la carpeta de esta aplicacion.", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText11.Wrap( -1 )
        bSizer11.Add( self.m_staticText11, 0, wx.ALL, 5 )

        bSizer17 = wx.BoxSizer( wx.HORIZONTAL )

        self.m_staticText13 = wx.StaticText( self.m_panel1, wx.ID_ANY, u"Puerto", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText13.Wrap( -1 )
        bSizer17.Add( self.m_staticText13, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

        self.m_textCtrlPort = wx.TextCtrl( self.m_panel1, wx.ID_ANY, u"COM4", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_textCtrlPort.SetMaxSize( wx.Size( 60,-1 ) )

        bSizer17.Add( self.m_textCtrlPort, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

        self.m_buttonInit = wx.Button( self.m_panel1, wx.ID_ANY, u"Iniciar", wx.DefaultPosition, wx.DefaultSize, 0 )
        bSizer17.Add( self.m_buttonInit, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

        self.m_buttonExit = wx.Button( self.m_panel1, wx.ID_ANY, u"Salir", wx.DefaultPosition, wx.DefaultSize, 0 )
        bSizer17.Add( self.m_buttonExit, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )


        bSizer11.Add( bSizer17, 1, wx.EXPAND|wx.ALIGN_CENTER_HORIZONTAL, 5 )

        self.m_staticTextStatus = wx.StaticText( self.m_panel1, wx.ID_ANY, u"Estado: Detenido", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticTextStatus.Wrap( -1 )
        bSizer11.Add( self.m_staticTextStatus, 0, wx.ALL|wx.ALIGN_CENTER_HORIZONTAL, 5 )


        self.m_panel1.SetSizer( bSizer11 )
        self.m_panel1.Layout()
        bSizer11.Fit( self.m_panel1 )
        bSizer.Add( self.m_panel1, 1, wx.EXPAND |wx.ALL, 5 )


        self.SetSizer( bSizer )
        self.Layout()

        self.Centre( wx.BOTH )

        #Bindings
        self.m_buttonInit.Bind(wx.EVT_BUTTON, self.Start)
        self.m_buttonExit.Bind(wx.EVT_BUTTON, self.Quit)

        self.timer = wx.Timer(self)
        self.Bind(wx.EVT_TIMER, self.Rx_data, self.timer)

    def Start(self, e):
        # Configure serial port
        self.ser = serial.Serial()
        self.ser.port = self.m_textCtrlPort.GetValue()
        self.ser.baudrate = 19200
        self.ser.parity = serial.PARITY_EVEN
        self.ser.timeout = None

        try:
            self.ser.open()
            print('Port open:', self.ser.isOpen())
            self.Archivo = open('Firmware.txt', 'w')
            self.Archivo.write('010203040506')  # Espacio para firma
            print('File open: True')

            # Initialize timer
            self.timer.Start(100)
            self.m_staticTextStatus.SetLabel('Estado: Iniciado')
            self.counter = 6
            self.m_buttonInit.Disable()
        except:
            self.m_staticTextStatus.SetLabel('Estado: Puerto no funciona')

    def Rx_data(self, e):
        self.timer.Stop()

        if self.ser.inWaiting():
            time.sleep(0.1)
            frame = self.ser.read(self.ser.inWaiting())

            if len(frame) > 17 and frame[17] == 0x15:
                frame = frame[24:-2]
                st = ''

                for i in frame:
                    st += str(hex(i)[2:].zfill(2))
                st = st.upper()
                self.Archivo.write(st)

                #for i in frame:
                    #x = str(hex(i)[2:].zfill(2))
                    #self.Archivo.write(x.upper())
                    #self.counter += 1
                    #if self.counter >= 64:
                        #self.counter = 0
                        #self.Archivo.write('\n')

                #self.counter += len(frame)
                #if self.counter % 2 == 0:
                    #self.Archivo.write('\n')

                self.m_staticTextStatus.SetLabel('Estado: Procesando')

        self.timer.Start(100)

    def Quit(self, e):
        try:
            self.Archivo.close()
            print('File open: False')
        except:
            print('File open: Never used')

        try:
            self.ser.close()
            print('Port open: False')
        except:
            print('Port open: Never used')

        self.timer.Stop()
        self.Close()


if __name__ == '__main__':
    app = wx.App()
    Firmware_Decoder(None, title='Extractor de firmware de Scorpio')
    app.MainLoop()






