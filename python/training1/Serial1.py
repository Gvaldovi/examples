# Puerto serial
# Lectura/Escritura basica del puerto
import serial
import time

# # Serial configuration. This way doesn't open the port
# ser = serial.Serial()
# ser.port = "ttyUSB0"
# ser.baudrate = 115200
# #ser.parity = serial.PARITY_EVEN
# ser.parity = serial.PARITY_NONE
# ser.timeout = None
# ser.interCharTimeout = 0.001

# # Serial configuration. This way opens the port
# ser = serial.Serial(
#     port="/dev/ttyUSB1",
#     baudrate=115200,
#     stopbits=serial.STOPBITS_ONE,
#     bytesize=serial.EIGHTBITS,
#     parity=serial.PARITY_NONE,
#     timeout=0,
#     # interCharTimeout=0.01
# )

# Test send receive
try:
    ser = serial.Serial("/dev/ttyUSB0", 115200)
    print('Port open: ', ser.is_open)
except:
    print('Port open: ', ser.is_open)
    exit()

time.sleep(1)
# Open the binary file

ser.write([0x7F, 0x06, 0xAA, 0x00, 0x8C, 0x89, 0xF7])
while 1:
    if ser.in_waiting > 0:
        # in_waiting variable is not 0 as long as the first byte arrives. So wait for more time to get all frame
        time.sleep(0.2)
        lista = ser.read(ser.in_waiting)
        print(lista)
        break
ser.close()




# ser.open()
# print('Puerto: ', ser.isOpen())

# Envio = "Salida del programa\n"
# ser.write([0x01, 0x06, 0x01, 0xF0, 0xE0, 0x0D])

" Envio de tramas simples "
# while 1:
#     if ser.inWaiting():
#         """
#         El tiempo de 200ms de espera permite recibir 255 bytes sin problemas
#         """
#         time.sleep(0.2)
#         lista = ser.read(ser.inWaiting())
#         if 0x34 in lista:
#             ser.write(Envio.encode('utf-8'))
#             ser.close()
#             break
#         else:
#             print(lista)

" Deteccion de tramas muy juntas. Se imprime cada trama por separado en base "
" a el timeout entre caracteres. Para sniffer de MRF-IK "
# while True:
#     data = ser.read(200)
#     print(data)



