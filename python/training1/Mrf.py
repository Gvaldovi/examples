"""
Clase para emular la MRF (Microchip RF module) a travez de puertos COM de la
computadora.
"""
import serial
import time
import struct

table = (
0x0000, 0xC0C1, 0xC181, 0x0140, 0xC301, 0x03C0, 0x0280, 0xC241,
0xC601, 0x06C0, 0x0780, 0xC741, 0x0500, 0xC5C1, 0xC481, 0x0440,
0xCC01, 0x0CC0, 0x0D80, 0xCD41, 0x0F00, 0xCFC1, 0xCE81, 0x0E40,
0x0A00, 0xCAC1, 0xCB81, 0x0B40, 0xC901, 0x09C0, 0x0880, 0xC841,
0xD801, 0x18C0, 0x1980, 0xD941, 0x1B00, 0xDBC1, 0xDA81, 0x1A40,
0x1E00, 0xDEC1, 0xDF81, 0x1F40, 0xDD01, 0x1DC0, 0x1C80, 0xDC41,
0x1400, 0xD4C1, 0xD581, 0x1540, 0xD701, 0x17C0, 0x1680, 0xD641,
0xD201, 0x12C0, 0x1380, 0xD341, 0x1100, 0xD1C1, 0xD081, 0x1040,
0xF001, 0x30C0, 0x3180, 0xF141, 0x3300, 0xF3C1, 0xF281, 0x3240,
0x3600, 0xF6C1, 0xF781, 0x3740, 0xF501, 0x35C0, 0x3480, 0xF441,
0x3C00, 0xFCC1, 0xFD81, 0x3D40, 0xFF01, 0x3FC0, 0x3E80, 0xFE41,
0xFA01, 0x3AC0, 0x3B80, 0xFB41, 0x3900, 0xF9C1, 0xF881, 0x3840,
0x2800, 0xE8C1, 0xE981, 0x2940, 0xEB01, 0x2BC0, 0x2A80, 0xEA41,
0xEE01, 0x2EC0, 0x2F80, 0xEF41, 0x2D00, 0xEDC1, 0xEC81, 0x2C40,
0xE401, 0x24C0, 0x2580, 0xE541, 0x2700, 0xE7C1, 0xE681, 0x2640,
0x2200, 0xE2C1, 0xE381, 0x2340, 0xE101, 0x21C0, 0x2080, 0xE041,
0xA001, 0x60C0, 0x6180, 0xA141, 0x6300, 0xA3C1, 0xA281, 0x6240,
0x6600, 0xA6C1, 0xA781, 0x6740, 0xA501, 0x65C0, 0x6480, 0xA441,
0x6C00, 0xACC1, 0xAD81, 0x6D40, 0xAF01, 0x6FC0, 0x6E80, 0xAE41,
0xAA01, 0x6AC0, 0x6B80, 0xAB41, 0x6900, 0xA9C1, 0xA881, 0x6840,
0x7800, 0xB8C1, 0xB981, 0x7940, 0xBB01, 0x7BC0, 0x7A80, 0xBA41,
0xBE01, 0x7EC0, 0x7F80, 0xBF41, 0x7D00, 0xBDC1, 0xBC81, 0x7C40,
0xB401, 0x74C0, 0x7580, 0xB541, 0x7700, 0xB7C1, 0xB681, 0x7640,
0x7200, 0xB2C1, 0xB381, 0x7340, 0xB101, 0x71C0, 0x7080, 0xB041,
0x5000, 0x90C1, 0x9181, 0x5140, 0x9301, 0x53C0, 0x5280, 0x9241,
0x9601, 0x56C0, 0x5780, 0x9741, 0x5500, 0x95C1, 0x9481, 0x5440,
0x9C01, 0x5CC0, 0x5D80, 0x9D41, 0x5F00, 0x9FC1, 0x9E81, 0x5E40,
0x5A00, 0x9AC1, 0x9B81, 0x5B40, 0x9901, 0x59C0, 0x5880, 0x9841,
0x8801, 0x48C0, 0x4980, 0x8941, 0x4B00, 0x8BC1, 0x8A81, 0x4A40,
0x4E00, 0x8EC1, 0x8F81, 0x4F40, 0x8D01, 0x4DC0, 0x4C80, 0x8C41,
0x4400, 0x84C1, 0x8581, 0x4540, 0x8701, 0x47C0, 0x4680, 0x8641,
0x8201, 0x42C0, 0x4380, 0x8341, 0x4100, 0x81C1, 0x8081, 0x4040)


def crc_calc(array, crc):
    for ch in array:
        crc = (crc >> 8) ^ table[(crc ^ ch) & 0xFF]
    return crc


def crc_append(array):
    crc = crc_calc(array, 0xFFFF)                        # Calculate crc
    crc1 = struct.unpack("2B", struct.pack("H", crc))    # Convert crc to bytes
    array.append(crc1[0])                                # Append bytes to array
    array.append(crc1[1])
    return array


class Mrf(object):
    """
    Comandos de MRF
    """
    # Registers of MRF
    registers = {0: [0x24, 0xDA, 0xB6, 0x0A, 0x01, 0x09, 0x10, 0x01],     # MAC Address
                 8: [0x0E, 0x0F],                                         # FW Version
                 10: [0, 0],                                              # HW Version
                 12: [0, 0],                                              # Channel
                 14: [0, 0],                                              # PAN ID
                 16: [0, 0, 0, 0],                                        # Universal Time
                 20: [0, 0, 0, 0],                                        # Time zone
                 24: [0, 0, 0, 0],                                        # Flags
                 28: [0, 0],                                              # Radio type
                 30: [0, 0],                                              # Radio Power
                 32: [0, 0]}                                              # Device counter

    def __init__(self, port=None, br=19200, timeout=5):
        """
        Initializes the serial connection to the port
        """
        self.ser = serial.Serial()
        self.ser.port = port
        self.ser.baud_rate = br
        self.ser.parity = serial.PARITY_EVEN
        self.ser.timeout = timeout    # Timeout for readline()
        self.ser.open()

        if self.ser.isOpen() is True:
            print("MRF module on")
        else:
            print("MRF module off")

    def New_data(self):
        if self.ser.inWaiting():
            time.sleep(0.2)    # 0.2 - Time for waiting up to 255 bytes @19200E1

            if self.ser.inWaiting() > 3:    # Review buffer more than 1 bytes
                self.rx_buffer = self.ser.read(self.ser.inWaiting())
                print(self.rx_buffer)

                if crc_calc(self.rx_buffer, 0xFFFF) == 0:    # Review of CRC, if 0 then OK
                    if self.rx_buffer[1] == 0x03:
                        self.Local_read()
                    elif self.rx_buffer[1] == 0x10:
                        self.Local_write()
                    elif self.rx_buffer[1] == 0x04:
                        self.Ok_response()
                        return "Remote"
                    elif self.rx_buffer[1] == 0x05:
                        self.Ok_response()
                    else:
                        print("Bad code")
                        return "Error"
                else:
                    print("CRC error")
                    return "Error"

            else:
                print("Less than 3 bytes")
                return "Error"
        else:
            return "Nothing"

    def Local_read(self):
        response = [0x01, 0x03, self.rx_buffer[3]]
        for i in self.registers[self.rx_buffer[2]]:
            response.append(i)
        crc_append(response)
        self.ser.write(response)

    def Local_write(self):
        response = [0x01, 0x10, 0x01, 0x00, 0x01, 0x8d]
        new_data = self.rx_buffer[4:-2]
        del self.registers[self.rx_buffer[2]]
        self.registers[self.rx_buffer[2]] = new_data
        self.ser.write(response)

    def Ok_response(self):
        if self.rx_buffer[1] == 0x04:
            response = [0x01, 0x04, 0x01, 0x00, 0x41, 0x89]
        else:
            response = [0x01, 0x05, 0x01, 0x00, 0x10, 0x49]
        self.ser.write(response)
        print(response)

    def Remote(self, frame):
        response = []
        response.append(frame[0])
        response.append(0x05)
        for i in self.registers[0]:
            response.append(i)
        for i in frame[10:-2]:
            response.append(i)
        crc_append(response)
        self.ser.write(response)
        print(response)

    def Reset(self):
        response = [0x01, 0x06, 0x01, 0xF0, 0xE0, 0x0D]
        self.ser.write(response)
        print(response)

    def Turn_off(self):
        self.ser.close()


def main():
    """
    Create ports
    """
    Coordinator = Mrf('COM2')
    Cabinet = Mrf('COM5')

    while 1:
        resp = Cabinet.New_data()
        if resp == 'Remote':
            Coordinator.remote(Cabinet.rx_buffer)

        elif resp == 'Error':
            print("Error")
            Cabinet.Turn_off()
            break

        resp = Coordinator.New_data()
        if resp == 'Remote':
            Cabinet.remote(Coordinator.rx_buffer)

        elif resp == 'Error':
            print("Error")
            Coordinator.Turn_off()
            break


if __name__ == '__main__':
    main()






