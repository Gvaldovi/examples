'Ejemplo 1'
#from queue import Queue


#my_queue = Queue(maxsize=0)
#my_queue.put(1)
#my_queue.put(2)
#my_queue.put(3)
#print(my_queue.get())
#print(my_queue.qsize())
#my_queue.task_done()
# Outputs: 1

'Ejemplo 2'
#from queue import Queue


#def do_stuff(q):
    #while not q.empty():
        #print(q.get())
        #q.task_done()

#q = Queue(maxsize=0)

#for x in range(20):
    #q.put(x)

#do_stuff(q)

'Ejemplo 3. Con threads'
from queue import Queue
from threading import Thread


def do_stuff(q):
    while True:
        print(q.get())
        q.task_done()

q = Queue(maxsize=0)
num_threads = 10

for i in range(num_threads):
    worker = Thread(target=do_stuff, args=(q,))
    worker.setDaemon(True)
    worker.start()

for x in range(100):
    q.put(x)

q.join()