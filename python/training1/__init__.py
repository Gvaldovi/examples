# -*- coding: utf-8 -*-

#import Protocol
#abc = Protocol.Mrf('COM4', 19200, 'even', [0]*8)

#from Protocol import Mrf
#abc = Mrf('COM4', 19200, 'even', [0]*8)

#import time
#import struct
#import sys
#sys.path.append('C:\\Users\\Gerardo\\Documents\\Ninja Projects\\Protocols')
#print(sys.path)


#from Protocol import IkCollector

''' Convertir de string hex a string'''
#text1 = '30 31 53 4D 45 49 30 31'
#text2 = ''
#print(ord('0'))
#print(chr(48))
#for i in text1.split():
    #text2 += chr(int(i, 16)) # + ' '
    ##Resp.append(int(i, 16))
#print(text2)
#abc = IkCollector('COM4')

''' Convertir de arreglo hex a string hex'''
#tx = [0x24, 0xDA, 0xB6]
#tx[2] += 1
#print(tx)
#st = ''
#for i in tx:
    #st += str(hex(i)[2:].zfill(2)) + ' '
#st = st.upper()
#print(st)



#lista = [i for i in range(17)]
#print(lista)
#ls = lista[3:3+lista[2]]
#print(ls)
#lista = lista[::-1]
#print(lista)
#resp = []

#for i in lista[8:16]:
    #resp.append(i)
#for i in lista[:8]:
    #resp.append(i)
#resp.append(lista[16])
#print(resp)

#Coord_cmd = [i for i in range(16, 32)]
#Meter_cmd = [1, 2, 3]
#Display_cmd = [0x31, 0x32]
#test = 31
#if test in Coord_cmd or test in Meter_cmd or test in Display_cmd:
    #print('Si esta')
#else:
    #print('Pelas')

''' Convertir de string hex a arreglo hex'''
#import struct

#a = '0xFFFF'
#b = '127'
#c = '340'

#a1 = int(a, 0)
#b1 = int(b)
#c1 = int(c)

#a2 = struct.unpack("2B", struct.pack("H", a1))
#b2 = struct.unpack("2B", struct.pack("H", b1))
#c2 = struct.unpack("2B", struct.pack("H", c1))
#print(a2)
#print(b2)
#print(c2)

#print('Otra forma')
#s = '0'
#st = '10000'
#if len(st) % 2 != 0:
    #s += st
#else:
    #s = st
#print(bytearray.fromhex(s))

#print('Otra forma 2')
#x = '0'
#x1 = int(x, 0)
#x2 = struct.unpack("4B", struct.pack("H", x1))
#print(x2)

#s19 = 'S3150800300000200020ED330008DD330008E133000816'
#if 'S7' in s19:
    #s = s19[2:]
    #print(bytearray.fromhex(s))
#else:
    #print('pelas')


''' Convertir de epoch a arreglo hex'''
#import time
##from datetime import datetime
#import struct
##import sys

#epoch = int(time.time())
#print('Epoch:', epoch)
#print('EpochHex: ', hex(epoch))
#epoch -= 946684800
#print('EpochHex 2000: ', hex(epoch))

#timezone = int(time.altzone * -1)
#print('Timezone: ', timezone)
##TZb = timezone.to_bytes(4, byteorder='big')
##print(TZb)


#Epoch2000 = struct.unpack("4B", struct.pack("I", epoch))
#print(Epoch2000)
#print(epoch.to_bytes(4, byteorder='big'))


#data = []
#for i in Epoch2000[::-1]:
    #data.append(i)
#print(data)

#s = bytearray(epoch)
#print(s)

#crc = crc_calc(array, 0xFFFF)                        # Calculate crc
#crc1 = struct.unpack("2B", struct.pack("H", crc))    # Convert crc to bytes
#array.append(crc1[0])                                # Append bytes to array
#array.append(crc1[1])
#return array

#result = bytearray.fromhex(hex(epoch))
#print(result)

#test = [0, 1, 2, 3, 4]

#for i in test:
    #print(i)

''' Convertir de int a arreglo hex'''
#import struct

#array = []
#a = 1134

#p = struct.pack("H", a)
#print(p)
#p1 = struct.unpack("2B", p)
#print(p1)

#b = "{0:0{1}x}".format(a, 8)
#print(b)
#buf = (bytearray.fromhex(b))
#print(buf)
#for i in buf:
    #array.append(i)

#print(array)

#for i in b.split():
    #print(i)
    #array.append(hex(i,0))
#print(array)
#x1 = int(b, 0)
#x2 = struct.unpack("4B", struct.pack("H", x1))
#print(x2)

''' Convertir string hex a arreglo hex'''
#arr = [0, 0]
#a = '00 01 02 7F'
#print(a)
##print(hex(a))
#buf = (bytearray.fromhex(a))
#print(buf)
#for i in buf:
    #arr.append(i)
#print(arr)

''' Convert hex list to int '''
#a = [1, 2, 3]
#print(a)
#x = (70024).to_bytes(3, byteorder='big')
#print(x)
#b = x[1:3]
#print(b)
#y = int.from_bytes(x, byteorder='big')
#print(y)
#print(int.from_bytes(b, byteorder='big'))


''' Convert string to hex array '''
#ex = 'ext'
#print(ex)
#b = bytes(ex, 'utf-8')
#print(b)
#for i in b:
    #print(i)

''' Crear espacios con ceros antes de un numero '''
#a = '000METER'
#print(a)
#b = int(a[:3])
#print(b)
#b += 1
#c = "{:03d}".format(b) + a[3:]
#print(c)

''' Buscar cadena en lista de cadenas '''
response = []
l = ['000METER  0x00000000',
     '001METER  0xFFFFFFFF',
     '002METER  0xEEEEEEEE']
print(l)
c = bytes([0x30, 0x30, 0x30, 0x4D, 0x45, 0x54, 0x45, 0x52])
print(c)

for i in c:
    response.append(i)

d = c.decode('utf-8')
print(d)

for m in l:
    print(m)
    if d in m:
        m1 = bytearray.fromhex(m[12:])
        print(m1)
        for i in m1:
            response.append(i)
print(response)

''' Threading '''
#import threading
#import time


#def worker(num):
    #"""thread worker function"""
    #print('Worker: ', num)
    #return

#threads = []
#for i in range(5):
    #t = threading.Thread(target=worker, args=(i,))
    #threads.append(t)
    #t.start()

#def doit(stop_event, arg):
    #while not stop_event:
        #print ("working on %s" % arg)
    #print("Stopping as you wish.")

#pill2kill = threading.Event()
#t = threading.Thread(target=doit, args=(pill2kill, "task"))
#t.start()
#time.sleep(1)
#pill2kill.set()
#t.join()

''' Conversiones '''
    # Bytes to string
#b = bytes([0x30, 0x30, 0x30, 0x4D, 0x45, 0x54, 0x45, 0x52])
#r = b.decode('utf-8')
    # String hex to byte array
#s = '00FF00FF'
#r = bytearray.fromhex(s)
    # String int to string hex
#a = '001'
#r = ' '.join("{:02x}".format(ord(c)) for c in a)
#print(r)

#a = bytes([0x55, 0xDD, 0x39, 0x31, 0x31, 0x41, 0x42, 0x43, 0x44, 0x08, 0x30, 0x39, 0x37, 0x54, 0x54, 0x45, 0x80])
#if a[0] == 0x55 and a[1] == 0xDD:
    #Display = a[3:9]
    #cmd = a[9]
    #Meter = a[10:16]
    #st = Display.decode('utf-8')
    #st +=
    #print(Display)
