'''
    Udp socket Client
'''
import socket   #for sockets
import sys      #for exit
import datetime

# create dgram udp socket
try:
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    print('Socket created')
except socket.error:
    print('Failed to create socket')
    sys.exit()
 
host = '54.71.49.12'
#host = '127.0.0.1'
port = 5006
 
while True:
    msg = input('Enter message to send : ')

    if msg == 'exit':
        break 

    try :
        # Set the message entered
        #s.sendto(msg.encode(), (host, port))

        # Send raw bytes
        raw = "15 07 00 01 03 69 00 0A 00 01 3F F0 B7 89 01 71 00 00 00 00 00 00 00 00 0C 00 13 FD"
        raw_hex = bytes.fromhex(raw)
        s.sendto(raw_hex, (host, port))

        # receive data from client (data, addr)
        d = s.recvfrom(1024)
        data = d[0]
        addr = d[1]
         
        # Print message as string
        r = " ".join("{:02x}".format(c) for c in data)
        date = datetime.datetime.now() - datetime.timedelta(hours = 6)
        print(date.strftime('%Y-%m-%d %H:%M:%S'), end='')
        print(' ', addr, end='')
        print(' IN  <<:', r)

    except socket.error as msg:
        print('ERROR : ', msg)
        break

s.close()        

      