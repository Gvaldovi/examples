'''
    Simple UDP socket Server
'''
import socket
import sys
import datetime
 
HOST = ''   # Symbolic name meaning all available interfaces
PORT = 5006 # Arbitrary non-privileged port

# Datagram (udp) socket
try :
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    print('Socket created')
except socket.error as msg :
    print('Failed to create socket. ERROR : ', msg)
    sys.exit()
 
# Bind socket to local host and port
try:
    s.bind((HOST, PORT))
except socket.error as msg:
    print('Bind failed. ERROR : ',msg)
    sys.exit()
     
print('Socket bind complete')
unsolicited = True  # Variable for sending an OTAC only once.

# Now keep talking with the client
while True:
    # receive data from client (data, addr)
    d = s.recvfrom(1024)

    data = d[0]
    addr = d[1]

    # Print message as string
    r = " ".join("{:02x}".format(c) for c in data)
    date = datetime.datetime.now() - datetime.timedelta(hours = 6)
    print(date.strftime('%Y-%m-%d %H:%M:%S'), end='')
    print(' ', addr, end='')
    print(' IN  <<:', r)

    if unsolicited == True:
        resp = bytearray(data[:24])
        resp[3] = 2
        resp[4] = 0

        # Unsolicited OTAC. Get values of 0x48A, 0x4B1 and 0x44D
        #resp[5] = 6
        #resp.extend(b'\x04\x8A\x04\xB1\x04\x4D') 

        # Unsolicited OTAC. Set values of 0x48A = 1, 0x4B1 = 100 and 0x4D5 = 2,0,0,5
        resp[5] = 33
        resp.extend(b'\x14\x8A\x00\x00\x00\x01\x14\xB1\x00\x00\x00\x64\x84\xD5\x01\x00\x04\x00\x00\x00\x02\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x05') 
        unsolicited = False

    else:
        # Respond with OK
        resp = bytearray(data[:24])
        resp[3] = 2
        resp[4] = 0
        resp[5] = 0

    # Print out response
    r1 = " ".join("{:02x}".format(c) for c in resp)
    date = datetime.datetime.now() - datetime.timedelta(hours = 6)
    print(date.strftime('%Y-%m-%d %H:%M:%S'), end='')
    print(' OUT >>:', r1)

    s.sendto(resp , addr)
    
s.close()