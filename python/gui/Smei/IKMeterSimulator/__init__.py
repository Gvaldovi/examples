# -*- coding: utf-8 -*-
"""
GUI for Test
"""
import wx
import datetime
from Meters import IkMeters


class Meters_simulator(wx.Frame):

    def __init__(self, parent, title):
        super(Meters_simulator, self).__init__(parent, title=title, size=(1200, 800))

        self.InitGUI()
        self.Centre()
        self.Show()

    def InitGUI(self):
        # GUI creation with wxFormBuilder
        bSizer = wx.BoxSizer( wx.VERTICAL )

        self.m_panel1 = wx.Panel( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
        bSizer11 = wx.BoxSizer( wx.VERTICAL )

        bSizer1 = wx.BoxSizer( wx.HORIZONTAL )

        bSizer1.SetMinSize( wx.Size( -1,50 ) )
        self.m_staticText11 = wx.StaticText( self.m_panel1, wx.ID_ANY, u"Puerto", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText11.Wrap( -1 )
        bSizer1.Add( self.m_staticText11, 0, wx.ALIGN_CENTER|wx.ALL, 10 )

        self.m_textCtrlPort = wx.TextCtrl( self.m_panel1, wx.ID_ANY, u"COM68", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_textCtrlPort.SetMaxSize( wx.Size( 60,-1 ) )

        bSizer1.Add( self.m_textCtrlPort, 0, wx.ALIGN_CENTER|wx.RIGHT, 50 )

        self.m_buttonInit = wx.Button( self.m_panel1, wx.ID_ANY, u"Iniciar", wx.DefaultPosition, wx.DefaultSize, 0 )
        bSizer1.Add( self.m_buttonInit, 0, wx.ALIGN_CENTER|wx.RIGHT, 15 )

        self.m_buttonStop = wx.Button( self.m_panel1, wx.ID_ANY, u"Parar", wx.DefaultPosition, wx.DefaultSize, 0 )
        bSizer1.Add( self.m_buttonStop, 0, wx.ALIGN_CENTER_VERTICAL|wx.RIGHT, 15 )

        self.m_buttonExit = wx.Button( self.m_panel1, wx.ID_ANY, u"Salir", wx.DefaultPosition, wx.DefaultSize, 0 )
        bSizer1.Add( self.m_buttonExit, 0, wx.ALIGN_CENTER_VERTICAL|wx.RIGHT, 50 )


        bSizer11.Add( bSizer1, 1, wx.ALIGN_CENTER_HORIZONTAL|wx.EXPAND|wx.RIGHT, 5 )

        self.m_staticline2 = wx.StaticLine( self.m_panel1, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.LI_HORIZONTAL )
        bSizer11.Add( self.m_staticline2, 0, wx.EXPAND |wx.ALL, 5 )

        bSizer4 = wx.BoxSizer( wx.HORIZONTAL )

        self.m_staticText2 = wx.StaticText( self.m_panel1, wx.ID_ANY, u"Medidor", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText2.Wrap( -1 )
        bSizer4.Add( self.m_staticText2, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )

        self.m_textCtrlMeter = wx.TextCtrl( self.m_panel1, wx.ID_ANY, u"000METER", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_textCtrlMeter.SetMaxSize( wx.Size( 70,-1 ) )

        bSizer4.Add( self.m_textCtrlMeter, 0, wx.ALIGN_CENTER|wx.RIGHT, 5 )

        self.m_staticText3 = wx.StaticText( self.m_panel1, wx.ID_ANY, u"(8 caracteres, 3 primeros numero hex)", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText3.Wrap( -1 )
        bSizer4.Add( self.m_staticText3, 0, wx.ALIGN_CENTER|wx.RIGHT, 50 )

        self.m_staticText4 = wx.StaticText( self.m_panel1, wx.ID_ANY, u"Numero", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText4.Wrap( -1 )
        bSizer4.Add( self.m_staticText4, 0, wx.ALIGN_CENTER|wx.ALL, 5 )

        self.m_textCtrlMeterNum = wx.TextCtrl( self.m_panel1, wx.ID_ANY, u"4", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_textCtrlMeterNum.SetMaxSize( wx.Size( 30,-1 ) )

        bSizer4.Add( self.m_textCtrlMeterNum, 0, wx.ALL, 5 )


        bSizer11.Add( bSizer4, 1, wx.EXPAND, 5 )

        self.m_staticline21 = wx.StaticLine( self.m_panel1, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.LI_HORIZONTAL )
        bSizer11.Add( self.m_staticline21, 0, wx.EXPAND |wx.ALL, 5 )

        bSizer14 = wx.BoxSizer( wx.HORIZONTAL )

        bSizer15 = wx.BoxSizer( wx.HORIZONTAL )

        self.m_staticText9 = wx.StaticText( self.m_panel1, wx.ID_ANY, u"Medidores", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText9.Wrap( -1 )
        bSizer15.Add( self.m_staticText9, 0, wx.ALIGN_CENTER|wx.ALL, 5 )

        self.m_buttonLoadMeter = wx.Button( self.m_panel1, wx.ID_ANY, u"Cargar", wx.DefaultPosition, wx.DefaultSize, 0 )
        bSizer15.Add( self.m_buttonLoadMeter, 0, wx.ALL, 5 )


        bSizer14.Add( bSizer15, 0, wx.EXPAND, 5 )

        bSizer17 = wx.BoxSizer( wx.VERTICAL )

        self.m_buttonLogClear = wx.Button( self.m_panel1, wx.ID_ANY, u"Limpiar Log", wx.DefaultPosition, wx.DefaultSize, 0 )
        bSizer17.Add( self.m_buttonLogClear, 0, wx.ALL|wx.ALIGN_CENTER_HORIZONTAL, 5 )


        bSizer14.Add( bSizer17, 1, wx.EXPAND, 5 )


        bSizer11.Add( bSizer14, 1, wx.EXPAND, 5 )

        bSizer12 = wx.BoxSizer( wx.HORIZONTAL )

        self.m_textCtrlMeters = wx.TextCtrl( self.m_panel1, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.TE_MULTILINE )
        self.m_textCtrlMeters.SetFont( wx.Font( 9, 75, 90, 90, False, "Courier New" ) )
        self.m_textCtrlMeters.SetMinSize( wx.Size( 200,-1 ) )

        bSizer12.Add( self.m_textCtrlMeters, 0, wx.ALL|wx.EXPAND, 5 )

        self.m_textCtrlLog = wx.TextCtrl( self.m_panel1, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.TE_MULTILINE )
        self.m_textCtrlLog.SetFont( wx.Font( 9, 75, 90, 90, False, "Courier New" ) )
        self.m_textCtrlLog.SetMinSize( wx.Size( -1,600 ) )

        bSizer12.Add( self.m_textCtrlLog, 1, wx.ALL|wx.EXPAND, 5 )


        bSizer11.Add( bSizer12, 1, wx.EXPAND, 5 )


        self.m_panel1.SetSizer( bSizer11 )
        self.m_panel1.Layout()
        bSizer11.Fit( self.m_panel1 )
        bSizer.Add( self.m_panel1, 1, wx.EXPAND |wx.ALL, 5 )


        self.SetSizer( bSizer )
        self.Layout()

        self.Centre( wx.BOTH )

        # Bindings
        self.m_buttonInit.Bind(wx.EVT_BUTTON, self.Start)
        self.m_buttonStop.Bind(wx.EVT_BUTTON, self.Stop)
        self.m_buttonExit.Bind(wx.EVT_BUTTON, self.Exit)
        self.m_buttonLoadMeter.Bind(wx.EVT_BUTTON, self.MetersTableSet)
        self.m_buttonLogClear.Bind(wx.EVT_BUTTON, self.Log_clear)

        # Start condition
        self.m_buttonStop.Disable()

        # Timer enable
        self.timer = wx.Timer(self)
        self.Bind(wx.EVT_TIMER, self.Rx_data, self.timer)

    def Start(self, e):
        # Create a Meter serial module
        port = self.m_textCtrlPort.GetValue()
        self.IkProtocol = IkMeters(port, 19200, 'even')

        # Verify port status
        if self.IkProtocol.PortStatus():
            self.Log_frame(port + ' 8E1@19200', 'IKMeters START', True)

            # Initialize buttons and timer
            self.m_buttonInit.Disable()
            self.m_buttonStop.Enable()
            self.timer.Start(100)
            self.RespFlag = False

            # Loas Meter's table
            self.MetersTableGet()
        else:
            self.Log_frame(port + ' Error in port', '', True)

    def Stop(self, e):
        self.IkProtocol.Close()
        self.timer.Stop()
        self.m_buttonInit.Enable()
        self.m_buttonStop.Disable()
        self.Log_frame('Port closed', '', True)

    def Rx_data(self, e):
        self.timer.Stop()

        if self.IkProtocol.Poll() is True:
            # Print the frame received
            rx = self.IkProtocol.Get_buffer()
            self.Log_frame(rx, 'Recibido', False)

            # Response
            if self.IkProtocol.Get_status() is True:
                Num = int(self.m_textCtrlMeterNum.GetValue())
                Name = self.m_textCtrlMeter.GetValue()
                self.Log_frame(self.IkProtocol.IkCmd_in(rx, Num, Name), 'Enviado', True)
                self.MetersTableGet()

                # Update name and num if command was read ID
                if rx[8] == 0x04 and Num > 0:
                    self.Update(Name, Num)

            else:
                self.Log_frame(self.IkProtocol.Get_status(), 'Error', True)

        self.timer.Start(100)

    def MetersTableGet(self):
        self.m_textCtrlMeters.Clear()
        Meters = self.IkProtocol.TableGet()
        self.m_textCtrlMeters.AppendText('\n'.join(Meters))

    def MetersTableSet(self, e):
        Meters = self.m_textCtrlMeters.GetValue()
        self.IkProtocol.TableSet(Meters.splitlines())

    def Update(self, Name, Num):
        Num -= 1
        a = int(Name[:3])
        a += 1
        Name = "{:03d}".format(a) + Name[3:]
        self.m_textCtrlMeterNum.SetValue(str(Num))
        self.m_textCtrlMeter.SetValue(Name)

    def Log_frame(self, lst, name='', dspace=False):
        if lst != '':
            # Review if type is str or list
            if type(lst) is str:
                st = lst
            else:
                size = len(lst)
                st = ''
                for i in lst:
                    st += str(hex(i)[2:].zfill(2)) + ' '
                st = st.upper()
                st += '-- '
                st += str(size)

            # Select one or two spaces
            if dspace is True:
                st += '\n\n'
            else:
                st += '\n'
            # Form data string
            date = datetime.datetime.now().strftime("%d/%b/%y %I:%M:%S:%f")
            self.m_textCtrlLog.AppendText(date)
            self.m_textCtrlLog.AppendText('  ')
            self.m_textCtrlLog.AppendText(name)
            space = ' ' * (15 - len(name))
            self.m_textCtrlLog.AppendText(space)
            self.m_textCtrlLog.AppendText(st)
        else:
            self.m_textCtrlLog.AppendText('\n')

    def Log_clear(self, e):
        self.m_textCtrlLog.Clear()

    def Exit(self, e):
        try:
            print('Port open: ', self.IkProtocol.Close())
        except:
            print('Port never used')
        self.timer.Stop()
        self.Close()


if __name__ == '__main__':
    app = wx.App()
    Meters_simulator(None, title='Meters Simulator')
    app.MainLoop()