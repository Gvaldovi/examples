# -*- coding: utf-8 -*-
"""
Clase para simular medidores IK.
"""
from Port import Serial


class IkMeters(Serial):

    def __init__(self, port, baud, parity):
        # Initialize serial port
        Serial.__init__(self, port, baud, parity)

        """
        Meters table
        """
        self.Meters = []
        self.pos = 0

    def IkCmd_in(self, frame, num, name):
        # Review onlye valid commands

        if frame[8] == 0x03:    # Read
            response = []
            for i in frame[:9]:
                response.append(i)

            # Get address
            address = int.from_bytes(frame[9:11], byteorder='big')
            word_num = int.from_bytes(frame[11:13], byteorder='big')

            if address == 0x02:       # Linker
                response.append(4)
                linker = [0xFF, 0xFF, 0xFF, 0xFF]
                for i in linker:
                    response.append(i)
            elif address == 0x8:     # Extended ID
                Id = bytes(name, 'utf-8')
                response.append(16)

                for i in range(8):
                    response.append(0xFF)
                for i in Id:
                    response.append(i)
            ###########
            # Display #
            ###########
            elif address == 0x10:
                Id = bytes(name, 'utf-8')
                Dpy = bytes('SMEI00', 'utf-8')
                response.append(8)

                # Display valid
                for i in Dpy:
                    response.append(i)
                response.append(Id[6])
                response.append(Id[7])

                # Display invalid
                #for i in range(8):
                    #response.append(0xFF)
            elif address == 0x1E:  # Version
                response.append(2)
                response.append(4)
                response.append(0)
            elif address == 0x1F:  # Position
                response.append(2)
                response.append(0)
                if self.pos > 3:
                    self.pos = 0
                response.append(self.pos)
                self.pos += 1
            elif address == 0x20:  # Type
                response.append(2)
                response.append(0)
                response.append(0x10)

            ############
            # Readings #
            ############
            elif address == 0x27:
                Readings = [0xFF, 0xFF, 0xB3, 0xD7, 0x00, 0x00, 0x08, 0x37, 0x82, 0x02,
                            0x00, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x07, 0x40, 0x49,
                            0x00, 0x00, 0xEA, 0x4C, 0x40, 0x00, 0x00, 0x00, 0x00, 0x00,
                            0xD0, 0x0B, 0x89, 0xB6, 0x00, 0x00, 0x0B, 0x2B, 0x00, 0x00, 0x00, 0x00]
                m = frame[:8]
                flag = 0

                response.append(62)

                # Find meter, if it exist then mark flag
                meter = m.decode('utf-8')
                for x in self.Meters:
                    if meter in x:
                        flag = 1
                        #m1 = bytearray.fromhex(x[12:])
                        #print(m1)
                        #for i in m1:
                            #response.append(i)

                if flag == 0:
                    # Add meter to table
                    meter += '  0x00000000'
                    self.Meters.append(meter)
                    # Sort meter table
                    self.Meters.sort()

                for i in Readings:
                    response.append(i)

            ##########
            # Alarms #
            ##########
            elif address == 0x2B:
                response.append(4)
                response.append(1)
                response.append(1)
                response.append(1)
                response.append(1)

            ##########
            # EVENTS #
            ##########
            elif address >= 0x6D and address <= 0x134:
                index = address - 0x6D
                event = [0x61, 0x00, 0x41, 0xFF, 0xFF, 0xFF, 0xFF]
                size = int(word_num / 2)
                response.append(size * 10)

                for i in range(size):
                    response.append(i + index)
                    for j in event:
                        response.append(j)

            #######################
            # CONFIG LOAD PROFILE #
            #######################
            elif address == 0x56:
                response.append(2)
                response.append(0x55)
                response.append(0xAA)

            ################
            # LOAD PROFILE #
            ################
            elif address >= 0x135 and address <= 0x46F4:
                if address < 0x2415:
                    index = address - 0x135
                    month = 1
                else:
                    index = address - 0x2415
                    month = 0

                day = int(index / 288) + 1
                hour = int(index / 12) % 24
                print(month, day, hour)

                profile = [0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF]
                response.append(120)

                for i in range(12):
                    response.append(month)
                    response.append(day)
                    response.append(hour)
                    response.append(i)
                    for j in profile:
                        response.append(j)

            ##############
            # TOU ENERGY #
            ##############
            elif address >= 0x46F5 and address <= 0x4714:
                index = address - 0x46F5
                index %= 8
                response.append(32)

                for i in range(8):
                    response.append(index)
                    response.append(i)
                    response.append(0xFF)
                    response.append(0xFF)

            ##############
            # TOU DEMAND #
            ##############
            elif address >= 0x4715 and address <= 0x4754:
                index = address - 0x4715
                index %= 16
                response.append(64)
                for i in range(16):
                    response.append(index)
                    response.append(i)
                    response.append(0xFF)
                    response.append(0xFF)

            ###########
            # DEFAULT #
            ###########
            else:
                size_b = word_num * 2
                response.append(size_b)
                for i in range(size_b):
                    response.append(i)

            return self.Print(response)

        elif frame[8] == 0x04:  # Read ID by position
            response = []
            for i in frame[:9]:
                response.append(i)

            if num != 0:
                # Add meter to table
                field = name + '  0x00000000'
                self.Meters.append(field)
                response.append(8)
                nameHex = bytes(name, 'utf-8')
                for i in nameHex:
                    response.append(i)
            else:
                return 'No meters'

            return self.Print(response)

        elif frame[8] == 0x10:  # Write mult
            response = []
            flag = 0
            # Get address
            address = int.from_bytes(frame[9:11], byteorder='big')

            if address == 0x1A:    # Commands
                # Get meter
                m = frame[:8]
                meter = m.decode('utf-8')

                # Find meter, if it exist then mark flag
                for i, x in enumerate(self.Meters):
                    if meter in x:
                        flag = 1
                        x = x[:-1]
                        if frame[13] == 0x80:    # Disconnect
                            x += '1'
                        else:                    # Connect
                            x += '0'
                        # Copy new flags to table
                        self.Meters[i] = x

                if flag == 0:
                    return 'No meter'

            for i in frame[:-2]:
                response.append(i)
            return self.Print(response)

        elif frame[8] == 0x71:  # Write configuration
            response = []

            for i in frame[:13]:
                response.append(i)
            return self.Print(response)

        elif frame[8] == 0x72:  # Read configuration
            response = []

            for i in frame[:]:
                response.append(i)

            if(frame[10] == 3):
                for i in range(73):
                    response.append(i)
            else:
                for i in range(20):
                    response.append(i)
            return self.Print(response)

        else:
            return 'Error in command'

    def TableGet(self):
        return self.Meters

    def TableSet(self, newMeters):
        self.Meters = newMeters

