# -*- coding: utf-8 -*-
"""
GUI for Test
"""
import wx
import datetime
import serial
import threading
from queue import Queue


class Sniffer485(wx.Frame):

    def __init__(self, parent, title):
        super(Sniffer485, self).__init__(parent, title=title, size=(1360, 380))

        self.InitGUI()
        self.Centre()
        self.Show()

    def InitGUI(self):
        # GUI creation with wxFormBuilder
        bSizer = wx.BoxSizer( wx.VERTICAL )

        self.m_panel1 = wx.Panel( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
        bSizer11 = wx.BoxSizer( wx.VERTICAL )

        bSizer1 = wx.BoxSizer( wx.HORIZONTAL )

        self.m_staticText11 = wx.StaticText( self.m_panel1, wx.ID_ANY, u"Puerto", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText11.Wrap( -1 )
        bSizer1.Add( self.m_staticText11, 0, wx.ALIGN_CENTER|wx.ALL, 10 )

        self.m_textCtrlPort = wx.TextCtrl( self.m_panel1, wx.ID_ANY, u"COM68", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_textCtrlPort.SetMaxSize( wx.Size( 60,-1 ) )

        bSizer1.Add( self.m_textCtrlPort, 0, wx.ALIGN_CENTER|wx.RIGHT, 50 )

        self.m_buttonInit = wx.Button( self.m_panel1, wx.ID_ANY, u"Iniciar", wx.DefaultPosition, wx.DefaultSize, 0 )
        bSizer1.Add( self.m_buttonInit, 0, wx.ALIGN_CENTER|wx.RIGHT, 15 )

        self.m_buttonStop = wx.Button( self.m_panel1, wx.ID_ANY, u"Parar", wx.DefaultPosition, wx.DefaultSize, 0 )
        bSizer1.Add( self.m_buttonStop, 0, wx.ALIGN_CENTER_VERTICAL|wx.RIGHT, 15 )

        self.m_buttonExit = wx.Button( self.m_panel1, wx.ID_ANY, u"Salir", wx.DefaultPosition, wx.DefaultSize, 0 )
        bSizer1.Add( self.m_buttonExit, 0, wx.ALIGN_CENTER_VERTICAL|wx.RIGHT, 50 )


        bSizer11.Add( bSizer1, 0, 0, 50 )

        self.m_staticline2 = wx.StaticLine( self.m_panel1, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.LI_HORIZONTAL )
        bSizer11.Add( self.m_staticline2, 0, wx.EXPAND |wx.ALL, 5 )

        self.m_buttonLogClear = wx.Button( self.m_panel1, wx.ID_ANY, u"Limpiar Log", wx.DefaultPosition, wx.DefaultSize, 0 )
        bSizer11.Add( self.m_buttonLogClear, 0, wx.ALL|wx.ALIGN_CENTER_HORIZONTAL, 5 )

        self.m_textCtrlLog = wx.TextCtrl( self.m_panel1, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.TE_MULTILINE )
        self.m_textCtrlLog.SetFont( wx.Font( 9, 75, 90, 90, False, "Courier New" ) )
        self.m_textCtrlLog.SetMinSize( wx.Size( -1,600 ) )

        bSizer11.Add( self.m_textCtrlLog, 1, wx.ALL|wx.EXPAND, 5 )


        self.m_panel1.SetSizer( bSizer11 )
        self.m_panel1.Layout()
        bSizer11.Fit( self.m_panel1 )
        bSizer.Add( self.m_panel1, 1, wx.EXPAND |wx.ALL, 5 )


        self.SetSizer( bSizer )
        self.Layout()

        self.Centre( wx.BOTH )

        # Bindings
        self.m_buttonInit.Bind(wx.EVT_BUTTON, self.Start)
        self.m_buttonStop.Bind(wx.EVT_BUTTON, self.Stop)
        self.m_buttonExit.Bind(wx.EVT_BUTTON, self.Quit)
        self.m_buttonLogClear.Bind(wx.EVT_BUTTON, self.Log_clear)

        # Start condition
        self.m_buttonStop.Disable()

    def Start(self, e):
        # Create a serial module
        port = self.m_textCtrlPort.GetValue()

        self.Ser = serial.Serial()
        self.Ser.port = port
        self.Ser.baudrate = 19200
        self.Ser.parity = serial.PARITY_EVEN
        self.Ser.timeout = 3
        #self.Ser.interCharTimeout = 0.25
        self.Ser.inter_byte_timeout = 0.025

        # Open port
        try:
            self.Ser.open()
        except:
            pass

        # Verify port status
        if self.Ser.isOpen():
            self.Log_frame(port + ' 8E1@19200', 'IK Sniffer RS485', True)

            # Create queue
            RxQueue = Queue(maxsize=0)

            # Create daemon 1, RxThread
            self.Thread1 = threading.Thread(name='RxThread', target=self.RxThread, args=(self.Ser, RxQueue, ))
            self.Thread1.setDaemon(True)
            self.Thread1.start()

            # Create daemon 2, RxOutThread
            self.Thread2 = threading.Thread(name='RxOutThread', target=self.CommandThread, args=(RxQueue, ))
            self.Thread2.setDaemon(True)
            self.Thread2.start()

            # Initialize buttons
            self.m_buttonInit.Disable()
            self.m_buttonStop.Enable()

            # Create variable for printing
            self.IkMsg = 0
        else:
            self.Log_frame(port + ' Error in port', '', True)

    def Stop(self, e):
        self.Ser.close()
        self.m_buttonInit.Enable()
        self.m_buttonStop.Disable()
        self.Log_frame('Port closed', '', True)

    def RxThread(self, Ser, out_queue):
        while True:
            try:
                data = Ser.read(1200)
                if len(data) > 3:
                    # Send through queue
                    out_queue.put(data)
                    #self.Rx_data(data)
            except:
                print('Thread finish')
                return

    def CommandThread(self, in_queue):
        # Review if frame is a notification
        while True:
            rx = in_queue.get()
            in_queue.task_done()

            if len(rx) < 11:
                st = 'Frame'
                self.Log_frame(rx, st, False, 25)

            #elif rx[0] == 0x55 and rx[1] == 0xDD:
                #dpy = rx[3:9]

                #if rx[9] == 0x01:
                    #cmd = 'Read'
                #elif rx[9] == 0x02:
                    #cmd = 'Msg'
                #elif rx[9] == 0x03:
                    #cmd = 'APB'
                #elif rx[9] == 0x04:
                    #cmd = 'Status'
                #elif rx[9] == 0x08:
                    #cmd = 'APA'
                #elif rx[9] == 0x09:
                    #cmd = 'DPA'
                #elif rx[9] == 0x0A:
                    #cmd = 'Force'
                #elif rx[9] == 0x0C:
                    #cmd = 'Erase'
                #else:
                    #cmd = 'Error'

                #try:
                    #st = dpy.decode('utf-8') + ' ' + cmd
                #except:
                    #st = cmd

                #self.Log_frame(rx, st, False, 25)

            else:
                mtr = rx[0:8]

                if rx[8] == 0x03:
                    cmd = 'R'
                elif rx[8] == 0x10:
                    cmd = 'W'
                else:
                    cmd = 'Error'

                try:
                    st = mtr.decode('utf-8') + ' ' + cmd
                except:
                    st = cmd

                self.Log_frame(rx, st, False, 25)

    def Log_frame(self, lst, name='', dspace=False, space=15):
        if lst != '':
            # Review if type is str or list
            if type(lst) is str:
                st = lst
            else:
                size = len(lst)
                st = ''
                for i in lst:
                    st += str(hex(i)[2:].zfill(2)) + ' '
                st = st.upper()
                st += '-- '
                st += str(size)

            # Select one or two spaces
            if dspace is True:
                st += '\n\n'
            else:
                st += '\n'

            # Form data string
            date = datetime.datetime.now().strftime("%d/%b/%y %I:%M:%S")
            self.m_textCtrlLog.AppendText(date)
            self.m_textCtrlLog.AppendText('  ')
            self.m_textCtrlLog.AppendText(name)
            space = ' ' * (space - len(name))
            self.m_textCtrlLog.AppendText(space)
            self.m_textCtrlLog.AppendText(st)
        else:
            self.m_textCtrlLog.AppendText('\n')

    def Log_clear(self, e):
        self.m_textCtrlLog.Clear()

    def Quit(self, e):
        try:
            #self.RxQueue.join()
            self.Ser.close()
            self.Thread1.join()
        except:
            print('Port open: Never used')
        self.Close()


if __name__ == '__main__':
    app = wx.App()
    Sniffer485(None, title='IK Sniffer RS485')
    app.MainLoop()
