"""
Protocol for ScorpioARM Chips Handler
"""
import serial
import time


class Chips(object):

    def __init__(self, port):
        """
        Initializes the serial connection to the port
        """
        try:
            self.ser = serial.Serial(
                port=port,
                baudrate=19200,
                parity=serial.PARITY_EVEN,
                stopbits=serial.STOPBITS_ONE,
                bytesize=serial.EIGHTBITS,
                timeout=1
            )

            print("Port: ", self.ser.isOpen())
        except:
            print("Error in port")

    def Write(self, memory, address, data):
        if len(data) < 256:
            array = [memory, 0]

            addr = bytearray.fromhex(address)
            for i in addr:
                array.append(i)

            d = bytearray.fromhex(data)
            array.append(0)
            array.append(len(d))
            for i in d:
                array.append(i)

            # Send response to COM
            self.ser.write(array)
            return array
        else:
            return 'Error: More than 256 bytes'

    def Read(self, memory, address, s):
        size = int(s)
        if size < 256:
            array = [memory, 1]

            addr = bytearray.fromhex(address)
            for i in addr:
                array.append(i)
            array.append(0)
            array.append(size)

            # Send response to COM
            self.ser.write(array)
            return array
        else:
            return 'Error: More than 256 bytes'

    def Erase(self, memory, address, mode):
        # This erase is made by sector (64Kb)
        array = [memory, 2]

        addr = bytearray.fromhex(address)
        for i in addr:
            array.append(i)
        array.append(0)
        array.append(mode)

        # Send response to COM
        self.ser.write(array)
        return array

    def Wait(self):
        time.sleep(0.3)    # 0.2 - Time for waiting up to 255 bytes @19200E1
        if self.ser.inWaiting():
            self.buffer = self.ser.read(self.ser.inWaiting())
            return self.buffer
        else:
            return 'Error: ScorpioARM no response'

    def Close(self):
        self.ser.close()
        print("Port: ", self.ser.isOpen())


#Chip = Chips('COM1')
#print(Chip.FlashR('AABBCCDD', 10))
#print(Chip.FlashW('AABBCCDD', [10, 11, 23]))
##print(Mem.FlashE([0, 0, 0, 0]))
#Chip.Close()

#Salir = "Salida del programa\n"
#Puerto = Serial("COM11")
#print(Puerto.Print([1, 2, 3, 4, 5, 6]))

#while 1:
    #if Puerto.Poll() is True:
        #Frame = Puerto.Get_buffer()
        #print(Frame)

        #if Frame in ['Bad Size', 'Bad CRC']:
            #Puerto.Print([1, 2, 3, 4, 5, 6])
        #else:
            #print('nose')
            #Puerto.Close()
            #break