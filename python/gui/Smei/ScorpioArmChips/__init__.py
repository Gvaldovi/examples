# -*- coding: utf-8 -*-
"""
GUI for ScorpioARM Memory handler
"""
import wx
import datetime
from MemProtocol import Chips


class ChipsGUI(wx.Frame):

    def __init__(self, parent, title):
        super(ChipsGUI, self).__init__(parent, title=title, size=(1100, 800))

        self.InitGUI()
        self.Centre()
        self.Show()

    def InitGUI(self):
        # GUI creation with wxFormBuilder
        #self.SetSizeHintsSz( wx.DefaultSize, wx.DefaultSize )

        bSizer = wx.BoxSizer( wx.VERTICAL )

        self.m_panel1 = wx.Panel( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
        bSizer11 = wx.BoxSizer( wx.VERTICAL )

        bSizer1 = wx.BoxSizer( wx.HORIZONTAL )

        bSizer1.SetMinSize( wx.Size( -1,50 ) )
        self.m_staticText11 = wx.StaticText( self.m_panel1, wx.ID_ANY, u"Puerto", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText11.Wrap( -1 )
        bSizer1.Add( self.m_staticText11, 0, wx.ALIGN_CENTER|wx.ALL, 10 )

        self.m_textCtrlPort = wx.TextCtrl( self.m_panel1, wx.ID_ANY, u"COM1", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_textCtrlPort.SetMaxSize( wx.Size( 60,-1 ) )

        bSizer1.Add( self.m_textCtrlPort, 0, wx.ALIGN_CENTER|wx.RIGHT, 50 )

        self.m_buttonInit = wx.Button( self.m_panel1, wx.ID_ANY, u"Iniciar", wx.DefaultPosition, wx.DefaultSize, 0 )
        bSizer1.Add( self.m_buttonInit, 0, wx.ALIGN_CENTER|wx.RIGHT, 15 )

        self.m_buttonStop = wx.Button( self.m_panel1, wx.ID_ANY, u"Parar", wx.DefaultPosition, wx.DefaultSize, 0 )
        bSizer1.Add( self.m_buttonStop, 0, wx.ALIGN_CENTER_VERTICAL|wx.RIGHT, 15 )

        self.m_buttonExit = wx.Button( self.m_panel1, wx.ID_ANY, u"Salir", wx.DefaultPosition, wx.DefaultSize, 0 )
        bSizer1.Add( self.m_buttonExit, 0, wx.ALIGN_CENTER_VERTICAL|wx.RIGHT, 50 )


        bSizer11.Add( bSizer1, 1, wx.ALIGN_CENTER_HORIZONTAL|wx.EXPAND|wx.RIGHT, 5 )

        self.m_staticline2 = wx.StaticLine( self.m_panel1, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.LI_HORIZONTAL )
        bSizer11.Add( self.m_staticline2, 0, wx.EXPAND |wx.ALL, 5 )

        bSizer21 = wx.BoxSizer( wx.HORIZONTAL )

        self.m_staticText19 = wx.StaticText( self.m_panel1, wx.ID_ANY, u"Seleccionar Chip", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText19.Wrap( -1 )
        bSizer21.Add( self.m_staticText19, 0, wx.EXPAND|wx.RIGHT, 40 )

        self.m_checkBoxFlash = wx.CheckBox( self.m_panel1, wx.ID_ANY, u"Flash", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_checkBoxFlash.SetValue(True)
        bSizer21.Add( self.m_checkBoxFlash, 0, wx.ALIGN_CENTER|wx.RIGHT, 30 )

        self.m_checkBoxFram = wx.CheckBox( self.m_panel1, wx.ID_ANY, u"FRAM", wx.DefaultPosition, wx.DefaultSize, 0 )
        bSizer21.Add( self.m_checkBoxFram, 0, wx.ALIGN_CENTER|wx.RIGHT, 30 )

        self.m_checkBoxEeprom = wx.CheckBox( self.m_panel1, wx.ID_ANY, u"EEPROM", wx.DefaultPosition, wx.DefaultSize, 0 )
        bSizer21.Add( self.m_checkBoxEeprom, 0, wx.ALIGN_CENTER|wx.RIGHT, 30 )

        self.m_checkBoxStpm = wx.CheckBox( self.m_panel1, wx.ID_ANY, u"STPM", wx.DefaultPosition, wx.DefaultSize, 0 )
        bSizer21.Add( self.m_checkBoxStpm, 0, wx.ALIGN_CENTER|wx.RIGHT, 40 )


        bSizer11.Add( bSizer21, 1, wx.ALL, 5 )

        self.m_staticline21 = wx.StaticLine( self.m_panel1, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.LI_HORIZONTAL )
        bSizer11.Add( self.m_staticline21, 0, wx.EXPAND |wx.ALL, 5 )

        bSizer30 = wx.BoxSizer( wx.HORIZONTAL )

        self.m_staticText14 = wx.StaticText( self.m_panel1, wx.ID_ANY, u"Direccion", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText14.Wrap( -1 )
        bSizer30.Add( self.m_staticText14, 0, wx.ALIGN_CENTER_VERTICAL|wx.LEFT|wx.RIGHT, 5 )

        self.m_textCtrlAddress = wx.TextCtrl( self.m_panel1, wx.ID_ANY, u"00002800", wx.DefaultPosition, wx.DefaultSize, 0 )
        bSizer30.Add( self.m_textCtrlAddress, 0, wx.ALIGN_CENTER|wx.RIGHT, 50 )

        self.m_staticText20 = wx.StaticText( self.m_panel1, wx.ID_ANY, u"Archivo (s19)", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText20.Wrap( -1 )
        bSizer30.Add( self.m_staticText20, 0, wx.ALIGN_CENTER_VERTICAL, 5 )

        self.m_filePickerFile = wx.FilePickerCtrl( self.m_panel1, wx.ID_ANY, wx.EmptyString, u"Select a file", u"*.srec", wx.DefaultPosition, wx.Size( 700,-1 ), wx.FLP_DEFAULT_STYLE )
        bSizer30.Add( self.m_filePickerFile, 0, wx.ALL, 5 )


        bSizer11.Add( bSizer30, 1, wx.EXPAND, 5 )

        bSizer27 = wx.BoxSizer( wx.HORIZONTAL )

        self.m_staticText18 = wx.StaticText( self.m_panel1, wx.ID_ANY, u"Borrar:", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText18.Wrap( -1 )
        bSizer27.Add( self.m_staticText18, 0, wx.ALL, 5 )

        self.m_checkBoxSector = wx.CheckBox( self.m_panel1, wx.ID_ANY, u"Sector", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_checkBoxSector.SetValue(True)
        bSizer27.Add( self.m_checkBoxSector, 0, wx.ALIGN_CENTER_VERTICAL|wx.LEFT, 18 )

        self.m_checkBoxBlock = wx.CheckBox( self.m_panel1, wx.ID_ANY, u"Bloque", wx.DefaultPosition, wx.DefaultSize, 0 )
        bSizer27.Add( self.m_checkBoxBlock, 0, wx.ALL, 5 )

        self.m_checkBoxAll = wx.CheckBox( self.m_panel1, wx.ID_ANY, u"Todo", wx.DefaultPosition, wx.DefaultSize, 0 )
        bSizer27.Add( self.m_checkBoxAll, 0, wx.ALL, 5 )


        bSizer11.Add( bSizer27, 1, wx.EXPAND, 5 )

        bSizer241 = wx.BoxSizer( wx.VERTICAL )

        self.m_staticText24 = wx.StaticText( self.m_panel1, wx.ID_ANY, u"Escribir:", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText24.Wrap( -1 )
        bSizer241.Add( self.m_staticText24, 0, wx.ALL, 5 )

        self.m_textCtrlWrite = wx.TextCtrl( self.m_panel1, wx.ID_ANY, u"00 01 02 03 04 FF", wx.DefaultPosition, wx.DefaultSize, 0 )
        bSizer241.Add( self.m_textCtrlWrite, 0, wx.ALL|wx.EXPAND, 5 )

        bSizer25 = wx.BoxSizer( wx.HORIZONTAL )


        bSizer241.Add( bSizer25, 1, wx.ALIGN_CENTER, 5 )


        bSizer11.Add( bSizer241, 1, wx.EXPAND, 5 )

        bSizer31 = wx.BoxSizer( wx.HORIZONTAL )

        self.m_staticText21 = wx.StaticText( self.m_panel1, wx.ID_ANY, u"Leer:", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText21.Wrap( -1 )
        bSizer31.Add( self.m_staticText21, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )

        self.m_staticText22 = wx.StaticText( self.m_panel1, wx.ID_ANY, u"Tamaño", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText22.Wrap( -1 )
        bSizer31.Add( self.m_staticText22, 0, wx.ALIGN_CENTER_VERTICAL|wx.LEFT, 30 )

        self.m_textCtrlReadSize = wx.TextCtrl( self.m_panel1, wx.ID_ANY, u"10", wx.DefaultPosition, wx.DefaultSize, 0 )
        bSizer31.Add( self.m_textCtrlReadSize, 0, wx.ALL, 5 )


        bSizer11.Add( bSizer31, 1, wx.EXPAND, 5 )

        self.m_staticline2111 = wx.StaticLine( self.m_panel1, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.LI_HORIZONTAL )
        bSizer11.Add( self.m_staticline2111, 0, wx.EXPAND |wx.ALL, 5 )

        bSizer33 = wx.BoxSizer( wx.HORIZONTAL )

        self.m_buttonErase = wx.Button( self.m_panel1, wx.ID_ANY, u"Borrar", wx.DefaultPosition, wx.DefaultSize, 0 )
        bSizer33.Add( self.m_buttonErase, 0, wx.ALL, 5 )

        self.m_buttonWrite = wx.Button( self.m_panel1, wx.ID_ANY, u"Escribir", wx.DefaultPosition, wx.DefaultSize, 0 )
        bSizer33.Add( self.m_buttonWrite, 0, wx.ALL, 5 )

        self.m_buttonWriteFromS19 = wx.Button( self.m_panel1, wx.ID_ANY, u"Escribir de s19", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_buttonWriteFromS19.Enable( False )

        bSizer33.Add( self.m_buttonWriteFromS19, 0, wx.ALL, 5 )

        self.m_buttonRead = wx.Button( self.m_panel1, wx.ID_ANY, u"Leer", wx.DefaultPosition, wx.DefaultSize, 0 )
        bSizer33.Add( self.m_buttonRead, 0, wx.ALL, 5 )

        self.m_buttonReadToS19 = wx.Button( self.m_panel1, wx.ID_ANY, u"Leer a s19", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_buttonReadToS19.Enable( False )

        bSizer33.Add( self.m_buttonReadToS19, 0, wx.ALL, 5 )


        bSizer11.Add( bSizer33, 1, wx.EXPAND, 5 )

        self.m_buttonLogClear = wx.Button( self.m_panel1, wx.ID_ANY, u"Limpiar Log", wx.DefaultPosition, wx.DefaultSize, 0 )
        bSizer11.Add( self.m_buttonLogClear, 0, wx.ALL|wx.ALIGN_CENTER_HORIZONTAL, 5 )

        self.m_textCtrlLog = wx.TextCtrl( self.m_panel1, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.TE_MULTILINE )
        self.m_textCtrlLog.SetFont( wx.Font( 9, 75, 90, 90, False, "Courier New" ) )
        self.m_textCtrlLog.SetMinSize( wx.Size( -1,600 ) )

        bSizer11.Add( self.m_textCtrlLog, 1, wx.ALL|wx.EXPAND, 5 )


        self.m_panel1.SetSizer( bSizer11 )
        self.m_panel1.Layout()
        bSizer11.Fit( self.m_panel1 )
        bSizer.Add( self.m_panel1, 1, wx.EXPAND |wx.ALL, 5 )


        self.SetSizer( bSizer )
        self.Layout()

        self.Centre( wx.BOTH )

        # Bindings
        self.m_buttonInit.Bind(wx.EVT_BUTTON, self.Start)
        self.m_buttonStop.Bind(wx.EVT_BUTTON, self.Stop)
        self.m_buttonExit.Bind(wx.EVT_BUTTON, self.Exit)
        self.m_buttonLogClear.Bind(wx.EVT_BUTTON, self.Log_clear)

        self.m_buttonErase.Bind(wx.EVT_BUTTON, self.Erase)
        self.m_buttonWrite.Bind(wx.EVT_BUTTON, self.Write)
        self.m_buttonRead.Bind(wx.EVT_BUTTON, self.Read)

        # Start condition
        self.m_buttonStop.Disable()

    def Start(self, e):
        # Create a Chip object
        port = self.m_textCtrlPort.GetValue()
        self.Chip = Chips(port)
        # Announce coordinator start to log
        self.Log_frame(port + ' 8N1@19200', 'Chip START', True)

        # Initialize buttons
        self.m_buttonInit.Disable()
        self.m_buttonStop.Enable()

    def Erase(self, e):
        # Initialize varables
        memory = 0
        mode = 0

        address = self.m_textCtrlAddress.GetValue()
        if(self.m_checkBoxFlash.GetValue() is True):
            memory = 0
        elif(self.m_checkBoxEeprom.GetValue() is True):
            memory = 1
        elif(self.m_checkBoxFram.GetValue() is True):
            memory = 2
        elif(self.m_checkBoxStpm.GetValue() is True):
            memory = 3

        if(self.m_checkBoxSector.GetValue() is True):
            mode = 0
        elif(self.m_checkBoxBlock.GetValue() is True):
            mode = 1
        elif(self.m_checkBoxAll.GetValue() is True):
            mode = 2

        self.Log_frame(self.Chip.Erase(memory, address, mode), 'Erase')
        self.Log_frame(self.Chip.Wait(), 'Erase Rsp', True)

    def Write(self, e):
        # Initialize varables
        memory = 0

        address = self.m_textCtrlAddress.GetValue()
        if(self.m_checkBoxFlash.GetValue() is True):
            memory = 0
        elif(self.m_checkBoxEeprom.GetValue() is True):
            memory = 1
        elif(self.m_checkBoxFram.GetValue() is True):
            memory = 2
        elif(self.m_checkBoxStpm.GetValue() is True):
            memory = 3

        data = self.m_textCtrlWrite.GetValue()

        self.Log_frame(self.Chip.Write(memory, address, data), 'Write')
        self.Log_frame(self.Chip.Wait(), 'Write Rsp', True)

    def Read(self, e):
        # Initialize varables
        memory = 0

        address = self.m_textCtrlAddress.GetValue()
        if(self.m_checkBoxFlash.GetValue() is True):
            memory = 0
        elif(self.m_checkBoxEeprom.GetValue() is True):
            memory = 1
        elif(self.m_checkBoxFram.GetValue() is True):
            memory = 2
        elif(self.m_checkBoxStpm.GetValue() is True):
            memory = 3

        size = self.m_textCtrlReadSize.GetValue()

        self.Log_frame(self.Chip.Read(memory, address, size), 'Read')
        self.Log_frame(self.Chip.Wait(), 'Read Rsp', True)

    def Stop(self, e):
        self.Chip.Close()
        self.m_buttonInit.Enable()
        self.m_buttonStop.Disable()

    def Log_frame(self, lst, name='', dspace=False):
        if lst != '':
            # Review if type is str or list
            if type(lst) is str:
                st = lst
            else:
                size = len(lst)
                st = ''
                for i in lst:
                    st += str(hex(i)[2:].zfill(2)) + ' '
                st = st.upper()
                st += '-- '
                st += str(size)

            # Select one or two spaces
            if dspace is True:
                st += '\n\n'
            else:
                st += '\n'
            # Form data string
            date = datetime.datetime.now().strftime("%d/%b/%y %I:%M:%S")
            self.m_textCtrlLog.AppendText(date)
            self.m_textCtrlLog.AppendText('  ')
            self.m_textCtrlLog.AppendText(name)
            space = ' ' * (15 - len(name))
            self.m_textCtrlLog.AppendText(space)
            self.m_textCtrlLog.AppendText(st)
        else:
            self.m_textCtrlLog.AppendText('\n')

    def Log_clear(self, e):
        self.m_textCtrlLog.Clear()

    def Exit(self, e):
        #try:
            #self.IkProtocol.Close()
        #except:
            #print('Port open: Never used')
        #self.timer.Stop()
        self.Close()


if __name__ == '__main__':
    app = wx.App()
    ChipsGUI(None, title='Scorpio ARM Chips')
    app.MainLoop()