"""
Prueba rapida para escribir el firmware del medidor ScorpioARM en la memoria flash externa
"""
'''
S0 19 0000 53 63 6F 72 70 69 6F 41 52 4D 31 2E 30 2E 65 6C 66 2E 73 72 65 63 58
'''
import serial
import time


def ConvertSrec():
    st = ''
    #x = 0
    while True:
        s19 = Srec.readline()

        if 'S3' in s19:
            s19 = s19[12:-3]
            for i in s19:
                st += i
                #x += 1 # For add a space between bytes
                #if x % 2 is 0:
                    #st += ' '
                if(len(st) == 128):
                    # Write to file
                    Firmware.write(st)
                    Firmware.write("\n")
                    st = ''
                    #x = 0

        elif 'S7' in s19:
            if st:
                Firmware.write(st)
                Firmware.write("\n")
            Firmware.close()
            print('Finish srec')
            break


def WriteFlash():
    # Open again Firmware file
    Firmware = open('Firmware.txt', 'r')

    # Send block erase command and wait 1s
    Ser.write([0, 2, 0, 0, 0, 0])
    time.sleep(1)

    Data = []
    Address = 0

    while True:
        fw = Firmware.readline()
        fw = fw[:-1]  # Erase line feeding
        array = bytearray.fromhex(fw)

        if len(array) == 64:
            for i in array:
                Data.append(i)

            if len(Data) == 256:
                Write(Data, Address)
                Address += len(Data)
                Data = []

        else:
            for i in array:
                Data.append(i)
            Write(Data, Address)
            break


def Write(Dta, addr):
    print('')
    print('DIRECCION: ', addr, 'Tamaño: ', len(Dta))

    # Form frame to output
    Frame = [0, 0]
    a = "{0:0{1}x}".format(addr, 8)
    a1 = (bytearray.fromhex(a))
    for i in a1:
        Frame.append(i)
    Frame.append(1)
    Frame.append(0)
    for i in Dta:
        Frame.append(i)
    Ser.write(Frame)

    # Print
    s = ''
    for i in Frame:
        s += str(hex(i)[2:].zfill(2)) + ' '
        s = s.upper()
    print(s)

    time.sleep(0.5)
    if Ser.inWaiting():
        print(Ser.read(Ser.inWaiting()))

    time.sleep(0.5)


def Exit():
    print('Exit')
    Ser.close()
    Srec.close()
    Firmware.close()


if __name__ == '__main__':
    Srec = open('ScorpioARM1.0_test.elf.srec', 'r')
    Firmware = open('Firmware.txt', 'w')

    Ser = serial.Serial(
        port='COM14',
        baudrate=19200,
        parity=serial.PARITY_EVEN,
        stopbits=serial.STOPBITS_ONE,
        bytesize=serial.EIGHTBITS,
        timeout=0
    )

    print("Port open: ", Ser.isOpen())

    # Convert srec to a file with row of 128 bytes length
    ConvertSrec()

    # Write the created file to FLASH
    WriteFlash()

    # Exit of application
    Exit()