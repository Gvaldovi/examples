# -*- coding: utf-8 -*-
"""
Clase para simular medidores IK.
"""
from Port import Serial


class IkMeters(Serial):

    def __init__(self, port, baud, parity):
        # Initialize serial port
        Serial.__init__(self, port, baud, parity)

        """
        Meters table
        """
        self.Meters = []

    def IkCmd_in(self, frame, num, name):
        # Review onlye valid commands

        if frame[17] == 0x03:    # Read
            response = []
            for i in frame[:18]:
                response.append(i)

            # Get address
            address = int.from_bytes(frame[18:20], byteorder='big')

            #if address == 2:       # Linker
                #response.append(4)
                #linker = [0xFF, 0xFF, 0xFF, 0xFF]
                #for i in linker:
                    #response.append(i)
            #elif address == 8:     # Extended ID
                #Id = bytes('Extended ID 1234', 'utf-8')
                #response.append(16)
                #for i in Id:
                    #response.append(i)
            if address == 0x21:  # Version
                response.append(2)
                response.append(4)
                response.append(0)
            #elif address == 0x20:  # Type
                #response.append(2)
                #response.append(0)
                #response.append(0x81)
            elif address == 0x24:  # Readings
                m = frame[:18]
                flag = 0

                response.append(54)
                #response.extend([0xFF] * 4)

                # Find meter, if it exist then mark flag
                meter = m.decode('utf-8')
                for x in self.Meters:
                    if meter in x:
                        flag = 1
                        m1 = bytearray.fromhex(x[12:])
                        for i in m1:
                            response.append(i)

                if flag == 0:
                    # Add meter to table
                    meter += '  0x0000000000000000'
                    self.Meters.append(meter)
                    # Sort meter table
                    self.Meters.sort()
                    #response.extend([0] * 4)

                Readings = [0x20,0x4C,0x30,0x9B,0x41,0xE7,0x52,0xBE,0x01,0x04,0x01,0x00,0x42,0x4B,0xCC,
                            0x42,0x42,0xD4,0x31,0x09,0x42,0x3F,0x80,0xE3,0x00,0x00,0x00,0x00,0x42,0xEC,
                            0xFC,0xCC,0x20,0x4C,0x84,0xFB,0x41,0xE7,0x52,0x3F,0x00,0x1F]

                for i in Readings:
                    response.append(i)
            else:
                return 'Error'

            return self.Print(response)

        #elif frame[8] == 0x04:  # Read ID by position
            #response = []
            #for i in frame[:9]:
                #response.append(i)

            #if num != 0:
                ## Add meter to table
                #field = name + '  0x00000000'
                #self.Meters.append(field)
                #response.append(8)
                #nameHex = bytes(name, 'utf-8')
                #for i in nameHex:
                    #response.append(i)
            #else:
                #return 'No meters'

            #return self.Print(response)

        elif frame[17] == 0x10:  # Write mult
            response = []
            flag = 0
            # Get address
            address = int.from_bytes(frame[18:20], byteorder='big')
            print(address)

            if address == 0x1A:    # Commands
                # Get meter
                m = frame[:8]
                meter = m.decode('utf-8')

                # Find meter, if it exist then mark flag
                for i, x in enumerate(self.Meters):
                    if meter in x:
                        flag = 1
                        x = x[:-1]
                        if frame[13] == 0x80:    # Disconnect
                            x += '1'
                        else:                    # Connect
                            x += '0'
                        # Copy new flags to table
                        self.Meters[i] = x

                if flag == 0:
                    return 'No meter'

            for i in frame[:-2]:
                response.append(i)
            return self.Print(response)

        else:
            return 'Error in command'

    def TableGet(self):
        return self.Meters

    def TableSet(self, newMeters):
        self.Meters = newMeters

