# -*- coding: utf-8 -*-
''' Script for merging an application with a bootloader for Scorpio meter '''

import os
import wx


class Image_creator(wx.Frame):

    def __init__(self, parent, title):
        super(Image_creator, self).__init__(parent, title=title, size=(700, 380))

        self.InitGUI()
        self.Centre()
        self.Show()

    def InitGUI(self):
        # GUI creation with wxFormBuilder
        bSizer = wx.BoxSizer( wx.VERTICAL )

        self.m_panel1 = wx.Panel( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
        bSizer11 = wx.BoxSizer( wx.VERTICAL )

        self.m_staticText2 = wx.StaticText( self.m_panel1, wx.ID_ANY, u"Herramienta para crear la imagen del medidor Scorpio (bootloader + aplicacion).\nEl bootloader debe ser ScorpioLC2b_BL_1.1.1ra.srec.\nLa aplicacion debe ser ScorpioARM1.0.elf.srec.", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText2.Wrap( -1 )
        bSizer11.Add( self.m_staticText2, 0, wx.ALL, 5 )

        bSizer4 = wx.BoxSizer( wx.HORIZONTAL )

        self.m_staticText3 = wx.StaticText( self.m_panel1, wx.ID_ANY, u"Estatus", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText3.Wrap( -1 )
        bSizer4.Add( self.m_staticText3, 0, wx.ALIGN_CENTER_HORIZONTAL|wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )

        self.textCtrlEstatus = wx.TextCtrl( self.m_panel1, wx.ID_ANY, u"Imagen no creada", wx.DefaultPosition, wx.Size( 250,-1 ), 0 )
        bSizer4.Add( self.textCtrlEstatus, 0, wx.ALL, 5 )


        bSizer11.Add( bSizer4, 1, wx.ALIGN_CENTER_HORIZONTAL, 5 )

        bSizer6 = wx.BoxSizer( wx.HORIZONTAL )

        self.buttonExit = wx.Button( self.m_panel1, wx.ID_ANY, u"Salir", wx.DefaultPosition, wx.DefaultSize, 0 )
        bSizer6.Add( self.buttonExit, 0, wx.ALL, 5 )

        self.buttonCreate = wx.Button( self.m_panel1, wx.ID_ANY, u"Crear", wx.DefaultPosition, wx.DefaultSize, 0 )
        bSizer6.Add( self.buttonCreate, 0, wx.ALL|wx.ALIGN_CENTER_HORIZONTAL, 5 )


        bSizer11.Add( bSizer6, 1, wx.ALIGN_CENTER_HORIZONTAL, 5 )


        self.m_panel1.SetSizer( bSizer11 )
        self.m_panel1.Layout()
        bSizer11.Fit( self.m_panel1 )
        bSizer.Add( self.m_panel1, 1, wx.EXPAND |wx.ALL, 5 )


        self.SetSizer( bSizer )
        self.Layout()
        bSizer.Fit( self )

        self.Centre( wx.BOTH )

        # Bindings
        self.buttonCreate.Bind(wx.EVT_BUTTON, self.Create)
        self.buttonExit.Bind(wx.EVT_BUTTON, self.Exit)

    def Create(self, e):
        #Copy bootloader to image file but last line
        try:
            boot = open('ScorpioLC2b_BL_1.1.1ra.srec', 'r')
        except:
            self.textCtrlEstatus.SetValue('Archivo bootloader no encontrado')
            return

        img = open('Release/Imagen.srec', 'w+')
        boot_lines = boot.readlines()
        img.writelines(item for item in boot_lines[:-1])

        # Close bootloader file
        boot.close()

        # Open app
        try:
            app = open('Release/ScorpioARM1.0.elf.srec', 'r')
        except:
            self.textCtrlEstatus.SetValue('Archivo applicacion no encontrado')
            return

        app_lines = app.readlines()
        app_lines = app_lines[1:]

        # Get version
        v = ''
        for i in app_lines:
            if 'S3110800FC00' in i:
                v += i[18] + i[19]
                v += i[16] + i[17]
                version = bytearray.fromhex(v)

                H = version[0] >> 2
                M = ((version[0] & 0x03) << 3) | (version[1] >> 5)
                L = version[1] & 0x1F
                break

        # Delete RAM section
        for i in app_lines:
            if 'S31520000000' in i:
                index = app_lines.index(i)
                del app_lines[index:-1]
                break

        # Copy to image and close files
        img.writelines(item for item in app_lines[:])
        img.close()
        app.close()

        # Rename files
        version_string = str(H) + '.' + str(M) + '.' + str(L)
        app_name = 'Release/ScorpioLC2b_App_' + version_string + '.srec'
        img_name = 'Release/ScorpioLC2b_Imagen_1.1.1_' + version_string + '.srec'

        try:
            os.remove(img_name)
            os.rename('Release/Imagen.srec', img_name)
            os.rename('Release/ScorpioARM1.0.srec', app_name)
            self.textCtrlEstatus.SetValue('Imagen sobrescrita')
        except:
            os.rename('Release/Imagen.srec', img_name)
            os.rename('Release/ScorpioARM1.0.srec', app_name)
            self.textCtrlEstatus.SetValue('Imagen creada')

    def Exit(self, e):
        self.Close()


if __name__ == '__main__':
    app = wx.App()
    Image_creator(None, title='Scorpio Image Creator')
    app.MainLoop()










