# Script for merging an application with a bootloader for Scorpio meter

import os
import sys

# Copy bootloader to image file but last line
print('Creando Imagen de Scorpio...')
print('Buscando bootloader ScorpioLC2b_BL_1.1.1ra.srec...')
try:
    boot = open('ScorpioLC2b_BL_1.1.1ra.srec', 'r')
except:
    print('Error: No se encuentra bootloader')
    sys.exit(1)

img = open('Imagen.srec', 'w+')
boot_lines = boot.readlines()
img.writelines(item for item in boot_lines[:-1])

# Close bootloader file
boot.close()

# Open app
print('Buscando aplicacion ScorpioARM1.0.elf.srec...')
try:
    app = open('ScorpioARM1.0.elf.srec', 'r')
except:
    print('Error: No se encuentra aplicacion')
    sys.exit(1)

app_lines = app.readlines()
app_lines = app_lines[1:]

# Get version
v = ''
for i in app_lines:
    if 'S3110800FC00' in i:
        v += i[18] + i[19]
        v += i[16] + i[17]
        version = bytearray.fromhex(v)

        H = version[0] >> 2
        M = ((version[0] & 0x03) << 3) | (version[1] >> 5)
        L = version[1] & 0x1F
        break

# Delete RAM section
for i in app_lines:
    if 'S31520000000' in i:
        index = app_lines.index(i)
        del app_lines[index:-1]
        break

# Copy to image and close files
img.writelines(item for item in app_lines[:])
img.close()
app.close()

# Rename files
version_string = str(H) + '.' + str(M) + '.' + str(L)
app_name = 'ScorpioLC2b_App_' + version_string + '.srec'
img_name = 'ScorpioLC2b_Imagen_1.1.1_' + version_string + '.srec'

try:
    os.remove(img_name)
    os.remove(app_name)
    os.rename('Imagen.srec', img_name)
    os.rename('ScorpioARM1.0.elf.srec', app_name)
    print('Imagen', version_string, 'sobrescrita')
except:
    os.rename('Imagen.srec', img_name)
    os.rename('ScorpioARM1.0.elf.srec', app_name)
    print('Imagen', version_string, 'creada')



