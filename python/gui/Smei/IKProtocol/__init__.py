#!C:\Python37\python.exe
# -*- coding: utf-8 -*-
"""
Interfaz grafica de usuario (GUI) para el simulador de Colector
"""
import wx
import datetime
import time
from IK import IkProtocol
from Port import Serial
from Mrf import Mrf


class Collector_simulator(wx.Frame):

    def __init__(self, parent, title):
        super(Collector_simulator, self).__init__(parent, title=title, size=(1300, 900))

        self.InitUI()
        #self.Centre()
        self.Show()

    def InitUI(self):
        # GUI creation with wxFormBuilder
        bSizer = wx.BoxSizer( wx.VERTICAL )
        
        self.m_notebook1 = wx.Notebook( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_scrolledWindow1 = wx.ScrolledWindow( self.m_notebook1, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.HSCROLL|wx.VSCROLL )
        self.m_scrolledWindow1.SetScrollRate( 5, 5 )
        bSizer11 = wx.BoxSizer( wx.VERTICAL )
        
        bSizer1 = wx.BoxSizer( wx.HORIZONTAL )
        
        bSizer20 = wx.BoxSizer( wx.HORIZONTAL )
        
        self.m_staticText11 = wx.StaticText( self.m_scrolledWindow1, wx.ID_ANY, u"Port", wx.DefaultPosition, wx.Size( -1,-1 ), 0 )
        self.m_staticText11.Wrap( -1 )
        bSizer20.Add( self.m_staticText11, 0, wx.ALIGN_CENTER_VERTICAL, 5 )
        
        self.m_textCtrlPort = wx.TextCtrl( self.m_scrolledWindow1, wx.ID_ANY, u"COM5", wx.DefaultPosition, wx.Size( -1,-1 ), 0 )
        self.m_textCtrlPort.SetMaxSize( wx.Size( 60,-1 ) )
        
        bSizer20.Add( self.m_textCtrlPort, 1, wx.ALIGN_CENTER_VERTICAL, 5 )
        
        self.m_staticText10 = wx.StaticText( self.m_scrolledWindow1, wx.ID_ANY, u"Baud", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText10.Wrap( -1 )
        bSizer20.Add( self.m_staticText10, 0, wx.ALIGN_CENTER_VERTICAL, 5 )
        
        self.m_textCtrlBaud = wx.TextCtrl( self.m_scrolledWindow1, wx.ID_ANY, u"19200", wx.DefaultPosition, wx.DefaultSize, 0 )
        bSizer20.Add( self.m_textCtrlBaud, 0, wx.ALL, 5 )
        
        self.m_buttonInit = wx.Button( self.m_scrolledWindow1, wx.ID_ANY, u"Start", wx.DefaultPosition, wx.DefaultSize, 0 )
        bSizer20.Add( self.m_buttonInit, 0, 0, 15 )
        
        self.m_buttonStop = wx.Button( self.m_scrolledWindow1, wx.ID_ANY, u"Stop", wx.DefaultPosition, wx.DefaultSize, 0 )
        bSizer20.Add( self.m_buttonStop, 0, 0, 15 )
        
        self.m_buttonExit = wx.Button( self.m_scrolledWindow1, wx.ID_ANY, u"Exit", wx.DefaultPosition, wx.DefaultSize, 0 )
        bSizer20.Add( self.m_buttonExit, 0, 0, 50 )
        
        
        bSizer1.Add( bSizer20, 1, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )
        
        bSizer22 = wx.BoxSizer( wx.HORIZONTAL )
        
        self.m_staticText6 = wx.StaticText( self.m_scrolledWindow1, wx.ID_ANY, u"Origin", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText6.Wrap( -1 )
        bSizer22.Add( self.m_staticText6, 0, wx.ALL, 5 )
        
        self.m_textCtrlColMac = wx.TextCtrl( self.m_scrolledWindow1, wx.ID_ANY, u"11 22 33 44 55 66 77 88", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_textCtrlColMac.SetMinSize( wx.Size( 200,-1 ) )
        
        bSizer22.Add( self.m_textCtrlColMac, 0, 0, 5 )
        
        
        bSizer1.Add( bSizer22, 1, wx.ALIGN_CENTER_VERTICAL, 5 )
        
        
        bSizer11.Add( bSizer1, 1, wx.EXPAND, 5 )
        
        bSizer2 = wx.BoxSizer( wx.HORIZONTAL )
        
        bSizer16 = wx.BoxSizer( wx.VERTICAL )
        
        self.m_checkBoxRadio = wx.CheckBox( self.m_scrolledWindow1, wx.ID_ANY, u"MAC Radio", wx.DefaultPosition, wx.DefaultSize, wx.CHK_2STATE )
        bSizer16.Add( self.m_checkBoxRadio, 0, wx.ALIGN_CENTER_HORIZONTAL|wx.ALL, 5 )
        
        self.m_textCtrlRadioMac = wx.TextCtrl( self.m_scrolledWindow1, wx.ID_ANY, u"BB BB BB BB BB BB BB BB", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_textCtrlRadioMac.SetMinSize( wx.Size( 200,-1 ) )
        
        bSizer16.Add( self.m_textCtrlRadioMac, 0, wx.ALIGN_CENTER_HORIZONTAL|wx.ALL, 5 )
        
        self.m_buttonRadioSend = wx.Button( self.m_scrolledWindow1, wx.ID_ANY, u"Send", wx.DefaultPosition, wx.DefaultSize, 0 )
        bSizer16.Add( self.m_buttonRadioSend, 0, wx.ALIGN_CENTER_HORIZONTAL|wx.ALL, 5 )
        
        bSizer14 = wx.BoxSizer( wx.VERTICAL )
        
        m_listBoxRadioOptionsChoices = [ u"(0xF0) Reset", u"(0xF1) New Node", u"(0xF2) New Network", u"(0xF3) New Time", u"(0xF4) Master reset" ]
        self.m_listBoxRadioOptions = wx.ListBox( self.m_scrolledWindow1, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, m_listBoxRadioOptionsChoices, 0 )
        bSizer14.Add( self.m_listBoxRadioOptions, 0, wx.ALIGN_CENTER_HORIZONTAL|wx.ALL, 5 )
        
        
        bSizer16.Add( bSizer14, 1, wx.EXPAND, 5 )
        
        self.m_staticText7113 = wx.StaticText( self.m_scrolledWindow1, wx.ID_ANY, u"Radio Parameter", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText7113.Wrap( -1 )
        bSizer16.Add( self.m_staticText7113, 0, wx.ALIGN_CENTER_HORIZONTAL|wx.ALL, 5 )
        
        self.m_textCtrlRadioPar = wx.TextCtrl( self.m_scrolledWindow1, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_textCtrlRadioPar.SetMinSize( wx.Size( 80,-1 ) )
        
        bSizer16.Add( self.m_textCtrlRadioPar, 0, wx.ALIGN_CENTER_HORIZONTAL|wx.ALL|wx.EXPAND, 5 )
        
        
        bSizer2.Add( bSizer16, 1, wx.EXPAND, 5 )
        
        bSizer24 = wx.BoxSizer( wx.VERTICAL )
        
        self.m_staticText7 = wx.StaticText( self.m_scrolledWindow1, wx.ID_ANY, u"ID Master", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText7.Wrap( -1 )
        bSizer24.Add( self.m_staticText7, 0, wx.ALL|wx.ALIGN_CENTER_HORIZONTAL, 5 )
        
        self.m_textCtrlCabMac = wx.TextCtrl( self.m_scrolledWindow1, wx.ID_ANY, u"30 30 30 30 30 30 30 30", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_textCtrlCabMac.SetMinSize( wx.Size( 200,-1 ) )
        
        bSizer24.Add( self.m_textCtrlCabMac, 0, wx.ALL|wx.ALIGN_CENTER_HORIZONTAL, 5 )
        
        self.m_buttonMstSend = wx.Button( self.m_scrolledWindow1, wx.ID_ANY, u"Send", wx.DefaultPosition, wx.DefaultSize, 0 )
        bSizer24.Add( self.m_buttonMstSend, 0, wx.ALIGN_CENTER_HORIZONTAL|wx.TOP, 5 )
        
        bSizer12 = wx.BoxSizer( wx.HORIZONTAL )
        
        m_listBoxMasterOptionsChoices = [ u"(0x10) Master readings", u"(0x11) Association", u"(0x12) Dissociation", u"(0x13) Discovery (0..1)", u"(0x14) Open door alarm (0..3)", u"(0x15) Freedom (u16 min)", u"(0x16) Synchronization", u"(0x17) Permit join on (u8 seg)", u"(0x18) Permit join off", u"(0x19) Master ID read", u"(0x1A) PAN ID write (0..0xFFF0", u"(0x1B) Channel write (11..26)", u"(0x1C) PAN ID read", u"(0x1D) Channel read", u"(0x1E) Add meter", u"(0x1F) Delete meter", u"(0x20) Multiple meter connect", u"(0x21) Multiple meter disconnect", u"(0x22) Data base erase", u"(0x23) Find new network", u"(0x24) Master internal state", u"(0x25) Meters table", u"(0x26) Master bootloader", u"(0x27) Meter bootloader", u"(0x28) Display bootloader", u"(0x29) Radio bootloader", u"(0x2E) External memories", u"(0x2F) Master reset", u"(0x40) Master ID write" ]
        self.m_listBoxMasterOptions = wx.ListBox( self.m_scrolledWindow1, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, m_listBoxMasterOptionsChoices, 0 )
        bSizer12.Add( self.m_listBoxMasterOptions, 0, wx.ALIGN_CENTER|wx.EXPAND, 5 )
        
        
        bSizer24.Add( bSizer12, 1, wx.ALIGN_CENTER_HORIZONTAL|wx.TOP, 5 )
        
        self.m_staticText711 = wx.StaticText( self.m_scrolledWindow1, wx.ID_ANY, u"Master Parameter", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText711.Wrap( -1 )
        bSizer24.Add( self.m_staticText711, 0, wx.ALL|wx.ALIGN_CENTER_HORIZONTAL, 5 )
        
        self.m_textCtrlCabPar = wx.TextCtrl( self.m_scrolledWindow1, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_textCtrlCabPar.SetMinSize( wx.Size( 80,-1 ) )
        
        bSizer24.Add( self.m_textCtrlCabPar, 0, wx.ALL|wx.EXPAND, 5 )
        
        
        bSizer2.Add( bSizer24, 1, wx.EXPAND, 5 )
        
        bSizer25 = wx.BoxSizer( wx.VERTICAL )
        
        self.m_staticText8 = wx.StaticText( self.m_scrolledWindow1, wx.ID_ANY, u"ID Meter", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText8.Wrap( -1 )
        bSizer25.Add( self.m_staticText8, 0, wx.ALL|wx.ALIGN_CENTER_HORIZONTAL, 5 )
        
        self.m_textCtrlMtrMac = wx.TextCtrl( self.m_scrolledWindow1, wx.ID_ANY, u"30 30 30 4D 45 54 45 52", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_textCtrlMtrMac.SetMinSize( wx.Size( 200,-1 ) )
        
        bSizer25.Add( self.m_textCtrlMtrMac, 0, wx.ALL|wx.ALIGN_CENTER_HORIZONTAL, 5 )
        
        self.m_buttonMtrSend = wx.Button( self.m_scrolledWindow1, wx.ID_ANY, u"Send", wx.DefaultPosition, wx.DefaultSize, 0 )
        bSizer25.Add( self.m_buttonMtrSend, 0, wx.ALL|wx.ALIGN_CENTER_HORIZONTAL, 5 )
        
        bSizer23 = wx.BoxSizer( wx.VERTICAL )
        
        m_listBoxMeterOptionsChoices = [ u"(0x01) Meter readings", u"(0x02) Events (All)", u"(0x03) Events (New)", u"(0x04) Meter reset (0..2)", u"(0x05) Load profile (FF FF FF:m d h)", u"(0x06) TOU (1..4)", u"(0x07) Configuration template read", u"(0x08) Configuration template write", u"(0x09) Meter bridge", u"(0x0C) Over demand" ]
        self.m_listBoxMeterOptions = wx.ListBox( self.m_scrolledWindow1, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, m_listBoxMeterOptionsChoices, 0 )
        bSizer23.Add( self.m_listBoxMeterOptions, 0, wx.ALL, 5 )
        
        
        bSizer25.Add( bSizer23, 1, wx.EXPAND, 5 )
        
        self.m_staticText7111 = wx.StaticText( self.m_scrolledWindow1, wx.ID_ANY, u"Meter Parameter", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText7111.Wrap( -1 )
        bSizer25.Add( self.m_staticText7111, 0, wx.ALL|wx.ALIGN_CENTER_HORIZONTAL, 5 )
        
        self.m_textCtrlMtrPar = wx.TextCtrl( self.m_scrolledWindow1, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_textCtrlMtrPar.SetMinSize( wx.Size( 80,-1 ) )
        
        bSizer25.Add( self.m_textCtrlMtrPar, 0, wx.ALL|wx.EXPAND, 5 )
        
        
        bSizer2.Add( bSizer25, 1, wx.EXPAND|wx.ALIGN_BOTTOM, 5 )
        
        bSizer26 = wx.BoxSizer( wx.VERTICAL )
        
        self.m_staticText9 = wx.StaticText( self.m_scrolledWindow1, wx.ID_ANY, u"ID Display", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText9.Wrap( -1 )
        bSizer26.Add( self.m_staticText9, 0, wx.ALL|wx.ALIGN_CENTER_HORIZONTAL, 5 )
        
        self.m_textCtrlDpyMac = wx.TextCtrl( self.m_scrolledWindow1, wx.ID_ANY, u"30 30 30 30 31 44 50 59", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_textCtrlDpyMac.SetMinSize( wx.Size( 200,-1 ) )
        
        bSizer26.Add( self.m_textCtrlDpyMac, 0, wx.ALL|wx.ALIGN_CENTER_HORIZONTAL, 5 )
        
        self.m_buttonDpySend = wx.Button( self.m_scrolledWindow1, wx.ID_ANY, u"Send", wx.DefaultPosition, wx.DefaultSize, 0 )
        bSizer26.Add( self.m_buttonDpySend, 0, wx.ALL|wx.ALIGN_CENTER_HORIZONTAL, 5 )
        
        bSizer293 = wx.BoxSizer( wx.VERTICAL )
        
        m_listBoxDisplayOptionsChoices = [ u"(0x31) Custom messages (Alphanumeric)", u"(0x32) Readings configuration (u16)", u"(0x33) Decimal point (0..3)", u"(0x34) Display bridge", u"(0x35) Modem bridge" ]
        self.m_listBoxDisplayOptions = wx.ListBox( self.m_scrolledWindow1, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, m_listBoxDisplayOptionsChoices, 0 )
        bSizer293.Add( self.m_listBoxDisplayOptions, 0, wx.ALL, 5 )
        
        
        bSizer26.Add( bSizer293, 1, wx.ALIGN_CENTER_HORIZONTAL, 5 )
        
        self.m_staticText7112 = wx.StaticText( self.m_scrolledWindow1, wx.ID_ANY, u"Display Parameter", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText7112.Wrap( -1 )
        bSizer26.Add( self.m_staticText7112, 0, wx.ALL|wx.ALIGN_CENTER_HORIZONTAL, 5 )
        
        self.m_textCtrlDpyPar = wx.TextCtrl( self.m_scrolledWindow1, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_textCtrlDpyPar.SetMinSize( wx.Size( 80,-1 ) )
        
        bSizer26.Add( self.m_textCtrlDpyPar, 0, wx.ALL|wx.EXPAND, 5 )
        
        
        bSizer2.Add( bSizer26, 1, wx.EXPAND, 5 )
        
        
        bSizer11.Add( bSizer2, 5, wx.EXPAND, 5 )
        
        
        self.m_scrolledWindow1.SetSizer( bSizer11 )
        self.m_scrolledWindow1.Layout()
        bSizer11.Fit( self.m_scrolledWindow1 )
        self.m_notebook1.AddPage( self.m_scrolledWindow1, u"Commands", True )
        self.m_scrolledWindow5 = wx.ScrolledWindow( self.m_notebook1, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.HSCROLL|wx.VSCROLL )
        self.m_scrolledWindow5.SetScrollRate( 5, 5 )
        self.m_notebook1.AddPage( self.m_scrolledWindow5, u"Testing", False )
        
        bSizer.Add( self.m_notebook1, 1, wx.ALIGN_TOP|wx.EXPAND, 5 )
        
        self.m_buttonLogClear = wx.Button( self, wx.ID_ANY, u"Clean Log", wx.DefaultPosition, wx.DefaultSize, 0 )
        bSizer.Add( self.m_buttonLogClear, 0, wx.ALL|wx.ALIGN_CENTER_HORIZONTAL, 5 )
        
        self.m_textCtrlLog = wx.TextCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.TE_MULTILINE )
        self.m_textCtrlLog.SetFont( wx.Font( 9, 75, 90, 90, False, "Courier New" ) )
        self.m_textCtrlLog.SetMinSize( wx.Size( -1,300 ) )
        
        bSizer.Add( self.m_textCtrlLog, 1, wx.ALL|wx.EXPAND, 5 )
        
        
        self.SetSizer( bSizer )
        self.Layout()
        
        self.Centre( wx.BOTH )

        #Bindings
        self.m_buttonInit.Bind(wx.EVT_BUTTON, self.Start)
        self.m_buttonStop.Bind(wx.EVT_BUTTON, self.Stop)
        self.m_buttonExit.Bind(wx.EVT_BUTTON, self.Quit)
        self.m_buttonLogClear.Bind(wx.EVT_BUTTON, self.Log_clear)
        self.m_buttonMstSend.Bind(wx.EVT_BUTTON, self.Master_send)
        self.m_buttonMtrSend.Bind(wx.EVT_BUTTON, self.Meter_send)
        self.m_buttonDpySend.Bind(wx.EVT_BUTTON, self.Display_send)
        self.m_buttonRadioSend.Bind(wx.EVT_BUTTON, self.Radio_send)

        # Start condition
        self.m_buttonStop.Disable()

        # Timer enable
        self.timer = wx.Timer(self)
        self.Bind(wx.EVT_TIMER, self.Rx_data, self.timer)

        # Create a collector object
        self.IkProtocol = IkProtocol()
        self.Mrf = Mrf()
        # Generate MAC Colector
        self.Orig = self.m_textCtrlColMac.GetValue()

    def Start(self, e):
        # Create a serial port object
        port = self.m_textCtrlPort.GetValue()
        baud = self.m_textCtrlBaud.GetValue()
        self.Serial = Serial(port, baud, 'none')

        # Verify port status
        if self.Serial.PortStatus():
            # Announce coordinator start to log
            self.Log_frame('Status:','IkProtocol started: ' + port + ' 8N1@' + baud, True)

            # Initialize buttons and timer
            self.m_buttonInit.Disable()
            self.m_buttonStop.Enable()
            self.timer.Start(100)
            self.RespFlag = False
        else:
            self.Log_frame('Error:', port + ': No serial port available but you can see the frames.', True)

    def Stop(self, e):
        self.Serial.Close()
        self.timer.Stop()
        self.m_buttonInit.Enable()
        self.m_buttonStop.Disable()
        self.Log_frame('Status:','Port closed', True)

    def Rx_data(self, e):
        self.timer.Stop()

        if self.Serial.Poll() is True:
            rx = self.Serial.Get_buffer()
            # Review CRC
            if self.Serial.Get_status() is True:
                # If a response had been waited then print out it
                if self.RespFlag is True:
                    self.Log_frame('Received', rx, True)
                    self.RespFlag = False
                else:
                    # Print out the frame received
                    self.Log_frame('Received', rx, False)
                    # Review if Radio is active
                    if self.m_checkBoxRadio.GetValue() is True:
                        # Send command to Mrf
                        self.Mrf_in(rx)
                    else:
                        # Send command to IK protocol
                        self.Ik_in(rx)
            # Error in CRC
            else:
                self.Log_frame('Error: CRC', rx, True)
        # Restart timer
        self.timer.Start(100)

    def Mrf_in(self, frame):
        # Execute the Mrf command
        status, Resp = self.Mrf.In(frame)
        if status == 'Ik':
            remote_response = [0x04,0x00]
            self.CmdOut(remote_response, True)

            # Time before sending the response
            time.sleep(0.5)

            self.Ik_in(Resp)
        elif status == 'Mrf':
            self.CmdOut(Resp, True)

    def Ik_in(self, Resp):
        # Execute the IK command
        status, Resp = self.IkProtocol.IkCmd_in(Resp)
        if status == True:
            self.CmdOut(Resp, False, True)
        elif 'Error' in Resp:
            self.Log_frame('Error:', Resp)
    
    def Radio_send(self, e):
        if self.m_checkBoxRadio.GetValue() is True:
            # Get parameter to send to cabinet
            parameter = self.m_textCtrlRadioPar.GetValue()
            # Get command
            option = self.m_listBoxRadioOptions.GetSelections()
            cmd = self.m_listBoxRadioOptions.GetString(option[0])
            # Execute command
            status, Resp = self.Mrf.Network(cmd, parameter)
            if status == True:
                self.CmdOut(Resp)
            else:
                self.Log_frame('Error:', Resp)
        else:
            self.CmdOut('Error:', 'Radio functions not activated yet')

    def Master_send(self, e):
        # Get mac of cabinet
        Dest = self.m_textCtrlCabMac.GetValue()
        # Get parameter to send to cabinet
        parameter = self.m_textCtrlCabPar.GetValue()
        # Get mac of meter
        MeterMac = self.m_textCtrlMtrMac.GetValue()
        # Get mac of display
        DisplayMac = self.m_textCtrlDpyMac.GetValue()
        self.RespFlag = True

        # Get command
        option = self.m_listBoxMasterOptions.GetSelections()
        name = self.m_listBoxMasterOptions.GetString(option[0])
        # Execute command
        status, Resp = self.IkProtocol.MasterCmd(Dest, self.Orig, name, parameter, MeterMac, DisplayMac)
        if status == True:
            self.CmdOut(Resp, False, True)
        else:
            self.Log_frame('Error:', Resp)

    def Meter_send(self, e):
        # Get mac of meter
        Dest = self.m_textCtrlMtrMac.GetValue()
        # Get parameter to send to cabinet
        parameter = self.m_textCtrlMtrPar.GetValue()
        self.RespFlag = True

        # Get command
        option = self.m_listBoxMeterOptions.GetSelections()
        name = self.m_listBoxMeterOptions.GetString(option[0])
        # Execute command
        status, Resp = self.IkProtocol.DisplayCmd(Dest, self.Orig, name, parameter)
        if status == True:
            self.CmdOut(Resp, False, True)
        else:
            self.Log_frame('Error:', Resp)

    def Display_send(self, e):
        # Get mac of Display
        Dest = self.m_textCtrlDpyMac.GetValue()
        # Get parameter to send to Display
        parameter = self.m_textCtrlDpyPar.GetValue()
        self.RespFlag = True

        # Get command
        option = self.m_listBoxDisplayOptions.GetSelections()
        name = self.m_listBoxDisplayOptions.GetString(option[0])
        # Execute command
        status, Resp = self.IkProtocol.DisplayCmd(Dest, self.Orig, name, parameter)
        if status == True:
            self.CmdOut(Resp, False, True)
        else:
            self.Log_frame('Error:', Resp)

    def CmdOut(self, frame, dspace=False, add_mac=False):
        if self.m_checkBoxRadio.GetValue() is True and add_mac is True:
            self.Serial.CrcAppend(frame)
            mac = self.m_textCtrlRadioMac.GetValue()
            frame = self.Mrf.Notification(mac, frame)

        # Send the response through serial port and log
        self.Serial.CrcAppend(frame)
        self.Log_frame('Sent', frame, dspace)
        # Send the response through serial port
        try:
            self.Serial.Print(frame)
        except:
            pass

    def Log_frame(self, name='', lst='', dspace=False):
        if lst != '':
            # Review if type is str or list
            if type(lst) is str:
                st = lst
            else:
                size = len(lst)
                st = ''
                for i in lst:
                    st += str(hex(i)[2:].zfill(2)) + ' '
                st = st.upper()
                st += '-- '
                st += str(size)

            # Select one or two spaces
            if dspace is True:
                st += '\n\n'
            else:
                st += '\n'
            # Form data string
            date = datetime.datetime.now().strftime("%d/%b/%y %I:%M:%S")
            self.m_textCtrlLog.AppendText(date)
            self.m_textCtrlLog.AppendText('  ')
            self.m_textCtrlLog.AppendText(name)
            space = ' ' * (15 - len(name))
            self.m_textCtrlLog.AppendText(space)
            self.m_textCtrlLog.AppendText(st)
        else:
            self.m_textCtrlLog.AppendText('\n')

    def Log_clear(self, e):
        self.m_textCtrlLog.Clear()

    def Quit(self, e):
        try:
            self.Serial.Close()
        except:
            print('Port open: Never used')
        self.timer.Stop()
        self.Close()


if __name__ == '__main__':
    app = wx.App()
    Collector_simulator(None, title='IK Protocol')
    app.MainLoop()





