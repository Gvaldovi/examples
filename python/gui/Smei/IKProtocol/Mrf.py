"""
Class for simulating a MRF radio (Zigbee)
"""
import time
import struct


class Mrf(object):

    def __init__(self):

        # Initialize serial port
        self.Data = [0x12, 0x34, 0x56, 0x78, 0x9A, 0xBC, 0xDE, 0xF0,
                     0x30, 0x31,
                     0x00, 0x45,
                     0x18,
                     0xCA, 0xFE,
                     0x5D, 0x56, 0x78, 0x89,
                     0x00, 0x00, 0x05, 0x43,
                     0x00]
    #     Serial.__init__(self, port, baud, parity)

    def In(self, frame):
        response = []

        if frame[0] == 0x03:
            status = 'Mrf'
            index = frame[1]
            count = frame[2]

            response.append(0x03)
            response.append(count)
            response.extend(self.Data[index:index+count])
        elif frame[0] == 0x10:
            status = 'Mrf'
            response.append(0x10)
            response.append(0)
        if frame[0] == 0x04:
            for i in frame[11:-2]:
                response.append(i)
            status = 'Ik'
        elif frame[0] == 0x05:
            status = 'Mrf_no'
        return status, response

    def Notification(self, mac, frame):
        response = [0x05]
        mac = bytearray.fromhex(mac)
        response.extend(mac)
        response.append(0)
        response.append(len(frame))
        response.extend(frame)
        return response


    def Network(self, cmd, parameter):
        response = [0x06]

        if cmd == '(0xF0) Reset':
            response.append(0xF0)
        elif cmd == '(0xF1) New Node':
            response.append(0xF1)
            response.append(8)
            if parameter != '':
                mac = bytearray.fromhex(parameter)
                response.extend(mac)
        elif cmd == '(0xF2) New Network':
            response.append(0xF2)
            if parameter != '':
                mac = bytearray.fromhex(parameter)
                response.extend(mac)
        elif cmd == '(0xF3) New Time':
            response.append(0xF3)
            response.append(8)
            # Get epoch
            epoch = int(time.time())
            epoch -= 946684800
            #epoch2000 = struct.unpack("4B", struct.pack("I", epoch))
            #for i in epoch2000[::-1]:
                #data.append(i)
            epochbytes = epoch.to_bytes(4, byteorder='big')
            for i in epochbytes:
                response.append(i)
            # Insert timezone
            response.append(0xFF)
            response.append(0xFF)
            response.append(0xB9)
            response.append(0xB0)

        elif cmd == '(0xF4) Master reset':
            response.append(0xF4)

        return True, response




