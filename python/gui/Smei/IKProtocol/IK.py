"""
Class for simulating IK protocol
"""
import time
import struct


class IkProtocol(object):

    # def __init__(self):

    #     Initialize serial port
    #     Serial.__init__(self, port, baud, parity)

    def IkCmd_in(self, frame):
        # Announces, deletes, alarms and events. All responses are OK
        if frame[16] in [0x0A, 0x0B, 0x3A, 0x3B, 0x2A, 0x2B, 0x2C, 0x2D, 0x60, 0x61, 0x70]:
            response = frame[8:16]
            response.extend(frame[:8])
            # response = []
            # for i in frame[8:16]:
            #     response.append(i)
            # for i in frame[:8]:
            #     response.append(i)
            response.append(frame[16])
            response.append(0)
            response.append(0)
            return True, response
        else:
            return  False, frame

    def MeterCmd(self, dest, orig, cmd, param):
        response = []
        data = []

        # Convert string to bytes
        dest1 = bytearray.fromhex(dest)
        orig1 = bytearray.fromhex(orig)

        for i in dest1:
            response.append(i)
        for i in orig1:
            response.append(i)

        # Meter commands
        if cmd == '(0x01) Meter readings':
            response.append(0x01)

        elif cmd == '(0x02) Events (All)':
            response.append(0x02)

        elif cmd == '(0x03) Events (New)':
            response.append(0x03)

        elif cmd == '(0x04) Meter reset (0..2)':
            response.append(0x04)
            data.append(int(param))

        elif cmd == '(0x05) Load profile (FF FF FF:m d h)':
            response.append(0x05)
            Date = bytearray.fromhex(param)
            for i in Date:
                data.append(i)

        elif cmd == '(0x06) TOU (1..4)':
            response.append(0x06)
            data.append(int(param))

        elif cmd == '(0x07) Configuration template read':
            response.append(0x07)

        elif cmd == '(0x08) Configuration template write':
            response.append(0x08)
            for i in range(332):
                data.append(i)
        #(0x09) Meter bridge' '(0x0C) Over demand'
        return True, (self.CompleteFrame(data, response))
                
    def DisplayCmd(self, dest, orig, cmd, param):
        response = []
        data = []

        # Convert string to bytes
        dest1 = bytearray.fromhex(dest)
        orig1 = bytearray.fromhex(orig)

        for i in dest1:
            response.append(i)
        for i in orig1:
            response.append(i)

        # Display commands
        if cmd == '(0x31) Custom messages (Alphanumeric)':
            response.append(0x31)
            for i in param:
                data.append(ord(i))

        elif cmd == '(0x32) Readings configuration (u16)':
            response.append(0x32)
            Readings = bytearray.fromhex(param)
            for i in Readings:
                data.append(i)

        elif cmd == '(0x33) Decimal point (0..3)':
            response.append(0x33)
            data.append(int(param))
        #"(0x34) Display bridge" "(0x35) Modem bridge"
        return True, (self.CompleteFrame(data, response))

    def MasterCmd(self, dest, orig, cmd, param, mtr, dpy):
        response = []
        data = []

        # Convert string to bytes
        dest1 = bytearray.fromhex(dest)
        orig1 = bytearray.fromhex(orig)

        for i in dest1:
            response.append(i)
        for i in orig1:
            response.append(i)

        # Master commands
        if cmd == '(0x10) Master readings':
            response.append(0x10)

        elif cmd == '(0x11) Association':
            response.append(0x11)
            both = mtr + dpy
            MtrDpy = bytearray.fromhex(both)
            for i in MtrDpy:
                data.append(i)
            #data.extend([0xFF] * 2)  # Revisar si las FF van al ultimo o al principio del display

        elif cmd == '(0x12) Dissociation':
            response.append(0x12)
            both = mtr + dpy
            MtrDpy = bytearray.fromhex(both)
            for i in MtrDpy:
                data.append(i)
            #data.extend([0xFF] * 2)  # Revisar si las FF van al ultimo o al principio del display

        elif cmd == '(0x13) Discovery (0..1)':
            response.append(0x13)
            data.append(int(param))

        elif cmd == '(0x14) Open door alarm (0..3)':
            response.append(0x14)
            data.append(int(param))

        elif cmd == '(0x15) Freedom (u16 min)':
            response.append(0x15)
            array = struct.unpack("2B", struct.pack("H", int(param)))
            data.append(array[1])
            data.append(array[0])

        elif cmd == '(0x16) Synchronization':
            response.append(0x16)
            # Get epoch
            epoch = int(time.time())
            epoch -= 946684800
            #epoch2000 = struct.unpack("4B", struct.pack("I", epoch))
            #for i in epoch2000[::-1]:
                #data.append(i)
            epochbytes = epoch.to_bytes(4, byteorder='big')
            for i in epochbytes:
                data.append(i)
            # Insert timezone
            data.append(0xFF)
            data.append(0xFF)
            data.append(0xB9)
            data.append(0xB0)

        elif cmd == '(0x1C) PAN ID read':
            response.append(0x1C)

        elif cmd == '(0x1D) Channel read':
            response.append(0x1D)

        elif cmd == '(0x19) Master ID read':
            response.append(0x19)

        elif cmd == '(0x24) Master internal state':
            response.append(0x24)

        elif cmd == '(0x17) Permit join on (u8 seg)':
            response.append(0x17)
            data.append(int(param))

        elif cmd == '(0x18) Permit join off':
            response.append(0x18)

        elif cmd == '(0x1A) PAN ID write (0..0xFFF0':
            response.append(0x1A)
            array = struct.unpack("2B", struct.pack("H", int(param, 0)))
            data.append(array[1])
            data.append(array[0])

        elif cmd == '(0x1B) Channel write (11..26)':
            response.append(0x1B)
            data.append(int(param))

        elif cmd == '(0x1E) Add meter':
            response.append(0x1E)
            Meter = bytearray.fromhex(mtr)
            for i in Meter:
                data.append(i)

        elif cmd == '(0x1F) Delete meter':
            response.append(0x1F)
            Meter = bytearray.fromhex(mtr)
            for i in Meter:
                data.append(i)

        elif cmd == '(0x20) Multiple meter connect' or cmd == '(0x21) Multiple meter disconnect':
            if cmd == '(0x20) Multiple meter connect':
                response.append(0x20)
            else:
                response.append(0x21)

            # Get number of meters and mac of initial meter
            Num = int(param)

            if Num > 0 and Num < 4:
                data.append(Num)
                s = mtr

                Meter1 = bytearray.fromhex(mtr)
                for i in Meter1:
                    data.append(i)

                if Num == 2:
                    a = int(bytearray.fromhex(s[:9]))
                    b = "{:03d}".format(a + 1)
                    print(b)
                    c = ''.join("{:02x}".format(ord(x)) for x in b)
                    Meter2 = bytearray.fromhex(c)
                    Meter2 += Meter1[3:]

                    for i in Meter2:
                        data.append(i)

                elif Num == 3:
                    a = int(bytearray.fromhex(s[:9]))
                    b = "{:03d}".format(a + 1)
                    c = ''.join("{:02x}".format(ord(x)) for x in b)
                    Meter2 = bytearray.fromhex(c)
                    Meter2 += Meter1[3:]

                    b1 = "{:03d}".format(a + 2)
                    c1 = ''.join("{:02x}".format(ord(x)) for x in b1)
                    Meter3 = bytearray.fromhex(c1)
                    Meter3 += Meter1[3:]

                    for i in Meter2:
                        data.append(i)
                    for i in Meter3:
                        data.append(i)
            else:
                return 'Error: Wrong multiple command'

        elif cmd == '(0x22) Data base erase':
            response.append(0x22)

        elif cmd == '(0x23) Find new network':
            response.append(0x23)

        elif cmd == '(0x25) Meters table':
            response.append(0x25)
        #(0x26) Master bootloader" 
        #"(0x27) Meter bootloader" 
        #"(0x28) Display bootloader" 
        #"(0x29) Radio bootloader" 
        #"(0x2E) External memories" 
        #"(0x2F) Master reset" 
        #"(0x40) Master ID write"
        else:
            return False, 'Error: Wrong command'
        return True, (self.CompleteFrame(data, response))

    def CompleteFrame(self, data, response):
        # Complete frame
        if len(data) == 0:
            response.append(0x00)
        elif len(data) <= 127:
            response.append(len(data))
        elif len(data) <= 256:
            response.append(0x80)
            response.append(len(data))
        for i in data:
            response.append(i)
        return response