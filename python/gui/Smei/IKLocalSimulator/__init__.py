# -*- coding: utf-8 -*-
"""
Interfaz grafica de usuario (GUI) para el simulador de Colector
"""
import wx
import datetime
from Collector import IkCollector


class Collector_simulator(wx.Frame):

    def __init__(self, parent, title):
        super(Collector_simulator, self).__init__(parent, title=title, size=(1300, 900))

        self.InitUI()
        #self.Centre()
        self.Show()

    def InitUI(self):
        # GUI creation with wxFormBuilder
        bSizer = wx.BoxSizer( wx.VERTICAL )

        self.m_panel1 = wx.Panel( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
        bSizer11 = wx.BoxSizer( wx.VERTICAL )

        bSizer1 = wx.BoxSizer( wx.HORIZONTAL )

        bSizer1.SetMinSize( wx.Size( -1,50 ) )
        self.m_staticText11 = wx.StaticText( self.m_panel1, wx.ID_ANY, u"Puerto", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText11.Wrap( -1 )
        bSizer1.Add( self.m_staticText11, 0, wx.ALIGN_CENTER|wx.ALL, 10 )

        self.m_textCtrlPort = wx.TextCtrl( self.m_panel1, wx.ID_ANY, u"COM101", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_textCtrlPort.SetMaxSize( wx.Size( 60,-1 ) )

        bSizer1.Add( self.m_textCtrlPort, 0, wx.ALIGN_CENTER|wx.RIGHT, 50 )

        self.m_buttonInit = wx.Button( self.m_panel1, wx.ID_ANY, u"Iniciar", wx.DefaultPosition, wx.DefaultSize, 0 )
        bSizer1.Add( self.m_buttonInit, 0, wx.ALIGN_CENTER|wx.RIGHT, 15 )

        self.m_buttonStop = wx.Button( self.m_panel1, wx.ID_ANY, u"Parar", wx.DefaultPosition, wx.DefaultSize, 0 )
        bSizer1.Add( self.m_buttonStop, 0, wx.ALIGN_CENTER_VERTICAL|wx.RIGHT, 15 )

        self.m_buttonExit = wx.Button( self.m_panel1, wx.ID_ANY, u"Salir", wx.DefaultPosition, wx.DefaultSize, 0 )
        bSizer1.Add( self.m_buttonExit, 0, wx.ALIGN_CENTER_VERTICAL|wx.RIGHT, 50 )

        self.m_staticText6 = wx.StaticText( self.m_panel1, wx.ID_ANY, u"Colector", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText6.Wrap( -1 )
        bSizer1.Add( self.m_staticText6, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

        self.m_textCtrlColMac = wx.TextCtrl( self.m_panel1, wx.ID_ANY, u"CC CC CC CC CC CC CC CC", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_textCtrlColMac.SetMinSize( wx.Size( 200,-1 ) )

        bSizer1.Add( self.m_textCtrlColMac, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )


        bSizer11.Add( bSizer1, 1, wx.EXPAND|wx.RIGHT|wx.ALIGN_CENTER_HORIZONTAL, 5 )

        bSizer2 = wx.BoxSizer( wx.HORIZONTAL )

        bSizer24 = wx.BoxSizer( wx.VERTICAL )

        self.m_staticText7 = wx.StaticText( self.m_panel1, wx.ID_ANY, u"IK Master", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText7.Wrap( -1 )
        bSizer24.Add( self.m_staticText7, 0, wx.ALL|wx.ALIGN_CENTER_HORIZONTAL, 5 )

        self.m_textCtrlCabMac = wx.TextCtrl( self.m_panel1, wx.ID_ANY, u"30 30 30 30 31 49 4B 4D", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_textCtrlCabMac.SetMinSize( wx.Size( 200,-1 ) )

        bSizer24.Add( self.m_textCtrlCabMac, 0, wx.ALL|wx.ALIGN_CENTER_HORIZONTAL, 5 )

        self.m_buttonCabSend = wx.Button( self.m_panel1, wx.ID_ANY, u"Enviar", wx.DefaultPosition, wx.DefaultSize, 0 )
        bSizer24.Add( self.m_buttonCabSend, 0, wx.ALIGN_CENTER_HORIZONTAL|wx.TOP, 5 )

        bSizer12 = wx.BoxSizer( wx.HORIZONTAL )

        bSizer291 = wx.BoxSizer( wx.VERTICAL )

        self.m_checkBoxCabReadings = wx.CheckBox( self.m_panel1, wx.ID_ANY, u"(0x10) Lectura de gabinete", wx.DefaultPosition, wx.DefaultSize, 0 )
        bSizer291.Add( self.m_checkBoxCabReadings, 0, wx.ALL, 5 )

        self.m_checkBoxCabMtrA = wx.CheckBox( self.m_panel1, wx.ID_ANY, u"(0x11) Asociacion", wx.DefaultPosition, wx.DefaultSize, 0 )
        bSizer291.Add( self.m_checkBoxCabMtrA, 0, wx.ALL, 5 )

        self.m_checkBoxCabMtrD = wx.CheckBox( self.m_panel1, wx.ID_ANY, u"(0x12) Desasociacion", wx.DefaultPosition, wx.DefaultSize, 0 )
        bSizer291.Add( self.m_checkBoxCabMtrD, 0, wx.ALL, 5 )

        self.m_checkBoxCabMtrDisco = wx.CheckBox( self.m_panel1, wx.ID_ANY, u"(0x13) Discovery (u8 0..1)", wx.DefaultPosition, wx.DefaultSize, 0 )
        bSizer291.Add( self.m_checkBoxCabMtrDisco, 0, wx.ALL, 5 )

        self.m_checkBoxCabOD = wx.CheckBox( self.m_panel1, wx.ID_ANY, u"(0x14) Puerta abierta (u8 0..3)", wx.DefaultPosition, wx.DefaultSize, 0 )
        bSizer291.Add( self.m_checkBoxCabOD, 0, wx.ALL, 5 )

        self.m_checkBoxCabFree = wx.CheckBox( self.m_panel1, wx.ID_ANY, u"(0x15) Libranza (u16 min)", wx.DefaultPosition, wx.DefaultSize, 0 )
        bSizer291.Add( self.m_checkBoxCabFree, 0, wx.ALL, 5 )

        self.m_checkBoxCabSync = wx.CheckBox( self.m_panel1, wx.ID_ANY, u"(0x16) Sincronizacion", wx.DefaultPosition, wx.DefaultSize, 0 )
        bSizer291.Add( self.m_checkBoxCabSync, 0, wx.ALL, 5 )

        self.m_checkBoxCabReadPan = wx.CheckBox( self.m_panel1, wx.ID_ANY, u"(0x1C) Lectura PAN ID", wx.DefaultPosition, wx.DefaultSize, 0 )
        bSizer291.Add( self.m_checkBoxCabReadPan, 0, wx.ALL, 5 )

        self.m_checkBoxCabReadCh = wx.CheckBox( self.m_panel1, wx.ID_ANY, u"(0x1D) Lectura de Canal", wx.DefaultPosition, wx.DefaultSize, 0 )
        bSizer291.Add( self.m_checkBoxCabReadCh, 0, wx.ALL, 5 )

        self.m_checkBoxCabReadIkId = wx.CheckBox( self.m_panel1, wx.ID_ANY, u"(0x19) Lectura de ID IKM", wx.DefaultPosition, wx.DefaultSize, 0 )
        bSizer291.Add( self.m_checkBoxCabReadIkId, 0, wx.ALL, 5 )

        self.m_checkBoxCabStatus = wx.CheckBox( self.m_panel1, wx.ID_ANY, u"(0x24) Lectura estatus de IK Master", wx.DefaultPosition, wx.DefaultSize, 0 )
        bSizer291.Add( self.m_checkBoxCabStatus, 0, wx.ALL, 5 )


        bSizer12.Add( bSizer291, 1, wx.EXPAND, 5 )

        bSizer281 = wx.BoxSizer( wx.VERTICAL )

        self.m_checkBoxCabPOn = wx.CheckBox( self.m_panel1, wx.ID_ANY, u"(0x17) Permit ON (u8 seg)", wx.DefaultPosition, wx.DefaultSize, 0 )
        bSizer281.Add( self.m_checkBoxCabPOn, 0, wx.ALL, 5 )

        self.m_checkBoxCabPOff = wx.CheckBox( self.m_panel1, wx.ID_ANY, u"(0x18) Permit OFF", wx.DefaultPosition, wx.DefaultSize, 0 )
        bSizer281.Add( self.m_checkBoxCabPOff, 0, wx.ALL, 5 )

        self.m_checkBoxCabWritePan = wx.CheckBox( self.m_panel1, wx.ID_ANY, u"(0x1A) Escritura PAN ID (u16 0..0xFFF0)", wx.DefaultPosition, wx.DefaultSize, 0 )
        bSizer281.Add( self.m_checkBoxCabWritePan, 0, wx.ALL, 5 )

        self.m_checkBoxCabWriteCh = wx.CheckBox( self.m_panel1, wx.ID_ANY, u"(0x1B) Escritura de Canal (u8 11..26)", wx.DefaultPosition, wx.DefaultSize, 0 )
        bSizer281.Add( self.m_checkBoxCabWriteCh, 0, wx.ALL, 5 )

        self.m_checkBoxCabMtrAdd = wx.CheckBox( self.m_panel1, wx.ID_ANY, u"(0x1E) Agregar Medidor", wx.DefaultPosition, wx.DefaultSize, 0 )
        bSizer281.Add( self.m_checkBoxCabMtrAdd, 0, wx.ALL, 5 )

        self.m_checkBoxCabMtrErase = wx.CheckBox( self.m_panel1, wx.ID_ANY, u"(0x1F) Borrar Medidor", wx.DefaultPosition, wx.DefaultSize, 0 )
        bSizer281.Add( self.m_checkBoxCabMtrErase, 0, wx.ALL, 5 )

        self.m_checkBoxCabMtrMultC = wx.CheckBox( self.m_panel1, wx.ID_ANY, u"(0x20) Conexion Multiple", wx.DefaultPosition, wx.DefaultSize, 0 )
        bSizer281.Add( self.m_checkBoxCabMtrMultC, 0, wx.ALL, 5 )

        self.m_checkBoxCabMtrMultD = wx.CheckBox( self.m_panel1, wx.ID_ANY, u"(0x21) Desconexion Multiple", wx.DefaultPosition, wx.DefaultSize, 0 )
        bSizer281.Add( self.m_checkBoxCabMtrMultD, 0, wx.ALL, 5 )

        self.m_checkBoxCabEraseDB = wx.CheckBox( self.m_panel1, wx.ID_ANY, u"(0x22) Borrado de Base de datos", wx.DefaultPosition, wx.DefaultSize, 0 )
        bSizer281.Add( self.m_checkBoxCabEraseDB, 0, wx.ALL, 5 )

        self.m_checkBoxCabNewNetwork = wx.CheckBox( self.m_panel1, wx.ID_ANY, u"(0x23) Buscar nueva red Radio", wx.DefaultPosition, wx.DefaultSize, 0 )
        bSizer281.Add( self.m_checkBoxCabNewNetwork, 0, wx.ALL, 5 )

        self.m_checkBoxCabDevices = wx.CheckBox( self.m_panel1, wx.ID_ANY, u"(0x25) Tabla de medidores", wx.DefaultPosition, wx.DefaultSize, 0 )
        bSizer281.Add( self.m_checkBoxCabDevices, 0, wx.ALL, 5 )


        bSizer12.Add( bSizer281, 1, wx.EXPAND, 5 )

        bSizer13 = wx.BoxSizer( wx.VERTICAL )

        self.m_checkBoxCabBootIKM = wx.CheckBox( self.m_panel1, wx.ID_ANY, u"(0x26) Bootloader IK Master (TBD)", wx.DefaultPosition, wx.DefaultSize, 0 )
        bSizer13.Add( self.m_checkBoxCabBootIKM, 0, wx.ALL, 5 )

        self.m_checkBoxCabBootMtr = wx.CheckBox( self.m_panel1, wx.ID_ANY, u"(0x27) Bootloader Medidor (TBD)", wx.DefaultPosition, wx.DefaultSize, 0 )
        bSizer13.Add( self.m_checkBoxCabBootMtr, 0, wx.ALL, 5 )

        self.m_checkBoxCabBootDpy = wx.CheckBox( self.m_panel1, wx.ID_ANY, u"(0x28) Bootloader Display (TBD)", wx.DefaultPosition, wx.DefaultSize, 0 )
        bSizer13.Add( self.m_checkBoxCabBootDpy, 0, wx.ALL, 5 )

        self.m_checkBoxCabxMem = wx.CheckBox( self.m_panel1, wx.ID_ANY, u"(0x29) Memorias externas (TBD)", wx.DefaultPosition, wx.DefaultSize, 0 )
        bSizer13.Add( self.m_checkBoxCabxMem, 0, wx.ALL, 5 )


        bSizer12.Add( bSizer13, 1, wx.EXPAND, 5 )


        bSizer24.Add( bSizer12, 1, wx.EXPAND, 5 )

        self.m_staticText711 = wx.StaticText( self.m_panel1, wx.ID_ANY, u"Parametro IK Master", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText711.Wrap( -1 )
        bSizer24.Add( self.m_staticText711, 0, wx.ALL|wx.ALIGN_CENTER_HORIZONTAL, 5 )

        self.m_textCtrlCabPar = wx.TextCtrl( self.m_panel1, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_textCtrlCabPar.SetMinSize( wx.Size( 80,-1 ) )

        bSizer24.Add( self.m_textCtrlCabPar, 0, wx.ALIGN_CENTER_HORIZONTAL|wx.ALL|wx.EXPAND, 5 )


        bSizer2.Add( bSizer24, 1, wx.EXPAND, 5 )

        bSizer25 = wx.BoxSizer( wx.VERTICAL )

        self.m_staticText8 = wx.StaticText( self.m_panel1, wx.ID_ANY, u"IK Medidor", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText8.Wrap( -1 )
        bSizer25.Add( self.m_staticText8, 0, wx.ALL|wx.ALIGN_CENTER_HORIZONTAL, 5 )

        self.m_textCtrlMtrMac = wx.TextCtrl( self.m_panel1, wx.ID_ANY, u"30 30 31 31 32 54 54 45", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_textCtrlMtrMac.SetMinSize( wx.Size( 200,-1 ) )

        bSizer25.Add( self.m_textCtrlMtrMac, 0, wx.ALL|wx.ALIGN_CENTER_HORIZONTAL, 5 )

        self.m_buttonMtrSend = wx.Button( self.m_panel1, wx.ID_ANY, u"Enviar", wx.DefaultPosition, wx.DefaultSize, 0 )
        bSizer25.Add( self.m_buttonMtrSend, 0, wx.ALL|wx.ALIGN_CENTER_HORIZONTAL, 5 )

        bSizer292 = wx.BoxSizer( wx.VERTICAL )

        self.m_checkBoxMtrRead = wx.CheckBox( self.m_panel1, wx.ID_ANY, u"(0x01) Lectura", wx.DefaultPosition, wx.DefaultSize, 0 )
        bSizer292.Add( self.m_checkBoxMtrRead, 0, wx.ALL, 5 )

        self.m_checkBoxMtrEventsAll = wx.CheckBox( self.m_panel1, wx.ID_ANY, u"(0x02) Lectura todos eventos", wx.DefaultPosition, wx.DefaultSize, 0 )
        bSizer292.Add( self.m_checkBoxMtrEventsAll, 0, wx.ALL, 5 )

        self.m_checkBoxMtrEventsNew = wx.CheckBox( self.m_panel1, wx.ID_ANY, u"(0x03) Lectura nuevos eventos", wx.DefaultPosition, wx.DefaultSize, 0 )
        bSizer292.Add( self.m_checkBoxMtrEventsNew, 0, wx.ALL, 5 )

        self.m_checkBoxMtrRst = wx.CheckBox( self.m_panel1, wx.ID_ANY, u"(0x04) Reset (u8 0..2)", wx.DefaultPosition, wx.DefaultSize, 0 )
        bSizer292.Add( self.m_checkBoxMtrRst, 0, wx.ALL, 5 )

        self.m_checkBoxMtrLoadP = wx.CheckBox( self.m_panel1, wx.ID_ANY, u"(0x05) Lectura perfil de carga (FF FF FF:m d h)", wx.DefaultPosition, wx.DefaultSize, 0 )
        bSizer292.Add( self.m_checkBoxMtrLoadP, 0, wx.ALL, 5 )

        self.m_checkBoxMtrTou = wx.CheckBox( self.m_panel1, wx.ID_ANY, u"(0x06) Lectura TOU (u8 1..4)", wx.DefaultPosition, wx.DefaultSize, 0 )
        bSizer292.Add( self.m_checkBoxMtrTou, 0, wx.ALL, 5 )

        self.m_checkBoxMtrReadTemplate = wx.CheckBox( self.m_panel1, wx.ID_ANY, u"(0x07) Lectura Plantilla", wx.DefaultPosition, wx.DefaultSize, 0 )
        bSizer292.Add( self.m_checkBoxMtrReadTemplate, 0, wx.ALL, 5 )

        self.m_checkBoxMtrWriteTemplate = wx.CheckBox( self.m_panel1, wx.ID_ANY, u"(0x08) Escritura Plantilla", wx.DefaultPosition, wx.DefaultSize, 0 )
        bSizer292.Add( self.m_checkBoxMtrWriteTemplate, 0, wx.ALL, 5 )

        self.m_checkBoxMtrBridge = wx.CheckBox( self.m_panel1, wx.ID_ANY, u"(0x09) Puente Medidor (TBD)", wx.DefaultPosition, wx.DefaultSize, 0 )
        bSizer292.Add( self.m_checkBoxMtrBridge, 0, wx.ALL, 5 )


        bSizer25.Add( bSizer292, 1, wx.ALIGN_CENTER_HORIZONTAL, 5 )

        self.m_staticText7111 = wx.StaticText( self.m_panel1, wx.ID_ANY, u"Parametro IK Medidor", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText7111.Wrap( -1 )
        bSizer25.Add( self.m_staticText7111, 0, wx.ALIGN_CENTER_HORIZONTAL|wx.ALL, 5 )

        self.m_textCtrlMtrPar = wx.TextCtrl( self.m_panel1, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_textCtrlMtrPar.SetMinSize( wx.Size( 80,-1 ) )

        bSizer25.Add( self.m_textCtrlMtrPar, 0, wx.ALIGN_CENTER_HORIZONTAL|wx.ALL, 5 )


        bSizer2.Add( bSizer25, 1, wx.EXPAND, 5 )

        bSizer26 = wx.BoxSizer( wx.VERTICAL )

        self.m_staticText9 = wx.StaticText( self.m_panel1, wx.ID_ANY, u"Display", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText9.Wrap( -1 )
        bSizer26.Add( self.m_staticText9, 0, wx.ALL|wx.ALIGN_CENTER_HORIZONTAL, 5 )

        self.m_textCtrlDpyMac = wx.TextCtrl( self.m_panel1, wx.ID_ANY, u"30 30 31 44 50 59", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_textCtrlDpyMac.SetMinSize( wx.Size( 200,-1 ) )

        bSizer26.Add( self.m_textCtrlDpyMac, 0, wx.ALL|wx.ALIGN_CENTER_HORIZONTAL, 5 )

        self.m_buttonDpySend = wx.Button( self.m_panel1, wx.ID_ANY, u"Enviar", wx.DefaultPosition, wx.DefaultSize, 0 )
        bSizer26.Add( self.m_buttonDpySend, 0, wx.ALL|wx.ALIGN_CENTER_HORIZONTAL, 5 )

        bSizer293 = wx.BoxSizer( wx.VERTICAL )

        self.m_checkBoxDpyMC = wx.CheckBox( self.m_panel1, wx.ID_ANY, u"(0x31) Mensaje Custom (Alfanumerico)", wx.DefaultPosition, wx.DefaultSize, 0 )
        bSizer293.Add( self.m_checkBoxDpyMC, 0, wx.ALL, 5 )

        self.m_checkBoxDpyFlags = wx.CheckBox( self.m_panel1, wx.ID_ANY, u"(0x32) Lecturas a mostrar (u16)", wx.DefaultPosition, wx.DefaultSize, 0 )
        bSizer293.Add( self.m_checkBoxDpyFlags, 0, wx.ALL, 5 )

        self.m_checkBoxDpyDecimalP = wx.CheckBox( self.m_panel1, wx.ID_ANY, u"(0x33) Punto decimal (u8 0..3)", wx.DefaultPosition, wx.DefaultSize, 0 )
        bSizer293.Add( self.m_checkBoxDpyDecimalP, 0, wx.ALL, 5 )

        self.m_checkBoxDpyDecimalP1 = wx.CheckBox( self.m_panel1, wx.ID_ANY, u"(0x34) Puente Display (TBD)", wx.DefaultPosition, wx.DefaultSize, 0 )
        bSizer293.Add( self.m_checkBoxDpyDecimalP1, 0, wx.ALL, 5 )


        bSizer26.Add( bSizer293, 1, wx.ALIGN_CENTER_HORIZONTAL, 5 )

        self.m_staticText7112 = wx.StaticText( self.m_panel1, wx.ID_ANY, u"Parametro Display", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText7112.Wrap( -1 )
        bSizer26.Add( self.m_staticText7112, 0, wx.ALL|wx.ALIGN_CENTER_HORIZONTAL, 5 )

        self.m_textCtrlDpyPar = wx.TextCtrl( self.m_panel1, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_textCtrlDpyPar.SetMinSize( wx.Size( 80,-1 ) )

        bSizer26.Add( self.m_textCtrlDpyPar, 0, wx.ALIGN_CENTER_HORIZONTAL|wx.ALL|wx.EXPAND, 5 )


        bSizer2.Add( bSizer26, 1, wx.EXPAND, 5 )


        bSizer11.Add( bSizer2, 1, wx.EXPAND, 5 )

        self.m_buttonLogClear = wx.Button( self.m_panel1, wx.ID_ANY, u"Limpiar Log", wx.DefaultPosition, wx.DefaultSize, 0 )
        bSizer11.Add( self.m_buttonLogClear, 0, wx.ALL|wx.ALIGN_CENTER_HORIZONTAL, 5 )

        self.m_textCtrlLog = wx.TextCtrl( self.m_panel1, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.TE_MULTILINE )
        self.m_textCtrlLog.SetFont( wx.Font( 9, 75, 90, 90, False, "Courier New" ) )
        self.m_textCtrlLog.SetMinSize( wx.Size( -1,600 ) )

        bSizer11.Add( self.m_textCtrlLog, 1, wx.ALL|wx.EXPAND, 5 )


        self.m_panel1.SetSizer( bSizer11 )
        self.m_panel1.Layout()
        bSizer11.Fit( self.m_panel1 )
        bSizer.Add( self.m_panel1, 1, wx.EXPAND |wx.ALL, 5 )


        self.SetSizer( bSizer )
        self.Layout()

        self.Centre( wx.BOTH )

        #Bindings
        self.m_buttonInit.Bind(wx.EVT_BUTTON, self.Start)
        self.m_buttonStop.Bind(wx.EVT_BUTTON, self.Stop)
        self.m_buttonExit.Bind(wx.EVT_BUTTON, self.Quit)
        self.m_buttonLogClear.Bind(wx.EVT_BUTTON, self.Log_clear)
        self.m_buttonCabSend.Bind(wx.EVT_BUTTON, self.Cab_send)
        self.m_buttonMtrSend.Bind(wx.EVT_BUTTON, self.Meter_send)
        self.m_buttonDpySend.Bind(wx.EVT_BUTTON, self.Display_send)

        # Start condition
        self.m_buttonStop.Disable()

        # Timer enable
        self.timer = wx.Timer(self)
        self.Bind(wx.EVT_TIMER, self.Rx_data, self.timer)

    def Start(self, e):
        # Create a collector serial module
        port = self.m_textCtrlPort.GetValue()
        self.IkProtocol = IkCollector(port, 115200, 'none')

        # Verify port status
        if self.IkProtocol.PortStatus():
            # Announce coordinator start to log
            self.Log_frame(port + ' 8N1@115200', 'Colector START', True)

            # Generate MAC Colector
            self.Orig = self.m_textCtrlColMac.GetValue()

            # Initialize buttons and timer
            self.m_buttonInit.Disable()
            self.m_buttonStop.Enable()
            self.timer.Start(100)
            self.RespFlag = False
        else:
            self.Log_frame(port + ' Error in port', '', True)

    def Stop(self, e):
        self.IkProtocol.Close()
        self.timer.Stop()
        self.m_buttonInit.Enable()
        self.m_buttonStop.Disable()
        self.Log_frame('Port closed', '', True)

    def Rx_data(self, e):
        self.timer.Stop()

        if self.IkProtocol.Poll() is True:
            # Print the frame received
            rx = self.IkProtocol.Get_buffer()
            if self.RespFlag is True:    # If a response had been waited
                self.Log_frame(rx, 'Recibido', True)
                self.RespFlag = False
            else:
                self.Log_frame(rx, 'Recibido', False)

            # Response
                if self.IkProtocol.Get_status() is True:
                    self.Log_frame(self.IkProtocol.IkCmd_in(rx), 'Enviado', True)
                else:
                    self.Log_frame(self.IkProtocol.Get_status(), 'Error', True)

        self.timer.Start(100)

    #def Coord_send(self, e):
        ## Get mac of coordinator
        #Dest = self.m_textCtrlCooMac.GetValue()
        ## Get parameter to send to coordinator
        #parameter = self.m_textCtrlCooPar.GetValue()
        #self.RespFlag = True

        #if self.m_checkBoxCooOD.GetValue() is True:
            #self.Log_frame(self.IkProtocol.IkCmd_out(Dest, self.Orig, 'Open door', parameter), 'Enviado')

        #elif self.m_checkBoxCooFree.GetValue() is True:
            #self.Log_frame(self.IkProtocol.IkCmd_out(Dest, self.Orig, 'Freedom', parameter), 'Enviado')

        #elif self.m_checkBoxCooSync.GetValue() is True:
            #self.Log_frame(self.IkProtocol.IkCmd_out(Dest, self.Orig, 'Sync', parameter), 'Enviado')

        #elif self.m_checkBoxCooReadPan.GetValue() is True:
            #self.Log_frame(self.IkProtocol.IkCmd_out(Dest, self.Orig, 'PAN ID read', parameter), 'Enviado')

        #elif self.m_checkBoxCooReadCh.GetValue() is True:
            #self.Log_frame(self.IkProtocol.IkCmd_out(Dest, self.Orig, 'Channel read', parameter), 'Enviado')

        #elif self.m_checkBoxCooPOn.GetValue() is True:
            #self.Log_frame(self.IkProtocol.IkCmd_out(Dest, self.Orig, 'Permit on', parameter), 'Enviado')

        #elif self.m_checkBoxCooPOff.GetValue() is True:
            #self.Log_frame(self.IkProtocol.IkCmd_out(Dest, self.Orig, 'Permit off', parameter), 'Enviado')

        #elif self.m_checkBoxCooWritePan.GetValue() is True:
            #self.Log_frame(self.IkProtocol.IkCmd_out(Dest, self.Orig, 'PAN ID write', parameter), 'Enviado')

        #elif self.m_checkBoxCooWriteCh.GetValue() is True:
            #self.Log_frame(self.IkProtocol.IkCmd_out(Dest, self.Orig, 'Channel write', parameter), 'Enviado')

    def Cab_send(self, e):
        # Get mac of cabinet
        Dest = self.m_textCtrlCabMac.GetValue()
        # Get parameter to send to cabinet
        parameter = self.m_textCtrlCabPar.GetValue()
        # Get mac of meter
        MeterMac = self.m_textCtrlMtrMac.GetValue()
        # Get mac of display
        DisplayMac = self.m_textCtrlDpyMac.GetValue()
        self.RespFlag = True

        # Column 1
        if self.m_checkBoxCabReadings.GetValue() is True:
            self.Log_frame(self.IkProtocol.IkCmd_out(Dest, self.Orig, 'Cabinet Readings', parameter), 'Enviado')

        if self.m_checkBoxCabMtrA.GetValue() is True:
            self.Log_frame(self.IkProtocol.IkCmd_out(Dest, self.Orig, 'Association', MeterMac + DisplayMac), 'Enviado')

        if self.m_checkBoxCabMtrD.GetValue() is True:
            self.Log_frame(self.IkProtocol.IkCmd_out(Dest, self.Orig, 'Dissociation', MeterMac + DisplayMac), 'Enviado')

        if self.m_checkBoxCabMtrDisco.GetValue() is True:
            self.Log_frame(self.IkProtocol.IkCmd_out(Dest, self.Orig, 'Discovery', parameter), 'Enviado')

        if self.m_checkBoxCabOD.GetValue() is True:
            self.Log_frame(self.IkProtocol.IkCmd_out(Dest, self.Orig, 'Open door', parameter), 'Enviado')

        elif self.m_checkBoxCabFree.GetValue() is True:
            self.Log_frame(self.IkProtocol.IkCmd_out(Dest, self.Orig, 'Freedom', parameter), 'Enviado')

        elif self.m_checkBoxCabSync.GetValue() is True:
            self.Log_frame(self.IkProtocol.IkCmd_out(Dest, self.Orig, 'Sync', parameter), 'Enviado')

        elif self.m_checkBoxCabReadPan.GetValue() is True:
            self.Log_frame(self.IkProtocol.IkCmd_out(Dest, self.Orig, 'PAN ID read', parameter), 'Enviado')

        elif self.m_checkBoxCabReadCh.GetValue() is True:
            self.Log_frame(self.IkProtocol.IkCmd_out(Dest, self.Orig, 'Channel read', parameter), 'Enviado')

        elif self.m_checkBoxCabReadIkId.GetValue() is True:
            self.Log_frame(self.IkProtocol.IkCmd_out(Dest, self.Orig, 'IKM ID', parameter), 'Enviado')

        elif self.m_checkBoxCabStatus.GetValue() is True:
            self.Log_frame(self.IkProtocol.IkCmd_out(Dest, self.Orig, 'IKM Status', parameter), 'Enviado')

        # Column 2
        elif self.m_checkBoxCabPOn.GetValue() is True:
            self.Log_frame(self.IkProtocol.IkCmd_out(Dest, self.Orig, 'Permit on', parameter), 'Enviado')

        elif self.m_checkBoxCabPOff.GetValue() is True:
            self.Log_frame(self.IkProtocol.IkCmd_out(Dest, self.Orig, 'Permit off', parameter), 'Enviado')

        elif self.m_checkBoxCabWritePan.GetValue() is True:
            self.Log_frame(self.IkProtocol.IkCmd_out(Dest, self.Orig, 'PAN ID write', parameter), 'Enviado')

        elif self.m_checkBoxCabWriteCh.GetValue() is True:
            self.Log_frame(self.IkProtocol.IkCmd_out(Dest, self.Orig, 'Channel write', parameter), 'Enviado')

        elif self.m_checkBoxCabMtrAdd.GetValue() is True:
            self.Log_frame(self.IkProtocol.IkCmd_out(Dest, self.Orig, 'Meter add', MeterMac), 'Enviado')

        elif self.m_checkBoxCabMtrErase.GetValue() is True:
            self.Log_frame(self.IkProtocol.IkCmd_out(Dest, self.Orig, 'Meter erase', MeterMac), 'Enviado')

        elif self.m_checkBoxCabMtrMultC.GetValue() is True:
            self.Log_frame(self.IkProtocol.IkCmd_out(Dest, self.Orig, 'Multiple connection', parameter + MeterMac), 'Enviado')

        elif self.m_checkBoxCabMtrMultD.GetValue() is True:
            self.Log_frame(self.IkProtocol.IkCmd_out(Dest, self.Orig, 'Multiple disconnection', parameter + MeterMac), 'Enviado')

        elif self.m_checkBoxCabEraseDB.GetValue() is True:
            self.Log_frame(self.IkProtocol.IkCmd_out(Dest, self.Orig, 'Data base erase', parameter + MeterMac), 'Enviado')

        elif self.m_checkBoxCabNewNetwork.GetValue() is True:
            self.Log_frame(self.IkProtocol.IkCmd_out(Dest, self.Orig, 'New Network', parameter + MeterMac), 'Enviado')

        elif self.m_checkBoxCabDevices.GetValue() is True:
            self.Log_frame(self.IkProtocol.IkCmd_out(Dest, self.Orig, 'Devices table', parameter + MeterMac), 'Enviado')

    def Meter_send(self, e):
        # Get mac of meter
        Dest = self.m_textCtrlMtrMac.GetValue()
        # Get parameter to send to cabinet
        parameter = self.m_textCtrlMtrPar.GetValue()
        self.RespFlag = True

        if self.m_checkBoxMtrRead.GetValue() is True:
            self.Log_frame(self.IkProtocol.IkCmd_out(Dest, self.Orig, 'Meter reading', 0), 'Enviado')

        elif self.m_checkBoxMtrEventsAll.GetValue() is True:
            self.Log_frame(self.IkProtocol.IkCmd_out(Dest, self.Orig, 'Meter events all', 0), 'Enviado')

        elif self.m_checkBoxMtrEventsNew.GetValue() is True:
            self.Log_frame(self.IkProtocol.IkCmd_out(Dest, self.Orig, 'Meter events new', 0), 'Enviado')

        elif self.m_checkBoxMtrRst.GetValue() is True:
            self.Log_frame(self.IkProtocol.IkCmd_out(Dest, self.Orig, 'Meter reset', parameter), 'Enviado')

        elif self.m_checkBoxMtrLoadP.GetValue() is True:
            self.Log_frame(self.IkProtocol.IkCmd_out(Dest, self.Orig, 'Load Profile', parameter), 'Enviado')

        elif self.m_checkBoxMtrTou.GetValue() is True:
            self.Log_frame(self.IkProtocol.IkCmd_out(Dest, self.Orig, 'TOU', parameter), 'Enviado')

        elif self.m_checkBoxMtrReadTemplate.GetValue() is True:
            self.Log_frame(self.IkProtocol.IkCmd_out(Dest, self.Orig, 'Read Templatess', 0), 'Enviado')

        elif self.m_checkBoxMtrWriteTemplate.GetValue() is True:
            self.Log_frame(self.IkProtocol.IkCmd_out(Dest, self.Orig, 'Write Templatess', 0), 'Enviado')

    def Display_send(self, e):
        # Get mac of Display
        Dest = self.m_textCtrlDpyMac.GetValue()
        Dest += ' FF FF'

        # Get parameter to send to Display
        parameter = self.m_textCtrlDpyPar.GetValue()
        self.RespFlag = True

        if self.m_checkBoxDpyMC.GetValue() is True:
            self.Log_frame(self.IkProtocol.IkCmd_out(Dest, self.Orig, 'Custom msg', parameter), 'Enviado')

        elif self.m_checkBoxDpyFlags.GetValue() is True:
            self.Log_frame(self.IkProtocol.IkCmd_out(Dest, self.Orig, 'Readings configurations', parameter), 'Enviado')

        elif self.m_checkBoxDpyDecimalP.GetValue() is True:
            self.Log_frame(self.IkProtocol.IkCmd_out(Dest, self.Orig, 'Decimal Point', parameter), 'Enviado')

    def Log_frame(self, lst, name='', dspace=False):
        if lst != '':
            # Review if type is str or list
            if type(lst) is str:
                st = lst
            else:
                size = len(lst)
                st = ''
                for i in lst:
                    st += str(hex(i)[2:].zfill(2)) + ' '
                st = st.upper()
                st += '-- '
                st += str(size)

            # Select one or two spaces
            if dspace is True:
                st += '\n\n'
            else:
                st += '\n'
            # Form data string
            date = datetime.datetime.now().strftime("%d/%b/%y %I:%M:%S")
            self.m_textCtrlLog.AppendText(date)
            self.m_textCtrlLog.AppendText('  ')
            self.m_textCtrlLog.AppendText(name)
            space = ' ' * (15 - len(name))
            self.m_textCtrlLog.AppendText(space)
            self.m_textCtrlLog.AppendText(st)
        else:
            self.m_textCtrlLog.AppendText('\n')

    def Log_clear(self, e):
        self.m_textCtrlLog.Clear()

    def Quit(self, e):
        try:
            self.IkProtocol.Close()
        except:
            print('Port open: Never used')
        self.timer.Stop()
        self.Close()


if __name__ == '__main__':
    app = wx.App()
    Collector_simulator(None, title='Local Simulator')
    app.MainLoop()





