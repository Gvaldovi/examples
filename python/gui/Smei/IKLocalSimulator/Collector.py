"""
Clase para simular un colector IK
"""
import time
import struct
from Port import Serial


class IkCollector(Serial):

    def __init__(self, port, baud, parity):
        # Initialize serial port
        Serial.__init__(self, port, baud, parity)

    def IkCmd_in(self, frame):
        # Announces, deletes, alarms and events. All responses are OK
        if frame[16] in [0x0A, 0x0B, 0x3A, 0x3B, 0x2A, 0x2B, 0x2C, 0x2D, 0x60, 0x61, 0x70]:
            response = []
            for i in frame[8:16]:
                response.append(i)
            for i in frame[:8]:
                response.append(i)
            response.append(frame[16])
            response.append(0)
            response.append(0)
            return self.Print(response)
        else:
            return 'Error in command'

    def IkCmd_out(self, dest, orig, cmd, param):
        response = []
        data = []

        # Convert string to bytes
        dest1 = bytearray.fromhex(dest)
        orig1 = bytearray.fromhex(orig)

        for i in dest1:
            response.append(i)
        for i in orig1:
            response.append(i)

        # Meter commands
        if cmd == 'Meter reading':
            response.append(0x01)

        elif cmd == 'Meter events all':
            response.append(0x02)

        elif cmd == 'Meter events new':
            response.append(0x03)

        elif cmd == 'Meter reset':
            response.append(0x04)
            data.append(int(param))

        elif cmd == 'Load Profile':
            response.append(0x05)
            Date = bytearray.fromhex(param)
            for i in Date:
                data.append(i)

        elif cmd == 'TOU':
            response.append(0x06)
            data.append(int(param))

        elif cmd == 'Read Template':
            response.append(0x07)

        elif cmd == 'Write Template':
            response.append(0x08)
            for i in range(332):
                data.append(i)

        # Display commands
        elif cmd == 'Custom msg':
            response.append(0x31)
            for i in param:
                data.append(ord(i))

        elif cmd == 'Readings configurations':
            response.append(0x32)
            Readings = bytearray.fromhex(param)
            for i in Readings:
                data.append(i)

        elif cmd == 'Decimal Point':
            response.append(0x33)
            data.append(int(param))

        # Cabinet and Coordinator commands
        elif cmd == 'Cabinet Readings':
            response.append(0x10)

        elif cmd == 'Association':
            response.append(0x11)
            MtrDpy = bytearray.fromhex(param)
            for i in MtrDpy:
                data.append(i)
            #data.extend([0xFF] * 2)  # Revisar si las FF van al ultimo o al principio del display

        elif cmd == 'Dissociation':
            response.append(0x12)
            MtrDpy = bytearray.fromhex(param)
            for i in MtrDpy:
                data.append(i)
            #data.extend([0xFF] * 2)  # Revisar si las FF van al ultimo o al principio del display

        elif cmd == 'Discovery':
            response.append(0x13)
            data.append(int(param))

        elif cmd == 'Open door':
            response.append(0x14)
            data.append(int(param))

        elif cmd == 'Freedom':
            response.append(0x15)
            array = struct.unpack("2B", struct.pack("H", int(param)))
            data.append(array[1])
            data.append(array[0])

        elif cmd == 'Sync':
            response.append(0x16)
            # Get epoch
            epoch = int(time.time())
            epoch -= 946684800
            #epoch2000 = struct.unpack("4B", struct.pack("I", epoch))
            #for i in epoch2000[::-1]:
                #data.append(i)
            epochbytes = epoch.to_bytes(4, byteorder='big')
            for i in epochbytes:
                data.append(i)
            # Insert timezone
            data.append(0xFF)
            data.append(0xFF)
            data.append(0xB9)
            data.append(0xB0)

        elif cmd == 'PAN ID read':
            response.append(0x1C)

        elif cmd == 'Channel read':
            response.append(0x1D)

        elif cmd == 'IKM ID':
            response.append(0x19)

        elif cmd == 'IKM Status':
            response.append(0x24)

        elif cmd == 'Permit on':
            response.append(0x17)
            data.append(int(param))

        elif cmd == 'Permit off':
            response.append(0x18)

        elif cmd == 'PAN ID write':
            response.append(0x1A)
            array = struct.unpack("2B", struct.pack("H", int(param, 0)))
            data.append(array[1])
            data.append(array[0])

        elif cmd == 'Channel write':
            response.append(0x1B)
            data.append(int(param))

        elif cmd == 'Meter add':
            response.append(0x1E)
            Meter = bytearray.fromhex(param)
            for i in Meter:
                data.append(i)

        elif cmd == 'Meter erase':
            response.append(0x1F)
            Meter = bytearray.fromhex(param)
            for i in Meter:
                data.append(i)

        elif cmd == 'Multiple connection' or cmd == 'Multiple disconnection':
            if cmd == 'Multiple connection':
                response.append(0x20)
            else:
                response.append(0x21)

            # Get number of meters and mac of initial meter
            Num = int(param[0])

            if Num > 0 and Num < 4:
                data.append(Num)
                s = param[1:]

                Meter1 = bytearray.fromhex(param[1:])
                for i in Meter1:
                    data.append(i)

                if Num == 2:
                    a = int(bytearray.fromhex(s[:9]))
                    b = "{:03d}".format(a + 1)
                    print(b)
                    c = ''.join("{:02x}".format(ord(x)) for x in b)
                    Meter2 = bytearray.fromhex(c)
                    Meter2 += Meter1[3:]

                    for i in Meter2:
                        data.append(i)

                elif Num == 3:
                    a = int(bytearray.fromhex(s[:9]))
                    b = "{:03d}".format(a + 1)
                    c = ''.join("{:02x}".format(ord(x)) for x in b)
                    Meter2 = bytearray.fromhex(c)
                    Meter2 += Meter1[3:]

                    b1 = "{:03d}".format(a + 2)
                    c1 = ''.join("{:02x}".format(ord(x)) for x in b1)
                    Meter3 = bytearray.fromhex(c1)
                    Meter3 += Meter1[3:]

                    for i in Meter2:
                        data.append(i)
                    for i in Meter3:
                        data.append(i)

            else:
                return 'Error cmd Mult'

        elif cmd == 'Data base erase':
            response.append(0x22)

        elif cmd == 'New Network':
            response.append(0x23)

        elif cmd == 'Devices table':
            response.append(0x25)

        else:
            return 'Error in command'

        # Complete frame
        if len(data) == 0:
            response.append(0x00)
        elif len(data) <= 127:
            response.append(len(data))
        elif len(data) <= 256:
            response.append(0x80)
            response.append(len(data))
        for i in data:
            response.append(i)
        return self.Print(response)