"""
Clase para simular diferentes protocolos que tienen interaccion con un puerto UART
computadora.
"""
from Port import Serial
import time
import struct


class Mrf(Serial):

    def __init__(self, port, baud, parity, mac_address):
        # Initialize serial port
        Serial.__init__(self, port, baud, parity)

        """
        Internal MRF registers
        """
        self.registers = {0: [0x24, 0xDA, 0xB6, 0x0A, 0x01, 0x09, 0x10, 0x11],     # MAC Address
                     8: [0x00, 0x00],                                         # FW Version
                     10: [0, 0],                                              # HW Version
                     12: [0, 0],                                              # Channel
                     14: [0, 0],                                              # PAN ID
                     16: [0, 0, 0, 0],                                        # Universal Time
                     20: [0, 0, 0, 0],                                        # Time zone
                     24: [0, 0, 0, 0],                                        # Flags
                     28: [0, 0],                                              # Radio type
                     30: [0, 0],                                              # Radio Power
                     32: [0x10, 0x11]}                                        # Device counter

        # If length is 8 then mac_address is correct
        if len(mac_address) == 8:
            self.registers[0] = mac_address

    def Mac(self):
        return self.registers[0]

    def MrfCmd(self):    # Command dispatcher
        if self.buffer[1] == 0x03:
            return self.Local_read()
        elif self.buffer[1] == 0x10:
            return self.Local_write()
        elif self.buffer[1] == 0x04:
            return self.Remote_ok()
        elif self.buffer[1] == 0x05:
            return ''
        else:
            return "Error code"

    def Local_read(self):
        # Copy number of bytes and address to a variable
        size = self.buffer[3]
        address = self.buffer[2]
        response = [0x01, 0x03, size]

        while size:
            if size >= len(self.registers[address]):
                for i in self.registers[address]:
                    response.append(i)
                size -= len(self.registers[address])
                address += len(self.registers[address])
            else:
                break
        # Send response to COM
        return self.Print(response)

    def Local_write(self):
        response = [0x01, 0x10, 0x01, 0x00, 0x01, 0x8d]
        data = []
        for i in self.buffer[4:-2]:
            data.append(i)
        del self.registers[self.buffer[2]]

        if len(data) is 1:
            self.registers[self.buffer[2]] = [data[0], 0]
        else:
            self.registers[self.buffer[2]] = data
        # Send response to COM
        return self.Print(response)

    def Remote_ok(self):
        # Save MAC dest
        self.Mac_destiny = self.buffer[2:10]
        # Show only the message
        self.buffer = self.buffer[12: (12 + self.buffer[11])]
        response = [0x01, 0x04, 0x01, 0x00]
        # Send response to COM
        return self.Print(response)

    def Mac_dest(self):
        return self.Mac_destiny

    def Remote(self, frame, orig):
        response = []
        response.append(frame[0])
        response.append(0x05)
        for i in orig:
            response.append(i)
        for i in frame[10:-2]:
            response.append(i)
        # Send response to COM
        return self.Print(response)

    def Local_Notification(self, noti, device=[]):
        response = [0x01, 0x06]
        if noti == 'Reset':
            response.append(0x01)
            response.append(0xF0)
        elif noti == 'Device':
            response.append(0x0B)
            response.append(0xF1)
            for i in device:
                response.append(i)
            response.append(0xAA)
            response.append(0xAA)
        elif noti == 'Network':
            response.append(0x01)
            response.append(0xF2)
        elif noti == 'Time':
            response.append(0x01)
            response.append(0xF3)
        else:
            return 'Error in command'
        # Send response to COM
        return self.Print(response)

    def Remote_Notification(self, orig, packet, data):
        response = [0x01, 0x05]
        for i in orig:
            response.append(i)
        response.append(packet)
        response.append(len(data))
        for i in data:
            response.append(i)
        # Send response to COM
        return self.Print(response)


class IkCabs(Mrf):
    """ IkCabs simulate a group of cabinets with their corresponding meters and displays """
    def __init__(self, port, baud, parity, mac):
        Mrf.__init__(self, port, baud, parity, mac)

    def IkCmd_in(self, frame):
        print(frame)
        # All responses are OK
        Cabinet_cmd = [i for i in range(16, 34)]
        Meter_cmd = [1, 2, 3]
        Display_cmd = [0x31, 0x32]

        # Start response
        response = []
        for i in frame[8:16]:
            response.append(i)
        for i in frame[:8]:
            response.append(i)
        response.append(frame[16])

        if frame[16] in Cabinet_cmd or frame[16] in Display_cmd:
            response.append(0)    # Status
            response.append(0)    # Size

        elif frame[16] in Meter_cmd:
            reading = [i for i in range(84)]
            response.append(0)               # Status
            response.append(len(reading))    # Size
            for i in reading:
                response.append(i)           # Data
        else:
            return 'Error en comando'

        response = self.CrcAppend(response)
        return self.Remote_Notification(self.Mac_dest(), 0, response)

    def IkCmd_out(self, dest, orig, cmd, param):
        response = []
        data = []
        for i in dest:
            response.append(i)
        for i in orig:
            response.append(i)

        # Meter commands
        if cmd == 'Meter announce':
            response.append(0x0A)
            data = [i for i in range(27)]

        elif cmd == 'Meter delete':
            response.append(0x0B)

        # Display commands
        elif cmd == 'Display announce':
            response.append(0x3A)
            data = [i for i in range(27)]

        elif cmd == 'Display delete':
            response.append(0x3B)

        # Cabinet commands
        elif cmd == 'Cabinet announce':
            response.append(0x2A)
            data = param

        else:
            return 'Error in command'

        # Complete frame
        if len(data) == 0:
            response.append(0x00)
        elif len(data) <= 127:
            response.append(len(data))
        elif len(data) <= 256:
            response.append(0x80)
            response.append(len(data))
        for i in data:
            response.append(i)
        response = self.CrcAppend(response)

        return self.Remote_Notification([0] * 8, 0, response)
        #return self.Print(response)


class IkCollector(Serial):

    def __init__(self, port, baud, parity):
        # Initialize serial port
        Serial.__init__(self, port, baud, parity)

    def IkCmd_in(self, frame):
        # Announces, deletes, alarms and events. All responses are OK
        if frame[16] in [0x0A, 0x0B, 0x3A, 0x3B, 0x2A, 0x2B, 0x2C, 0x2D, 0x60, 0x61, 0x70]:
            response = []
            for i in frame[8:16]:
                response.append(i)
            for i in frame[:8]:
                response.append(i)
            response.append(frame[16])
            response.append(0)
            response.append(0)
            return self.Print(response)
        else:
            return 'Error in command'

    def IkCmd_out(self, dest, orig, cmd, param):
        response = []
        data = []
        for i in dest:
            response.append(i)
        for i in orig:
            response.append(i)

        # Meter commands
        if cmd == 'Meter reading':
            response.append(0x01)

        elif cmd == 'Meter conection':
            response.append(0x02)

        elif cmd == 'Meter disconection':
            response.append(0x03)

        # Display commands
        elif cmd == 'Custom msg':
            response.append(0x31)
            data.append(0x00)    # Harcodeado para pruebas
            data.append(0x10)
            data.append(0x30)

        elif cmd == 'Readings configurations':
            response.append(0x32)
            data.append(0x01)    # Lecturas a mostrar harcodeadas
            data.append(0x01)
            data.append(0x01)
            data.append(0x01)

        # Cabinet and Coordinator commands
        elif cmd == 'Open door':
            print('llego aqui')
            response.append(0x14)
            data.append(int(param))

        elif cmd == 'Freedom':
            response.append(0x15)
            array = struct.unpack("2B", struct.pack("H", int(param)))
            data.append(array[1])
            data.append(array[0])

        elif cmd == 'Sync':
            response.append(0x16)
            # Get epoch
            epoch = int(time.time())
            epoch -= 946684800
            epoch2000 = struct.unpack("4B", struct.pack("I", epoch))
            for i in epoch2000[::-1]:
                data.append(i)
            # Insert timezone
            data.append(0)
            data.append(0)
            data.append(0)
            data.append(5)

        elif cmd == 'PAN ID read':
            response.append(0x1C)

        elif cmd == 'Channel read':
            response.append(0x1D)

        elif cmd == 'Permit on':
            response.append(0x17)
            data.append(int(param))

        elif cmd == 'Permit off':
            response.append(0x18)

        elif cmd == 'PAN ID write':
            response.append(0x1A)
            array = struct.unpack("2B", struct.pack("H", int(param, 0)))
            data.append(array[1])
            data.append(array[0])

        elif cmd == 'Channel write':
            response.append(0x1B)
            data.append(int(param))

        else:
            return 'Error in command'

        # Complete frame
        if len(data) == 0:
            response.append(0x00)
        elif len(data) <= 127:
            response.append(len(data))
        elif len(data) <= 256:
            response.append(0x80)
            response.append(len(data))
        for i in data:
            response.append(i)
        return self.Print(response)


class IkMeters(Serial):

    def __init__(self, port, baud, parity):
        # Initialize serial port
        Serial.__init__(self, port, baud, parity)

        """
        Meters table
        """
        self.Meters = {}

    #def IkCmd_in(self, frame):
        ## Review onlye valid commands
        #if frame[8] is 0x03:

        #elif frame[8] is 0x04:
            #self.Meter.append()
        #elif frame[8] is 0x10:
            #response = []
            #for i in frame[8:16]:
                #response.append(i)
            #for i in frame[:8]:
                #response.append(i)
            #response.append(frame[16])
            #response.append(0)
            #response.append(0)
            #return self.Print(response)
        #else:
            #return 'Error in command'



#dest = [0x24, 0xDA, 0xB6]
#orig = [0x24, 0xDA, 0xB6]
#cmd = 0x02
#data = [0x24, 0xDA, 0xB6]
#Puerto = IkProtocol('COM11')

#while 1:
    #if Puerto.Poll() is True:
        #Frame = Puerto.Get_buffer()
        #print(Frame)

        #if Frame in ['Bad Size', 'Bad CRC']:
            #print(Puerto.Cmd(dest, orig, cmd, data))
        #else:
            #print('Salir')
            #Puerto.Close()
            #break