# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'mainwindowRZxvxc.ui'
##
## Created by: Qt User Interface Compiler version 5.15.16
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import *  # type: ignore
from PySide6.QtGui import *  # type: ignore
from PySide6.QtWidgets import *  # type: ignore


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        if not MainWindow.objectName():
            MainWindow.setObjectName(u"MainWindow")
        MainWindow.resize(913, 676)
        MainWindow.setTabletTracking(True)
        MainWindow.setDocumentMode(True)
        MainWindow.setTabShape(QTabWidget.Rounded)
        self.actionConnect = QAction(MainWindow)
        self.actionConnect.setObjectName(u"actionConnect")
        self.action115200 = QAction(MainWindow)
        self.action115200.setObjectName(u"action115200")
        self.action115200.setCheckable(True)
        self.action115200.setChecked(False)
        self.action115200.setEnabled(True)
        self.action115200.setVisible(True)
        self.action57600 = QAction(MainWindow)
        self.action57600.setObjectName(u"action57600")
        self.action57600.setCheckable(True)
        self.action57600.setChecked(False)
        self.action57600.setEnabled(True)
        self.action9600 = QAction(MainWindow)
        self.action9600.setObjectName(u"action9600")
        self.action9600.setCheckable(True)
        self.action9600.setEnabled(True)
        self.actionttyUSB0 = QAction(MainWindow)
        self.actionttyUSB0.setObjectName(u"actionttyUSB0")
        self.actionttyUSB0.setCheckable(True)
        self.actionttyUSB0.setChecked(False)
        self.actionttyUSB1 = QAction(MainWindow)
        self.actionttyUSB1.setObjectName(u"actionttyUSB1")
        self.actionttyUSB1.setCheckable(True)
        self.actionExit = QAction(MainWindow)
        self.actionExit.setObjectName(u"actionExit")
        self.centralwidget = QWidget(MainWindow)
        self.centralwidget.setObjectName(u"centralwidget")
        self.centralwidget.setMinimumSize(QSize(500, 500))
        self.verticalLayout_5 = QVBoxLayout(self.centralwidget)
        self.verticalLayout_5.setObjectName(u"verticalLayout_5")
        self.verticalLayout_5.setSizeConstraint(QLayout.SetNoConstraint)
        self.verticalLayout_5.setContentsMargins(0, 0, 0, 0)
        self.tabWidget = QTabWidget(self.centralwidget)
        self.tabWidget.setObjectName(u"tabWidget")
        self.tabWidget.setEnabled(True)
        sizePolicy = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.tabWidget.sizePolicy().hasHeightForWidth())
        self.tabWidget.setSizePolicy(sizePolicy)
        self.tab_Log = QWidget()
        self.tab_Log.setObjectName(u"tab_Log")
        self.verticalLayout_4 = QVBoxLayout(self.tab_Log)
        self.verticalLayout_4.setObjectName(u"verticalLayout_4")
        self.verticalLayout_6 = QVBoxLayout()
        self.verticalLayout_6.setObjectName(u"verticalLayout_6")
        self.textEdit = QTextEdit(self.tab_Log)
        self.textEdit.setObjectName(u"textEdit")

        self.verticalLayout_6.addWidget(self.textEdit)


        self.verticalLayout_4.addLayout(self.verticalLayout_6)

        self.horizontalLayout = QHBoxLayout()
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.pushButton_logClear = QPushButton(self.tab_Log)
        self.pushButton_logClear.setObjectName(u"pushButton_logClear")

        self.horizontalLayout.addWidget(self.pushButton_logClear)

        self.horizontalSpacer_2 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout.addItem(self.horizontalSpacer_2)


        self.verticalLayout_4.addLayout(self.horizontalLayout)

        self.tabWidget.addTab(self.tab_Log, "")
        self.tab_J1979 = QWidget()
        self.tab_J1979.setObjectName(u"tab_J1979")
        self.tab_J1979.setEnabled(True)
        self.verticalLayout_2 = QVBoxLayout(self.tab_J1979)
        self.verticalLayout_2.setObjectName(u"verticalLayout_2")
        self.horizontalLayout_2 = QHBoxLayout()
        self.horizontalLayout_2.setObjectName(u"horizontalLayout_2")
        self.horizontalLayout_2.setSizeConstraint(QLayout.SetNoConstraint)
        self.pushButton_j1979_Reset = QPushButton(self.tab_J1979)
        self.pushButton_j1979_Reset.setObjectName(u"pushButton_j1979_Reset")

        self.horizontalLayout_2.addWidget(self.pushButton_j1979_Reset)

        self.horizontalSpacer_3 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout_2.addItem(self.horizontalSpacer_3)

        self.label = QLabel(self.tab_J1979)
        self.label.setObjectName(u"label")

        self.horizontalLayout_2.addWidget(self.label)

        self.comboBox_j1979_protocol = QComboBox(self.tab_J1979)
        self.comboBox_j1979_protocol.addItem("")
        self.comboBox_j1979_protocol.addItem("")
        self.comboBox_j1979_protocol.addItem("")
        self.comboBox_j1979_protocol.addItem("")
        self.comboBox_j1979_protocol.addItem("")
        self.comboBox_j1979_protocol.addItem("")
        self.comboBox_j1979_protocol.addItem("")
        self.comboBox_j1979_protocol.addItem("")
        self.comboBox_j1979_protocol.addItem("")
        self.comboBox_j1979_protocol.addItem("")
        self.comboBox_j1979_protocol.setObjectName(u"comboBox_j1979_protocol")
        self.comboBox_j1979_protocol.setMaximumSize(QSize(500, 16777215))

        self.horizontalLayout_2.addWidget(self.comboBox_j1979_protocol)

        self.horizontalSpacer = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout_2.addItem(self.horizontalSpacer)

        self.pushButton_j1979_Start = QPushButton(self.tab_J1979)
        self.pushButton_j1979_Start.setObjectName(u"pushButton_j1979_Start")

        self.horizontalLayout_2.addWidget(self.pushButton_j1979_Start)


        self.verticalLayout_2.addLayout(self.horizontalLayout_2)

        self.horizontalLayout_14 = QHBoxLayout()
        self.horizontalLayout_14.setObjectName(u"horizontalLayout_14")
        self.comboBox_j1979_var1 = QComboBox(self.tab_J1979)
        self.comboBox_j1979_var1.setObjectName(u"comboBox_j1979_var1")
        self.comboBox_j1979_var1.setMaxVisibleItems(10)

        self.horizontalLayout_14.addWidget(self.comboBox_j1979_var1)

        self.horizontalSlider_j1979_var1 = QSlider(self.tab_J1979)
        self.horizontalSlider_j1979_var1.setObjectName(u"horizontalSlider_j1979_var1")
        self.horizontalSlider_j1979_var1.setOrientation(Qt.Horizontal)

        self.horizontalLayout_14.addWidget(self.horizontalSlider_j1979_var1)

        self.doubleSpinBox_j1979_var1 = QDoubleSpinBox(self.tab_J1979)
        self.doubleSpinBox_j1979_var1.setObjectName(u"doubleSpinBox_j1979_var1")
        self.doubleSpinBox_j1979_var1.setMinimumSize(QSize(100, 0))
        self.doubleSpinBox_j1979_var1.setMaximum(99.989999999999995)

        self.horizontalLayout_14.addWidget(self.doubleSpinBox_j1979_var1)


        self.verticalLayout_2.addLayout(self.horizontalLayout_14)

        self.horizontalLayout_4 = QHBoxLayout()
        self.horizontalLayout_4.setObjectName(u"horizontalLayout_4")
        self.comboBox_j1979_var2 = QComboBox(self.tab_J1979)
        self.comboBox_j1979_var2.setObjectName(u"comboBox_j1979_var2")
        self.comboBox_j1979_var2.setMaxVisibleItems(10)

        self.horizontalLayout_4.addWidget(self.comboBox_j1979_var2)

        self.horizontalSlider_j1979_var2 = QSlider(self.tab_J1979)
        self.horizontalSlider_j1979_var2.setObjectName(u"horizontalSlider_j1979_var2")
        self.horizontalSlider_j1979_var2.setOrientation(Qt.Horizontal)

        self.horizontalLayout_4.addWidget(self.horizontalSlider_j1979_var2)

        self.doubleSpinBox_j1979_var2 = QDoubleSpinBox(self.tab_J1979)
        self.doubleSpinBox_j1979_var2.setObjectName(u"doubleSpinBox_j1979_var2")
        self.doubleSpinBox_j1979_var2.setMinimumSize(QSize(100, 0))

        self.horizontalLayout_4.addWidget(self.doubleSpinBox_j1979_var2)


        self.verticalLayout_2.addLayout(self.horizontalLayout_4)

        self.horizontalLayout_5 = QHBoxLayout()
        self.horizontalLayout_5.setObjectName(u"horizontalLayout_5")
        self.comboBox_j1979_var3 = QComboBox(self.tab_J1979)
        self.comboBox_j1979_var3.setObjectName(u"comboBox_j1979_var3")
        self.comboBox_j1979_var3.setMaxVisibleItems(10)

        self.horizontalLayout_5.addWidget(self.comboBox_j1979_var3)

        self.horizontalSlider_j1979_var3 = QSlider(self.tab_J1979)
        self.horizontalSlider_j1979_var3.setObjectName(u"horizontalSlider_j1979_var3")
        self.horizontalSlider_j1979_var3.setOrientation(Qt.Horizontal)

        self.horizontalLayout_5.addWidget(self.horizontalSlider_j1979_var3)

        self.doubleSpinBox_j1979_var3 = QDoubleSpinBox(self.tab_J1979)
        self.doubleSpinBox_j1979_var3.setObjectName(u"doubleSpinBox_j1979_var3")
        self.doubleSpinBox_j1979_var3.setMinimumSize(QSize(100, 0))

        self.horizontalLayout_5.addWidget(self.doubleSpinBox_j1979_var3)


        self.verticalLayout_2.addLayout(self.horizontalLayout_5)

        self.horizontalLayout_6 = QHBoxLayout()
        self.horizontalLayout_6.setObjectName(u"horizontalLayout_6")
        self.table_j1979 = QTableWidget(self.tab_J1979)
        if (self.table_j1979.columnCount() < 6):
            self.table_j1979.setColumnCount(6)
        __qtablewidgetitem = QTableWidgetItem()
        self.table_j1979.setHorizontalHeaderItem(0, __qtablewidgetitem)
        __qtablewidgetitem1 = QTableWidgetItem()
        self.table_j1979.setHorizontalHeaderItem(1, __qtablewidgetitem1)
        __qtablewidgetitem2 = QTableWidgetItem()
        self.table_j1979.setHorizontalHeaderItem(2, __qtablewidgetitem2)
        __qtablewidgetitem3 = QTableWidgetItem()
        self.table_j1979.setHorizontalHeaderItem(3, __qtablewidgetitem3)
        __qtablewidgetitem4 = QTableWidgetItem()
        self.table_j1979.setHorizontalHeaderItem(4, __qtablewidgetitem4)
        __qtablewidgetitem5 = QTableWidgetItem()
        self.table_j1979.setHorizontalHeaderItem(5, __qtablewidgetitem5)
        self.table_j1979.setObjectName(u"table_j1979")
        self.table_j1979.setEnabled(True)
        sizePolicy.setHeightForWidth(self.table_j1979.sizePolicy().hasHeightForWidth())
        self.table_j1979.setSizePolicy(sizePolicy)
        self.table_j1979.setMaximumSize(QSize(889, 16777215))
        self.table_j1979.setSizeIncrement(QSize(0, 0))
        self.table_j1979.setTextElideMode(Qt.ElideMiddle)
        self.table_j1979.horizontalHeader().setCascadingSectionResizes(False)
        self.table_j1979.horizontalHeader().setStretchLastSection(False)
        self.table_j1979.verticalHeader().setCascadingSectionResizes(False)
        self.table_j1979.verticalHeader().setStretchLastSection(False)

        self.horizontalLayout_6.addWidget(self.table_j1979)


        self.verticalLayout_2.addLayout(self.horizontalLayout_6)

        self.tabWidget.addTab(self.tab_J1979, "")
        self.tab_J1939 = QWidget()
        self.tab_J1939.setObjectName(u"tab_J1939")
        self.tab_J1939.setEnabled(False)
        self.tab_J1939.setMinimumSize(QSize(335, 0))
        self.verticalLayout_3 = QVBoxLayout(self.tab_J1939)
        self.verticalLayout_3.setObjectName(u"verticalLayout_3")
        self.horizontalLayout_10 = QHBoxLayout()
        self.horizontalLayout_10.setObjectName(u"horizontalLayout_10")
        self.comboBox_j1939Var1 = QComboBox(self.tab_J1939)
        self.comboBox_j1939Var1.addItem("")
        self.comboBox_j1939Var1.addItem("")
        self.comboBox_j1939Var1.addItem("")
        self.comboBox_j1939Var1.setObjectName(u"comboBox_j1939Var1")
        self.comboBox_j1939Var1.setEditable(False)
        self.comboBox_j1939Var1.setMaxVisibleItems(10)

        self.horizontalLayout_10.addWidget(self.comboBox_j1939Var1)

        self.horizontalSlider_j1939Var1 = QSlider(self.tab_J1939)
        self.horizontalSlider_j1939Var1.setObjectName(u"horizontalSlider_j1939Var1")
        self.horizontalSlider_j1939Var1.setSingleStep(0)
        self.horizontalSlider_j1939Var1.setOrientation(Qt.Horizontal)

        self.horizontalLayout_10.addWidget(self.horizontalSlider_j1939Var1)

        self.doubleSpinBox_j1939Var1 = QDoubleSpinBox(self.tab_J1939)
        self.doubleSpinBox_j1939Var1.setObjectName(u"doubleSpinBox_j1939Var1")
        self.doubleSpinBox_j1939Var1.setEnabled(False)
        self.doubleSpinBox_j1939Var1.setMinimumSize(QSize(100, 0))
        self.doubleSpinBox_j1939Var1.setCorrectionMode(QAbstractSpinBox.CorrectToNearestValue)

        self.horizontalLayout_10.addWidget(self.doubleSpinBox_j1939Var1)


        self.verticalLayout_3.addLayout(self.horizontalLayout_10)

        self.verticalSpacer = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.verticalLayout_3.addItem(self.verticalSpacer)

        self.horizontalLayout_11 = QHBoxLayout()
        self.horizontalLayout_11.setObjectName(u"horizontalLayout_11")
        self.label_5 = QLabel(self.tab_J1939)
        self.label_5.setObjectName(u"label_5")

        self.horizontalLayout_11.addWidget(self.label_5)

        self.doubleSpinBox_j1939Period = QDoubleSpinBox(self.tab_J1939)
        self.doubleSpinBox_j1939Period.setObjectName(u"doubleSpinBox_j1939Period")
        self.doubleSpinBox_j1939Period.setMinimumSize(QSize(100, 0))
        self.doubleSpinBox_j1939Period.setAutoFillBackground(True)
        self.doubleSpinBox_j1939Period.setReadOnly(False)
        self.doubleSpinBox_j1939Period.setKeyboardTracking(False)
        self.doubleSpinBox_j1939Period.setDecimals(1)
        self.doubleSpinBox_j1939Period.setMinimum(0.200000000000000)
        self.doubleSpinBox_j1939Period.setSingleStep(0.200000000000000)
        self.doubleSpinBox_j1939Period.setValue(1.000000000000000)

        self.horizontalLayout_11.addWidget(self.doubleSpinBox_j1939Period)

        self.horizontalSpacer_4 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout_11.addItem(self.horizontalSpacer_4)


        self.verticalLayout_3.addLayout(self.horizontalLayout_11)

        self.tabWidget.addTab(self.tab_J1939, "")

        self.verticalLayout_5.addWidget(self.tabWidget)

        MainWindow.setCentralWidget(self.centralwidget)
        self.menuBar = QMenuBar(MainWindow)
        self.menuBar.setObjectName(u"menuBar")
        self.menuBar.setGeometry(QRect(0, 0, 913, 23))
        self.menuConfiguration = QMenu(self.menuBar)
        self.menuConfiguration.setObjectName(u"menuConfiguration")
        self.menuBaud = QMenu(self.menuConfiguration)
        self.menuBaud.setObjectName(u"menuBaud")
        self.menuPort = QMenu(self.menuConfiguration)
        self.menuPort.setObjectName(u"menuPort")
        MainWindow.setMenuBar(self.menuBar)

        self.menuBar.addAction(self.menuConfiguration.menuAction())
        self.menuConfiguration.addAction(self.menuPort.menuAction())
        self.menuConfiguration.addAction(self.menuBaud.menuAction())
        self.menuConfiguration.addAction(self.actionConnect)
        self.menuConfiguration.addAction(self.actionExit)
        self.menuBaud.addAction(self.action115200)
        self.menuBaud.addAction(self.action57600)
        self.menuBaud.addAction(self.action9600)
        self.menuPort.addAction(self.actionttyUSB0)
        self.menuPort.addAction(self.actionttyUSB1)

        self.retranslateUi(MainWindow)

        self.tabWidget.setCurrentIndex(1)


        QMetaObject.connectSlotsByName(MainWindow)
    # setupUi

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(QCoreApplication.translate("MainWindow", u"FJSaint", None))
        self.actionConnect.setText(QCoreApplication.translate("MainWindow", u"Connect", None))
        self.action115200.setText(QCoreApplication.translate("MainWindow", u"115200", None))
        self.action57600.setText(QCoreApplication.translate("MainWindow", u"57600", None))
        self.action9600.setText(QCoreApplication.translate("MainWindow", u"9600", None))
        self.actionttyUSB0.setText(QCoreApplication.translate("MainWindow", u"ttyUSB0", None))
        self.actionttyUSB1.setText(QCoreApplication.translate("MainWindow", u"ttyUSB1", None))
        self.actionExit.setText(QCoreApplication.translate("MainWindow", u"Exit", None))
        self.textEdit.setHtml(QCoreApplication.translate("MainWindow", u"<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Noto Sans'; font-size:10pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Saint simulator V1.0</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">1. Select Port and Baud equals to 57600 from &quot;Configuration&quot; menu and click on &quot;Connect&quot;.</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">2. Go to a simulation tab.</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">3. Reset saint to veri"
                        "fy connection was stablished.</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">4. Start simulation.</p></body></html>", None))
        self.textEdit.setPlaceholderText("")
        self.pushButton_logClear.setText(QCoreApplication.translate("MainWindow", u"Clear", None))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_Log), QCoreApplication.translate("MainWindow", u"Log", None))
        self.pushButton_j1979_Reset.setText(QCoreApplication.translate("MainWindow", u"Reset Saint", None))
        self.label.setText(QCoreApplication.translate("MainWindow", u"Select Protocol", None))
        self.comboBox_j1979_protocol.setItemText(0, QCoreApplication.translate("MainWindow", u"CAN250 11bits", None))
        self.comboBox_j1979_protocol.setItemText(1, QCoreApplication.translate("MainWindow", u"CAN250 29bits", None))
        self.comboBox_j1979_protocol.setItemText(2, QCoreApplication.translate("MainWindow", u"CAN500 11bits", None))
        self.comboBox_j1979_protocol.setItemText(3, QCoreApplication.translate("MainWindow", u"CAN500 29bits", None))
        self.comboBox_j1979_protocol.setItemText(4, QCoreApplication.translate("MainWindow", u"KWP Fast", None))
        self.comboBox_j1979_protocol.setItemText(5, QCoreApplication.translate("MainWindow", u"KWP Slow", None))
        self.comboBox_j1979_protocol.setItemText(6, QCoreApplication.translate("MainWindow", u"ISO9141-2", None))
        self.comboBox_j1979_protocol.setItemText(7, QCoreApplication.translate("MainWindow", u"J1850 VPW", None))
        self.comboBox_j1979_protocol.setItemText(8, QCoreApplication.translate("MainWindow", u"J1850 PWM", None))
        self.comboBox_j1979_protocol.setItemText(9, "")

        self.pushButton_j1979_Start.setText(QCoreApplication.translate("MainWindow", u"Start", None))
        self.comboBox_j1979_var1.setCurrentText("")
        self.comboBox_j1979_var2.setCurrentText("")
        self.comboBox_j1979_var3.setCurrentText("")
        ___qtablewidgetitem = self.table_j1979.horizontalHeaderItem(0)
        ___qtablewidgetitem.setText(QCoreApplication.translate("MainWindow", u"PID", None));
        ___qtablewidgetitem1 = self.table_j1979.horizontalHeaderItem(1)
        ___qtablewidgetitem1.setText(QCoreApplication.translate("MainWindow", u"Service", None));
        ___qtablewidgetitem2 = self.table_j1979.horizontalHeaderItem(2)
        ___qtablewidgetitem2.setText(QCoreApplication.translate("MainWindow", u"ID", None));
        ___qtablewidgetitem3 = self.table_j1979.horizontalHeaderItem(3)
        ___qtablewidgetitem3.setText(QCoreApplication.translate("MainWindow", u"Value", None));
        ___qtablewidgetitem4 = self.table_j1979.horizontalHeaderItem(4)
        ___qtablewidgetitem4.setText(QCoreApplication.translate("MainWindow", u"Units", None));
        ___qtablewidgetitem5 = self.table_j1979.horizontalHeaderItem(5)
        ___qtablewidgetitem5.setText(QCoreApplication.translate("MainWindow", u"Active", None));
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_J1979), QCoreApplication.translate("MainWindow", u"J1979", None))
        self.comboBox_j1939Var1.setItemText(0, QCoreApplication.translate("MainWindow", u"Select Variable", None))
        self.comboBox_j1939Var1.setItemText(1, QCoreApplication.translate("MainWindow", u"RPM", None))
        self.comboBox_j1939Var1.setItemText(2, QCoreApplication.translate("MainWindow", u"Temperature", None))

        self.label_5.setText(QCoreApplication.translate("MainWindow", u"Period (s)", None))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_J1939), QCoreApplication.translate("MainWindow", u"J1939", None))
        self.menuConfiguration.setTitle(QCoreApplication.translate("MainWindow", u"Configuration", None))
        self.menuBaud.setTitle(QCoreApplication.translate("MainWindow", u"Baud", None))
        self.menuPort.setTitle(QCoreApplication.translate("MainWindow", u"Port", None))
    # retranslateUi

