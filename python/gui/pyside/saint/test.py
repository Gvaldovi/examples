j1979_PID = {
    "Engine speed": {"service": 1, "pid": 12, "size": 2, "min": 0, "max": 16383.75, "step": 0.25, "units": "rpm", "value": 10, "active": True},
    "Vehicle speed": {"service": 1, "pid": 13, "size": 1, "min": 0, "max": 255, "step": 1, "units": "km/h", "value": 0, "active": True},
    "Throttle position": {"service": 1, "pid": 17, "size": 1, "min": 0, "max": 100, "step": 1, "units": "%", "value": 0, "active": False},
    "VIN": {"service": 9, "pid": 2, "size": 17, "min": 0, "max": 0, "step": 0, "units": 0,
        "value": '49 02 2D 31 46 4D 43 55 30 44 47 30 43 4B 31 33 35 36 31 38', "active": False},
}
cmd = bytes.fromhex('51 07 DF 02 09 02')
rsp = b''

# keys = list(j1979_PID.keys())
# for key in keys:
#     if cmd[0] == 0x51 and cmd[3] == 2:
#         service = cmd[4]
#         pid = cmd[5]
#         if service == j1979_PID[key]["service"] and pid == j1979_PID[key]["pid"]:
#             if key == "Engine speed":
#                 rsp = bytes.fromhex('53 07 E8 04 41 0C')
#                 val = j1979_PID[pid]["value"]
#                 val = int(val * 4)
#                 rsp = rsp + val.to_bytes(2, 'big') + bytes.fromhex('00 00 00')
#             elif pid == "Vehicle speed":
#                 rsp = bytes.fromhex('53 07 E8 04 41 0D')
#                 val = j1979_PID[pid]["value"]
#                 rsp = rsp + bytes([val]) + bytes.fromhex('00 00 00 00')

if cmd[0] == 0x51 and cmd[3] == 2:
    service = cmd[4]
    pid = cmd[5]
    if service == j1979_PID["Engine speed"]["service"] and pid == j1979_PID["Engine speed"]["pid"]:
        rsp = bytes.fromhex('53 07 E8 04 41 0C')
        val = j1979_PID["Engine speed"]["value"]
        val = int(val * 4)
        rsp = rsp + val.to_bytes(2, 'big') + bytes.fromhex('00 00 00')
    elif service == j1979_PID["Vehicle speed"]["service"] and pid == j1979_PID["Vehicle speed"]["pid"]:
        rsp = bytes.fromhex('53 07 E8 04 41 0D')
        val = j1979_PID["Vehicle speed"]["value"]
        rsp = rsp + bytes([val]) + bytes.fromhex('00 00 00 00')
    elif service == j1979_PID["VIN"]["service"] and pid == j1979_PID["VIN"]["pid"]:
        rsp = bytes.fromhex('53 07 E8 10')
        vin = bytes.fromhex(j1979_PID["VIN"]["value"])
        vin_size = len(vin)
        rsp = rsp + bytes([vin_size]) + vin

# if cmd[0] == 0x51:
#     if cmd[3] == 2:
#         service = cmd[4]
#         pid = cmd[5]
#         print(service,pid)
#         # if service == bytes.fromhex('51 07 DF 02 01 0C'):
#         if service == j1979_PID["Engine speed"]["service"] and pid == j1979_PID["Engine speed"]["pid"]:
#             rsp = bytes.fromhex('53 07 E8 04 41 0C')
#             val = j1979_PID["Engine speed"]["value"]
#             val = int(val * 4)
#             rsp = rsp + val.to_bytes(2, 'big') + bytes.fromhex('00 00 00')

#             # rsp = bytes.fromhex('53 07 E8 04 41 0C')
#             # a = 20
#             # b = 255
#             # v = ((256*a)+b)/4
#             # print(v, type(v))

#             # val = int(v * 4)
#             # A = (val >> 8) & 0xFF  # High byte
#             # B = val & 0xFF         # Low byte
#             # ans = bytes([A, B])
#             # ans1 = val.to_bytes(2, 'big')
#             # print(ans, ans1)
#             # rsp = rsp + val.to_bytes(2, 'big') + bytes.fromhex('00 00 00')
#             # rsp = rsp + ans + bytes.fromhex('00 00 00')
#         elif service == j1979_PID["Vehicle speed"]["service"] and pid == j1979_PID["Vehicle speed"]["pid"]:
#             rsp = bytes.fromhex('53 07 E8 04 41 0D')
#             val = j1979_PID["Vehicle speed"]["value"]
#             rsp = rsp + bytes([val]) + bytes.fromhex('00 00 00 00')
#             # rsp = bytes.fromhex('53 07 E8 03 41 0D 0B 00 00 00 00')

if rsp:
# 0010 50 07 E8 10 14 49 02 2D 31 46 4D
# 0170 50 07 E8 21 43 55 30 44 47 30 43
# 0010 50 07 E8 22 4B 31 33 35 36 31 38
    if rsp[3] == 0x10:
        print(rsp[:11].hex(' '))
        # Send multi frame
        size = len(rsp)
        messages = int((rsp[4] - 6) / 7)
        if int((rsp[4] - 6) % 7):
            messages = messages + 1
        idx = 11
        count = 0x21
        for m in range(messages):
            if (size - idx) >= 7:
                f = rsp[:3] + bytes([count+m]) + rsp[idx:idx+7]
                idx = idx + 7
            else:
                rest = size - idx
                f = rsp[:3] + bytes([count+m]) + rsp[idx:] + bytes(7-rest)
                idx = idx + rest
            print(f.hex(' '))
    else:
        print(rsp.hex(' '))
else:
    print("bad rsp")