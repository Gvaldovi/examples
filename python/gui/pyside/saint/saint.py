import sys
import serial
import time
import math
import binascii
from PySide6.QtCore import Qt, QThread, Signal, QByteArray
from PySide6.QtWidgets import QApplication, QWidget, QVBoxLayout, QTextEdit, QPushButton, QComboBox, QLabel, QMainWindow, QTableWidgetItem
from ui_mainwindow import Ui_MainWindow
try:
    import Queue
except:
    import queue as Queue

# Saint commands
# CONFIGURATION
SAINT_ENTER = b'\xFF\x00'
SAINT_RESET = b'\x08\x80'
SAINT_CAN1_250 = b'\x54\x01\xCE\x3E'  # Set Baud to 250,000, sample point = 80%
SAINT_CAN1_500 = b'\x54\x01\xC9\x39'  # Set Baud to 500,000, sample point = 73.3%
SAINT_CAN1_HS = b'\x54\x04\x00'      # Set Protocol to Dual Wire, High Speed CAN (CAN_1_H, CAN_1_L)
SAINT_CAN1_RX_TX = b'\x54\x03\x00'   # Set CAN to listen and transmit mode (power on default)

SAINT_CAN2_250 = b'\x5C\x01\xCE\x3E'  # Set Baud to 250,000, sample point = 80%
SAINT_CAN2_500 = b'\x5C\x01\xC9\x39'  # Set Baud to 500,000, sample point = 73.3%
SAINT_CAN2_HS = b'\x5C\x04\x00'      # Set Protocol to Dual Wire, High Speed CAN (CAN_2_H, CAN_2_L)
SAINT_CAN2_RX_TX = b'\x5C\x03\x00'   # Set CAN to listen and transmit mode (power on default)
# COMMANDS
SAINT_CMD_CAN = b'\x50\x98'         # Header of every CAN command
# J1979 PIDs
j1979_PID = {
    "Engine speed": {"service": 1, "pid": 12, "size": 2, "min": 0, "max": 16383.75, "step": 0.1, "units": "rpm", "value": 0, "active": True},
    "Vehicle speed": {"service": 1, "pid": 13, "size": 1, "min": 0, "max": 255, "step": 1, "units": "km/h", "value": 0, "active": True},
    "Throttle position": {"service": 1, "pid": 17, "size": 1, "min": 0, "max": 100, "step": 1, "units": "%", "value": 0, "active": False},
    "VIN": {"service": 9, "pid": 2, "size": 17, "min": 0, "max": 0, "step": 0, "units": 0,
            "value": '49 02 2D 31 46 4D 43 55 30 44 47 30 43 4B 31 33 35 36 31 38', "active": False},
}

class SerialReaderThread(QThread):
    # Define a signal to send data from the thread to the main UI
    data_received = Signal(QByteArray)

    def __init__(self, port, baudrate):
        super().__init__()
        self.port = port
        self.baudrate = baudrate
        self.serial_connection = None
        self.running = False
        self.txq = Queue.Queue()

    def serial_out(self, s):
        self.txq.put(s) 

    def run(self):
        """Start reading from the serial port."""
        # serial_port = QSerialPort() # TODO: test the pyside QSerialPort
        self.serial_connection = serial.Serial(self.port, self.baudrate, timeout=0.1)
        self.running = True
        while self.running:
            if self.serial_connection.in_waiting > 0:
                # data = self.serial_connection.readline().decode('utf-8', errors='ignore').strip()
                # if data:
                #     self.data_received.emit(data)
                data = self.serial_connection.readline()
                byte_array = QByteArray(data)
                self.data_received.emit(byte_array)
            if not self.txq.empty():
                txd = self.txq.get()                    # If Tx data in queue, write to serial port
                self.serial_connection.write(txd)

    def stop(self):
        """Stop the serial reader."""
        self.running = False
        if self.serial_connection:
            self.serial_connection.close()

class MyWindow(QMainWindow, Ui_MainWindow):
    text_update = Signal(str)
    def __init__(self):
        super(MyWindow, self).__init__()
        self.setupUi(self)

        ### Assign Widgets for configuration
        self.actionExit.triggered.connect(self.exit)
        self.actionConnect.triggered.connect(self.serial_toggle)
        self.actionttyUSB0.triggered.connect(self.configuration)
        self.actionttyUSB1.triggered.connect(self.configuration)
        self.action115200.triggered.connect(self.configuration)
        self.action57600.triggered.connect(self.configuration)
        self.action9600.triggered.connect(self.configuration)
        ### Assign Widgets for Log
        self.pushButton_logClear.clicked.connect(self.clear_log)
        ### Assign Widgets for J1979
        self.pushButton_j1979_Reset.clicked.connect(self.j1979_reset)
        self.pushButton_j1979_Start.clicked.connect(self.j1979_start)
        # Place holder for the serial thread
        self.serial_thread = None
        self.textEdit.setReadOnly(True)

        # Insert item to table
        keys = list(j1979_PID.keys())
        for k in keys:
            ser = hex(j1979_PID[k]["service"])
            # pid = j1979_PID[k]["pid"].hex().upper()
            pid = hex(j1979_PID[k]["pid"])
            val = str(j1979_PID[k]["value"])
            uni = j1979_PID[k]["units"]
            act = j1979_PID[k]["active"]
            self.set_row([k, ser, pid, val, uni, act])
            self.comboBox_j1979_var1.addItem(k)
            self.comboBox_j1979_var2.addItem(k)
            self.comboBox_j1979_var3.addItem(k)

        # Select the default PID on selectors
        self.comboBox_j1979_var1.setCurrentText("Engine speed")
        self.comboBox_j1979_var2.setCurrentText("Vehicle speed")
        self.comboBox_j1979_var3.setCurrentText("Throttle position")
        # Connect the slider to the spin box and the spin box to the slider
        self.horizontalSlider_j1979_var1.valueChanged.connect(self.update_j1979_var1_spin_box)
        self.doubleSpinBox_j1979_var1.valueChanged.connect(self.update_j1979_var1_slider)
        self.horizontalSlider_j1979_var2.valueChanged.connect(self.update_j1979_var2_spin_box)
        self.doubleSpinBox_j1979_var2.valueChanged.connect(self.update_j1979_var2_slider)
        self.horizontalSlider_j1979_var3.valueChanged.connect(self.update_j1979_var3_spin_box)
        self.doubleSpinBox_j1979_var3.valueChanged.connect(self.update_j1979_var3_slider)
        pid = "Engine speed"
        self.doubleSpinBox_j1979_var1.setSingleStep(j1979_PID[pid]["step"])
        self.doubleSpinBox_j1979_var1.setRange(j1979_PID[pid]["min"], j1979_PID[pid]["max"])
        self.horizontalSlider_j1979_var1.setRange(j1979_PID[pid]["min"], j1979_PID[pid]["max"])
        self.update_j1979_var1_spin_box(j1979_PID[pid]["value"])
        self.update_j1979_var1_slider(j1979_PID[pid]["value"])
        pid = "Vehicle speed"
        self.doubleSpinBox_j1979_var2.setSingleStep(j1979_PID[pid]["step"])
        self.doubleSpinBox_j1979_var2.setRange(j1979_PID[pid]["min"], j1979_PID[pid]["max"])
        self.horizontalSlider_j1979_var2.setRange(j1979_PID[pid]["min"], j1979_PID[pid]["max"])
        self.update_j1979_var2_spin_box(j1979_PID[pid]["value"])
        self.update_j1979_var2_slider(j1979_PID[pid]["value"])
        pid = "Throttle position"
        self.doubleSpinBox_j1979_var3.setSingleStep(j1979_PID[pid]["step"])
        self.doubleSpinBox_j1979_var3.setRange(j1979_PID[pid]["min"], j1979_PID[pid]["max"])
        self.horizontalSlider_j1979_var3.setRange(j1979_PID[pid]["min"], j1979_PID[pid]["max"])
        self.update_j1979_var3_spin_box(j1979_PID[pid]["value"])
        self.update_j1979_var3_slider(j1979_PID[pid]["value"])

    def update_j1979_var1_spin_box(self, value):
        """Convert slider value (0-100) to spin box value (0.0-1.0)."""
        self.doubleSpinBox_j1979_var1.setValue(value)

    def update_j1979_var1_slider(self, value):
        """Convert spin box value (0.0-1.0) to slider value (0-100)."""
        self.horizontalSlider_j1979_var1.setValue(math.floor(value))
        # Update table
        pid = self.comboBox_j1979_var1.currentText()
        self.update_table(pid, value)

    def update_j1979_var2_spin_box(self, value):
        """Convert slider value (0-100) to spin box value (0.0-1.0)."""
        self.doubleSpinBox_j1979_var2.setValue(value)

    def update_j1979_var2_slider(self, value):
        """Convert spin box value (0.0-1.0) to slider value (0-100)."""
        self.horizontalSlider_j1979_var2.setValue(math.floor(value))
        # Update table
        pid = self.comboBox_j1979_var2.currentText()
        self.update_table(pid, value)

    def update_j1979_var3_spin_box(self, value):
        """Convert slider value (0-100) to spin box value (0.0-1.0)."""
        self.doubleSpinBox_j1979_var3.setValue(value)

    def update_j1979_var3_slider(self, value):
        """Convert spin box value (0.0-1.0) to slider value (0-100)."""
        self.horizontalSlider_j1979_var3.setValue(math.floor(value))
        # Update table
        pid = self.comboBox_j1979_var3.currentText()
        self.update_table(pid, value)

    def set_row(self, data):
        row_position = self.table_j1979.rowCount()
        self.table_j1979.insertRow(row_position)
        for col_index, value in enumerate(data):
            if type(value) == bool:
                item = QTableWidgetItem()
                item.setFlags(Qt.ItemIsUserCheckable | Qt.ItemIsEnabled)  # Make the item checkable
                if value == True:
                    item.setCheckState(Qt.Checked)  # Set the initial state to unchecked
                else:
                    item.setCheckState(Qt.Unchecked)  # Set the initial state to unchecked
            else:
                item = QTableWidgetItem(value)
            self.table_j1979.setItem(row_position, col_index, item)
            
    def update_table(self, pid, value):
        idx = list(j1979_PID.keys()).index(pid)
        self.table_j1979.item(idx, 3).setText(str(value))
        j1979_PID[pid]["value"] = value

    ##################
    # Configurations #
    ##################
    def exit(self):
        if self.serial_thread and self.serial_thread.isRunning():
            self.serial_thread.running = False           # Wait until serial thread terminates
            self.serial_thread.wait()
        sys.exit(0)

    def configuration(self):
        # Check the option and disable others
        action = self.sender()
        if action.text() == "ttyUSB0":
            self.actionttyUSB1.setChecked(False)
        elif action.text() == "ttyUSB1":
            self.actionttyUSB0.setChecked(False)
        elif action.text() == "115200":
            self.action57600.setChecked(False)
            self.action9600.setChecked(False)
        elif action.text() == "57600":
            self.action115200.setChecked(False)
            self.action9600.setChecked(False)
        elif action.text() == "9600":
            self.action57600.setChecked(False)
            self.action9600.setChecked(False)

    def serial_toggle(self, event):
        """Start or stop serial communication."""
        if self.serial_thread and self.serial_thread.isRunning():
            self.serial_thread.stop()
            self.serial_thread.wait()
            self.actionConnect.setText("Connect")
            # Disable protocols
            self.tab_J1979.setEnabled(False)
            self.tab_J1939.setEnabled(False)
        else:
            # TODO: create something more automatic
            port = ""
            baudrate = 0
            if self.actionttyUSB0.isChecked():
                port = "/dev/ttyUSB0"
            elif self.actionttyUSB1.isChecked():
                port = "/dev/ttyUSB1"

            if self.action115200.isChecked():
                baudrate = 115200
            elif self.action57600.isChecked():
                baudrate = 57600
            elif self.action9600.isChecked():
                baudrate = 9600

            # FIXME: remove this harcoded port
            port = "/dev/ttyUSB1"
            baudrate = 57600

            self.serial_thread = SerialReaderThread(port, baudrate)
            self.serial_thread.data_received.connect(self.process_data)
            self.serial_thread.start()
            self.actionConnect.setText("Disconnect")
            # Enable protocols
            self.tab_J1979.setEnabled(True)
            self.tab_J1939.setEnabled(True)

    #######
    # Log #
    #######
    def clear_log(self):
        self.textEdit.clear()

    def display_string(self, data):
        self.textEdit.append(data)

    def display_string_hex_out(self, data):
        hex_string = data.hex()
        output_hex = ' '.join(hex_string[i:i+2] for i in range(0, len(hex_string), 2))
        self.textEdit.append("> " + output_hex)

    def display_string_hex_in(self, data):
        hex_string = data.hex()
        output_hex = ' '.join(hex_string[i:i+2] for i in range(0, len(hex_string), 2))
        self.textEdit.append("< " + output_hex)

    def process_data(self, data):
        """ Process data comming from saint """
        array = data.data()
        self.display_string_hex_in(array)

        # Check command
        cmd = array[:6]
        rsp = b''
        if cmd[0] == 0x51 and cmd[3] == 2:
            service = cmd[4]
            pid = cmd[5]
            if cmd == bytes.fromhex('51 07 DF 02 01 00'):
                rsp = bytes.fromhex('53 07 E8 06 41 00 98 19 80 01 00')
            elif cmd == bytes.fromhex('51 07 DF 02 01 20'):
                rsp = bytes.fromhex('53 07 E8 06 41 20 91 02 80 01 00')
            elif cmd == bytes.fromhex('51 07 DF 02 01 40'):
                rsp = bytes.fromhex('53 07 E8 06 41 40 00 0C 90 15 00')
            elif cmd == bytes.fromhex('51 07 DF 02 01 60'):
                rsp = bytes.fromhex('53 07 E8 06 41 60 00 00 00 03 00')
            elif cmd == bytes.fromhex('51 07 DF 02 01 80'):
                rsp = bytes.fromhex('53 07 E8 06 41 80 00 00 00 20 00')
            elif cmd == bytes.fromhex('51 07 DF 02 01 A0'):
                rsp = bytes.fromhex('53 07 E8 06 41 A0 04 00 00 00 00')
            elif cmd == bytes.fromhex('51 07 DF 02 01 01'):
                rsp = bytes.fromhex('53 07 E8 06 41 01 80 07 ED 6F 00')
            elif cmd == bytes.fromhex('51 07 DF 02 01 04'):
                rsp = bytes.fromhex('53 07 E8 03 41 04 00 00 00 00 00')
            elif cmd == bytes.fromhex('51 07 DF 02 01 05'):
                rsp = bytes.fromhex('53 07 E8 03 41 05 00 00 00 00 00')
            elif service == j1979_PID["Engine speed"]["service"] and pid == j1979_PID["Engine speed"]["pid"]:
                rsp = bytes.fromhex('53 07 E8 04 41 0C')
                val = j1979_PID["Engine speed"]["value"]
                val = int(val * 4)
                rsp = rsp + val.to_bytes(2, 'big') + bytes.fromhex('00 00 00')
            elif service == j1979_PID["Vehicle speed"]["service"] and pid == j1979_PID["Vehicle speed"]["pid"]:
                rsp = bytes.fromhex('53 07 E8 04 41 0D')
                val = int(j1979_PID["Vehicle speed"]["value"])
                rsp = rsp + bytes([val]) + bytes.fromhex('00 00 00 00')
            elif cmd == bytes.fromhex('51 07 DF 02 01 10'):
                rsp = bytes.fromhex('53 07 E8 04 41 10 00 00 00 00 00')
            elif service == j1979_PID["Throttle position"]["service"] and pid == j1979_PID["Throttle position"]["pid"]:
                rsp = bytes.fromhex('53 07 E8 04 41 11')
                val = int(j1979_PID["Throttle position"]["value"])
                rsp = rsp + bytes([val]) + bytes.fromhex('00 00 00 00')
            elif cmd == bytes.fromhex('51 07 DF 02 01 21'):
                rsp = bytes.fromhex('53 07 E8 04 41 21 00 00 00 00 00')
            elif cmd == bytes.fromhex('51 07 DF 02 01 2F'):
                rsp = bytes.fromhex('53 07 E8 03 41 2F 00 00 00 00 00')
            elif cmd == bytes.fromhex('51 07 DF 02 01 31'):
                rsp = bytes.fromhex('53 07 E8 04 41 31 00 00 00 00 00')
            elif cmd == bytes.fromhex('51 07 DF 02 01 4D'):
                rsp = bytes.fromhex('53 07 E8 04 41 4D 00 00 00 00 00')
            elif cmd == bytes.fromhex('51 07 DF 02 01 4E'):
                rsp = bytes.fromhex('53 07 E8 04 41 4E 00 00 00 00 00')
            elif cmd == bytes.fromhex('51 07 DF 02 01 51'):
                rsp = bytes.fromhex('53 07 E8 03 41 51 00 00 00 00 00')
            elif cmd == bytes.fromhex('51 07 DF 02 01 5C'):
                rsp = bytes.fromhex('53 07 E8 03 41 5C 00 00 00 00 00')
            elif cmd == bytes.fromhex('51 07 DF 02 01 5E'):
                rsp = bytes.fromhex('53 07 E8 04 41 5E 00 00 00 00 00')
            elif cmd == bytes.fromhex('51 07 DF 02 01 9B'):
                rsp = bytes.fromhex('53 07 E8 06 41 9B 00 00 00 00 00')
            elif cmd == bytes.fromhex('51 07 DF 02 01 A6'):
                rsp = bytes.fromhex('53 07 E8 06 41 A6 11 22 34 50 00')
            elif service == j1979_PID["VIN"]["service"] and pid == j1979_PID["VIN"]["pid"]:
                rsp = bytes.fromhex('53 07 E8 10')
                vin = bytes.fromhex(j1979_PID["VIN"]["value"])
                vin_size = len(vin)
                rsp = rsp + bytes([vin_size]) + vin

        if rsp:
            self.saint_out(rsp)
        
    ###################
    # Saint functions #
    ###################
    def saint_out(self, output):
        if len(output) > 8 and output[3] == 0x10:
            # EXAMPLE
            # 0010 50 07 E8 10 14 49 02 2D 31 46 4D
            # 0170 50 07 E8 21 43 55 30 44 47 30 43
            # 0010 50 07 E8 22 4B 31 33 35 36 31 38
            f = output[:11] + SAINT_ENTER
            self.display_string_hex_out(f)
            self.serial_thread.serial_out(f)
            time.sleep(0.2)
            
            # Send multi frame
            size = len(output)
            messages = int((output[4] - 6) / 7)
            if int((output[4] - 6) % 7):
                messages = messages + 1
            idx = 11
            count = 0x21
            for m in range(messages):
                if (size - idx) >= 7:
                    f = output[:3] + bytes([count+m]) + output[idx:idx+7]
                    idx = idx + 7
                else:
                    r = size - idx
                    f = output[:3] + bytes([count+m]) + output[idx:] + bytes(7-r)
                    idx = idx + r
                f = f + SAINT_ENTER
                self.display_string_hex_out(f)
                self.serial_thread.serial_out(f)
                time.sleep(0.2)
        else:
            output = output + SAINT_ENTER
            self.display_string_hex_out(output)
            self.serial_thread.serial_out(output)

    def j1979_reset(self):
        self.saint_out(SAINT_RESET)

    def j1979_start(self):
        # TODO: harcoded to CAN250 11bits for now
        self.saint_out(SAINT_CAN1_250)
        time.sleep(0.5)
        self.saint_out(SAINT_CAN1_HS)
        time.sleep(0.5)
        self.saint_out(SAINT_CAN1_RX_TX)

    def j1939(self):
        sel = self.comboBox_j1939Var1.currentText()
        if sel != "Select Variable":
            parameters = self.j1939Vars.get(sel)
            self.lineEdit_saint.setText(str(parameters[0]))

            output = SAINT_CMD_CAN
            output += (parameters[0]).to_bytes(2, byteorder = 'big')
            output += SAINT_ENTER
            self.serSaint.ser_out(output)


if __name__ == "__main__":
    app = QApplication(sys.argv)
    window = MyWindow()
    window.show()
    app.exec()