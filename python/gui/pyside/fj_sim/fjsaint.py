import sys
from PySide6 import QtWidgets, QtCore, QtGui

from MainWindow import Ui_MainWindow

try:
    import Queue
except:
    import queue as Queue
import sys, time, serial
 
WIN_WIDTH, WIN_HEIGHT = 684, 400    # Window size
SER_TIMEOUT = 0.1                   # Timeout for serial Rx
RETURN_CHAR = "\n"                  # Char to be sent when Enter key pressed
PASTE_CHAR  = "\x16"                # Ctrl code for clipboard paste
baudrate    = 115200                # Default baud rate
portnameFJ  = ""                    # Default port name for FJ
portnameSaint  = ""                 # Default port name for Saint
hexmode     = False                 # Flag to enable hex display

# Saint commands
SAINT_ENTER = b'\xFF\x00\x0D'
SAINT_RESET = b'\x08\x80'
SAINT_CAN250 = b'\x54\x01\xCE\x3E'  # Set Baud to 250,000, sample point = 80%
SAINT_CAN500 = b'\x54\x01\xC9\x39'  # Set Baud to 500,000, sample point = 73.3%
SAINT_CAN_HS = b'\x54\x04\x00'      # Set Protocol to Dual Wire, High Speed CAN (CAN_1_H, CAN_1_L)
SAINT_CAN_RX_TX = b'\x54\x03\x00'   # Set CAN to listen and transmit mode (power on default)
SAINT_CMD_CAN = b'\x50\x98'         # Header of every CAN command

# Convert a string to bytes
def str_bytes(s):
    return s.encode('latin-1')
     
# Convert bytes to string
def bytes_str(d):
    return d if type(d) is str else "".join([chr(b) for b in d])
     
# Return hexadecimal values of data
def hexdump(data):
    return " ".join(["%02X" % ord(b) for b in data])
 
# Return a string with high-bit chars replaced by hex values
def textdump(data):
    return "".join(["[%02X]" % ord(b) if b>'\x7e' else b for b in data])
     
# Display incoming serial data
def display(s):
    if not hexmode:
        sys.stdout.write(textdump(str(s)))
    else:
        sys.stdout.write(hexdump(s) + ' ')


class MainWindow(QtWidgets.QMainWindow, Ui_MainWindow):
    text_update = QtCore.Signal(str)
    def __init__(self):
        super(MainWindow, self).__init__()
        self.setupUi(self)
        self.assignWidgets()

        self.text_update.connect(self.append_text)      # Connect text update to handler
        sys.stdout = self                               # Redirect sys.stdout to self
        if portnameFJ != "":
            self.serFJ = SerialThreadFJ(portnameFJ, baudrate)   # Start serial thread
            self.serFJ.start()
        if portnameSaint != "":
            self.serSaint = SerialThreadSaint(portnameSaint, baudrate, self.saintResponse)   # Start serial thread
            self.serSaint.start()
            self.saintInit()
        # Initialize J1939 variables. Every variable has a list of: PGN, min, max, step, position, lenght, default value
        self.j1939Vars = {  "RPM":[61444,0,8031.875,0.125,4,2,0],
                            "Temperature":[65262,-273,1734.96875,0.03125,3,2,0]} #"Engine Oil Temp"

    def assignWidgets(self):
        # Send button
        self.pushButton_3.clicked.connect(self.send)
        self.pushButton_3.setAutoDefault(True)
        # Send edit
        self.lineEdit_2.returnPressed.connect(self.pushButton_3.click)
        self.lineEdit_2.installEventFilter(self)
        # J1939
        self.comboBox_j1939Var1.activated.connect(self.j1939)

    def send(self):
        text = self.lineEdit_2.text() + "\n"
        self.lineEdit_2.clear()
        self.serFJ.ser_out(text)

    def eventFilter(self, obj, event):
        if obj is self.lineEdit_2 and event.type() == QtCore.QEvent.KeyPress:
            if event.key() == QtCore.Qt.Key_Up:
                self.commandDB(True)
            elif event.key() == QtCore.Qt.Key_Down:
                self.commandDB(False)
        return super().eventFilter(obj, event)

    def commandDB(self, option):
        if option is True:
            self.serFJ.ser_out("up pressed")
        else:
            self.serFJ.ser_out("down pressed")

    ###################
    # Saint functions #
    ###################
    def saintInit(self):
        # Reset Saint
        output = SAINT_RESET + SAINT_ENTER
        self.serSaint.ser_out(output)
        output = SAINT_CAN500 + SAINT_ENTER
        self.serSaint.ser_out(output)
        output = SAINT_CAN_HS + SAINT_ENTER
        self.serSaint.ser_out(output)
        output = SAINT_CAN_RX_TX + SAINT_ENTER
        self.serSaint.ser_out(output)

    def j1939(self):
        sel = self.comboBox_j1939Var1.currentText()
        if sel != "Select Variable":
            parameters = self.j1939Vars.get(sel)
            self.lineEdit_saint.setText(str(parameters[0]))

            output = SAINT_CMD_CAN
            output += (parameters[0]).to_bytes(2, byteorder = 'big')
            output += SAINT_ENTER
            self.serSaint.ser_out(output)


    def saintResponse(self, s):
        print(s)
        self.lineEdit_saint.setText(str(s)) #print on saint line edit

    def write(self, text):                      # Handle sys.stdout.write: update display
        self.text_update.emit(text)             # Send signal to synchronise call with main thread
         
    def flush(self):                            # Handle sys.stdout.flush: do nothing
        pass
 
    def append_text(self, text):                # Text display update handler
        cur = self.textEdit.textCursor()
        cur.movePosition(QtGui.QTextCursor.End) # Move cursor to end of text
        s = str(text)
        while s:
            head,sep,s = s.partition("\n")      # Split line at LF
            cur.insertText(head)                # Insert text at cursor
            # if sep:                             # New line if LF
            #     cur.insertBlock()
        self.textEdit.setTextCursor(cur)         # Update visible cursor
     
    def closeEvent(self, event):                # Window closing
        if portnameFJ != "":
            self.serFJ.running = False              # Wait until serial thread terminates
            self.serFJ.wait()
        if portnameSaint != "":
            self.serSaint.running = False           # Wait until serial thread terminates
            self.serSaint.wait()



# Thread to handle incoming & outgoing serial data
class SerialThreadFJ(QtCore.QThread):
    def __init__(self, portname, baudrate): # Initialise with serial port details
        QtCore.QThread.__init__(self)
        self.portname, self.baudrate = portname, baudrate
        self.txq = Queue.Queue()
        self.running = True
 
    def ser_out(self, s):                   # Write outgoing data to serial port if open
        self.txq.put(s)                     # ..using a queue to sync with reader thread
         
    def ser_in(self, s):                    # Write incoming serial data to screen
        display(s)
         
    def run(self):                          # Run serial reader thread
        print("Opening %s at %u baud %s" % (self.portname, self.baudrate,
              "(hex display)" if hexmode else ""))
        try:
            self.ser = serial.Serial(self.portname, self.baudrate, timeout=SER_TIMEOUT)
            time.sleep(SER_TIMEOUT*1.2)
            self.ser.flushInput()
        except:
            self.ser = None
        if not self.ser:
            print("Can't open port")
            self.running = False
        while self.running:
            s = self.ser.read(self.ser.in_waiting or 1)
            if s:                                       # Get data from serial port
                self.ser_in(bytes_str(s))               # ..and convert to string
            if not self.txq.empty():
                txd = str(self.txq.get())               # If Tx data in queue, write to serial port
                self.ser.write(str_bytes(txd))
        if self.ser:                                    # Close serial port when thread finished
            self.ser.close()
            self.ser = None

# Thread to handle incoming & outgoing serial data
class SerialThreadSaint(QtCore.QThread):
    def __init__(self, portname, baudrate,display): # Initialise with serial port details
        QtCore.QThread.__init__(self)
        self.portname, self.baudrate = portname, baudrate
        self.txq = Queue.Queue()
        self.running = True
        self.display = display
 
    def ser_out(self, s):                   # Write outgoing data to serial port if open
        self.txq.put(s)                     # ..using a queue to sync with reader thread
         
    def ser_in(self, s):                    # Write incoming serial data to screen
        self.display(s)
         
    def run(self):                          # Run serial reader thread
        print("Opening %s at %u baud %s" % (self.portname, self.baudrate,
              "(hex display)" if hexmode else ""))
        try:
            self.ser = serial.Serial(self.portname, self.baudrate, timeout=SER_TIMEOUT)
            time.sleep(SER_TIMEOUT*1.2)
            self.ser.flushInput()
        except:
            self.ser = None
        if not self.ser:
            print("Can't open port")
            self.running = False
        while self.running:
            s = self.ser.read(self.ser.in_waiting or 1)
            if s:                                       # Get data from serial port
                self.ser_in(s)               # ..and convert to string
            if not self.txq.empty():
                txd = self.txq.get()                    # If Tx data in queue, write to serial port
                self.ser.write(txd)
        if self.ser:                                    # Close serial port when thread finished
            self.ser.close()
            self.ser = None


if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    opt = err = None
    for arg in sys.argv[1:]:                # Process command-line options
        if len(arg)==2 and arg[0]=="-":
            opt = arg.lower()
            if opt == '-x':                 # -X: display incoming data in hex
                hexmode = True
                opt = None
        else:
            if opt == '-s':                 # -s serial port name for Saint2, e.g. 'COM1' or '/dev/ttyUSB0'
                portnameSaint = arg
            elif opt == '-f':               # -f serial port name for FJ unit, e.g. 'COM1' or '/dev/ttyUSB1'
                portnameFJ = arg
    if err:
        print(err)
        sys.exit(1)

    window = MainWindow()
    window.show()
    app.exec()