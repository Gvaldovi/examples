# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'mainwindow.ui'
##
## Created by: Qt User Interface Compiler version 6.2.1
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import (QCoreApplication, QDate, QDateTime, QLocale,
    QMetaObject, QObject, QPoint, QRect,
    QSize, QTime, QUrl, Qt)
from PySide6.QtGui import (QBrush, QColor, QConicalGradient, QCursor,
    QFont, QFontDatabase, QGradient, QIcon,
    QImage, QKeySequence, QLinearGradient, QPainter,
    QPalette, QPixmap, QRadialGradient, QTransform)
from PySide6.QtWidgets import (QAbstractSpinBox, QApplication, QCheckBox, QComboBox,
    QDoubleSpinBox, QHBoxLayout, QLabel, QLineEdit,
    QMainWindow, QPushButton, QSizePolicy, QSlider,
    QSpacerItem, QTabWidget, QTextEdit, QVBoxLayout,
    QWidget)

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        if not MainWindow.objectName():
            MainWindow.setObjectName(u"MainWindow")
        MainWindow.resize(500, 500)
        MainWindow.setTabletTracking(True)
        MainWindow.setDocumentMode(True)
        MainWindow.setTabShape(QTabWidget.Rounded)
        self.centralwidget = QWidget(MainWindow)
        self.centralwidget.setObjectName(u"centralwidget")
        self.centralwidget.setMinimumSize(QSize(500, 500))
        self.verticalLayout_5 = QVBoxLayout(self.centralwidget)
        self.verticalLayout_5.setObjectName(u"verticalLayout_5")
        self.horizontalLayout_9 = QHBoxLayout()
        self.horizontalLayout_9.setObjectName(u"horizontalLayout_9")
        self.checkBox_4 = QCheckBox(self.centralwidget)
        self.checkBox_4.setObjectName(u"checkBox_4")

        self.horizontalLayout_9.addWidget(self.checkBox_4)

        self.horizontalSpacer_2 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout_9.addItem(self.horizontalSpacer_2)


        self.verticalLayout_5.addLayout(self.horizontalLayout_9)

        self.tabWidget = QTabWidget(self.centralwidget)
        self.tabWidget.setObjectName(u"tabWidget")
        self.tabWidget.setEnabled(True)
        sizePolicy = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.tabWidget.sizePolicy().hasHeightForWidth())
        self.tabWidget.setSizePolicy(sizePolicy)
        self.tab_CAN = QWidget()
        self.tab_CAN.setObjectName(u"tab_CAN")
        self.tab_CAN.setEnabled(True)
        self.verticalLayout_2 = QVBoxLayout(self.tab_CAN)
        self.verticalLayout_2.setObjectName(u"verticalLayout_2")
        self.horizontalLayout_14 = QHBoxLayout()
        self.horizontalLayout_14.setObjectName(u"horizontalLayout_14")
        self.comboBox_8 = QComboBox(self.tab_CAN)
        self.comboBox_8.addItem("")
        self.comboBox_8.addItem("")
        self.comboBox_8.addItem("")
        self.comboBox_8.setObjectName(u"comboBox_8")
        self.comboBox_8.setMaxVisibleItems(10)

        self.horizontalLayout_14.addWidget(self.comboBox_8)

        self.horizontalSlider_7 = QSlider(self.tab_CAN)
        self.horizontalSlider_7.setObjectName(u"horizontalSlider_7")
        self.horizontalSlider_7.setOrientation(Qt.Horizontal)

        self.horizontalLayout_14.addWidget(self.horizontalSlider_7)

        self.doubleSpinBox_8 = QDoubleSpinBox(self.tab_CAN)
        self.doubleSpinBox_8.setObjectName(u"doubleSpinBox_8")
        self.doubleSpinBox_8.setMinimumSize(QSize(100, 0))

        self.horizontalLayout_14.addWidget(self.doubleSpinBox_8)


        self.verticalLayout_2.addLayout(self.horizontalLayout_14)

        self.horizontalLayout_4 = QHBoxLayout()
        self.horizontalLayout_4.setObjectName(u"horizontalLayout_4")
        self.comboBox_3 = QComboBox(self.tab_CAN)
        self.comboBox_3.addItem("")
        self.comboBox_3.addItem("")
        self.comboBox_3.addItem("")
        self.comboBox_3.setObjectName(u"comboBox_3")
        self.comboBox_3.setMaxVisibleItems(10)

        self.horizontalLayout_4.addWidget(self.comboBox_3)

        self.horizontalSlider_2 = QSlider(self.tab_CAN)
        self.horizontalSlider_2.setObjectName(u"horizontalSlider_2")
        self.horizontalSlider_2.setOrientation(Qt.Horizontal)

        self.horizontalLayout_4.addWidget(self.horizontalSlider_2)

        self.doubleSpinBox_2 = QDoubleSpinBox(self.tab_CAN)
        self.doubleSpinBox_2.setObjectName(u"doubleSpinBox_2")
        self.doubleSpinBox_2.setMinimumSize(QSize(100, 0))

        self.horizontalLayout_4.addWidget(self.doubleSpinBox_2)


        self.verticalLayout_2.addLayout(self.horizontalLayout_4)

        self.horizontalLayout_5 = QHBoxLayout()
        self.horizontalLayout_5.setObjectName(u"horizontalLayout_5")
        self.comboBox_4 = QComboBox(self.tab_CAN)
        self.comboBox_4.addItem("")
        self.comboBox_4.addItem("")
        self.comboBox_4.addItem("")
        self.comboBox_4.setObjectName(u"comboBox_4")
        self.comboBox_4.setMaxVisibleItems(10)

        self.horizontalLayout_5.addWidget(self.comboBox_4)

        self.horizontalSlider_3 = QSlider(self.tab_CAN)
        self.horizontalSlider_3.setObjectName(u"horizontalSlider_3")
        self.horizontalSlider_3.setOrientation(Qt.Horizontal)

        self.horizontalLayout_5.addWidget(self.horizontalSlider_3)

        self.doubleSpinBox_3 = QDoubleSpinBox(self.tab_CAN)
        self.doubleSpinBox_3.setObjectName(u"doubleSpinBox_3")
        self.doubleSpinBox_3.setMinimumSize(QSize(100, 0))

        self.horizontalLayout_5.addWidget(self.doubleSpinBox_3)


        self.verticalLayout_2.addLayout(self.horizontalLayout_5)

        self.tabWidget.addTab(self.tab_CAN, "")
        self.tab_J1939 = QWidget()
        self.tab_J1939.setObjectName(u"tab_J1939")
        self.tab_J1939.setMinimumSize(QSize(335, 0))
        self.verticalLayout_3 = QVBoxLayout(self.tab_J1939)
        self.verticalLayout_3.setObjectName(u"verticalLayout_3")
        self.horizontalLayout_10 = QHBoxLayout()
        self.horizontalLayout_10.setObjectName(u"horizontalLayout_10")
        self.comboBox_j1939Var1 = QComboBox(self.tab_J1939)
        self.comboBox_j1939Var1.addItem("")
        self.comboBox_j1939Var1.addItem("")
        self.comboBox_j1939Var1.addItem("")
        self.comboBox_j1939Var1.setObjectName(u"comboBox_j1939Var1")
        self.comboBox_j1939Var1.setEditable(False)
        self.comboBox_j1939Var1.setMaxVisibleItems(10)

        self.horizontalLayout_10.addWidget(self.comboBox_j1939Var1)

        self.horizontalSlider_j1939Var1 = QSlider(self.tab_J1939)
        self.horizontalSlider_j1939Var1.setObjectName(u"horizontalSlider_j1939Var1")
        self.horizontalSlider_j1939Var1.setSingleStep(0)
        self.horizontalSlider_j1939Var1.setOrientation(Qt.Horizontal)

        self.horizontalLayout_10.addWidget(self.horizontalSlider_j1939Var1)

        self.doubleSpinBox_j1939Var1 = QDoubleSpinBox(self.tab_J1939)
        self.doubleSpinBox_j1939Var1.setObjectName(u"doubleSpinBox_j1939Var1")
        self.doubleSpinBox_j1939Var1.setEnabled(True)
        self.doubleSpinBox_j1939Var1.setMinimumSize(QSize(100, 0))
        self.doubleSpinBox_j1939Var1.setCorrectionMode(QAbstractSpinBox.CorrectToNearestValue)

        self.horizontalLayout_10.addWidget(self.doubleSpinBox_j1939Var1)


        self.verticalLayout_3.addLayout(self.horizontalLayout_10)

        self.verticalSpacer = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.verticalLayout_3.addItem(self.verticalSpacer)

        self.horizontalLayout_11 = QHBoxLayout()
        self.horizontalLayout_11.setObjectName(u"horizontalLayout_11")
        self.label_5 = QLabel(self.tab_J1939)
        self.label_5.setObjectName(u"label_5")

        self.horizontalLayout_11.addWidget(self.label_5)

        self.doubleSpinBox_j1939Period = QDoubleSpinBox(self.tab_J1939)
        self.doubleSpinBox_j1939Period.setObjectName(u"doubleSpinBox_j1939Period")
        self.doubleSpinBox_j1939Period.setMinimumSize(QSize(100, 0))
        self.doubleSpinBox_j1939Period.setAutoFillBackground(True)
        self.doubleSpinBox_j1939Period.setReadOnly(False)
        self.doubleSpinBox_j1939Period.setKeyboardTracking(False)
        self.doubleSpinBox_j1939Period.setDecimals(1)
        self.doubleSpinBox_j1939Period.setMinimum(0.200000000000000)
        self.doubleSpinBox_j1939Period.setSingleStep(0.200000000000000)
        self.doubleSpinBox_j1939Period.setValue(1.000000000000000)

        self.horizontalLayout_11.addWidget(self.doubleSpinBox_j1939Period)

        self.horizontalSpacer_4 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout_11.addItem(self.horizontalSpacer_4)


        self.verticalLayout_3.addLayout(self.horizontalLayout_11)

        self.tabWidget.addTab(self.tab_J1939, "")
        self.tab_ISO = QWidget()
        self.tab_ISO.setObjectName(u"tab_ISO")
        self.verticalLayout_4 = QVBoxLayout(self.tab_ISO)
        self.verticalLayout_4.setObjectName(u"verticalLayout_4")
        self.horizontalLayout = QHBoxLayout()
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.label_2 = QLabel(self.tab_ISO)
        self.label_2.setObjectName(u"label_2")

        self.horizontalLayout.addWidget(self.label_2)


        self.verticalLayout_4.addLayout(self.horizontalLayout)

        self.tabWidget.addTab(self.tab_ISO, "")

        self.verticalLayout_5.addWidget(self.tabWidget)

        self.horizontalLayout_8 = QHBoxLayout()
        self.horizontalLayout_8.setObjectName(u"horizontalLayout_8")
        self.label = QLabel(self.centralwidget)
        self.label.setObjectName(u"label")

        self.horizontalLayout_8.addWidget(self.label)

        self.lineEdit_saint = QLineEdit(self.centralwidget)
        self.lineEdit_saint.setObjectName(u"lineEdit_saint")
        self.lineEdit_saint.setEnabled(True)

        self.horizontalLayout_8.addWidget(self.lineEdit_saint)


        self.verticalLayout_5.addLayout(self.horizontalLayout_8)

        self.verticalLayout = QVBoxLayout()
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.horizontalLayout_7 = QHBoxLayout()
        self.horizontalLayout_7.setObjectName(u"horizontalLayout_7")
        self.pushButton_3 = QPushButton(self.centralwidget)
        self.pushButton_3.setObjectName(u"pushButton_3")

        self.horizontalLayout_7.addWidget(self.pushButton_3)

        self.lineEdit_2 = QLineEdit(self.centralwidget)
        self.lineEdit_2.setObjectName(u"lineEdit_2")
        self.lineEdit_2.setEnabled(True)

        self.horizontalLayout_7.addWidget(self.lineEdit_2)

        self.horizontalSpacer = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout_7.addItem(self.horizontalSpacer)

        self.label_4 = QLabel(self.centralwidget)
        self.label_4.setObjectName(u"label_4")

        self.horizontalLayout_7.addWidget(self.label_4)

        self.lineEdit = QLineEdit(self.centralwidget)
        self.lineEdit.setObjectName(u"lineEdit")
        self.lineEdit.setEnabled(True)

        self.horizontalLayout_7.addWidget(self.lineEdit)


        self.verticalLayout.addLayout(self.horizontalLayout_7)

        self.textEdit = QTextEdit(self.centralwidget)
        self.textEdit.setObjectName(u"textEdit")
        self.textEdit.setEnabled(True)
        self.textEdit.setAutoFillBackground(False)

        self.verticalLayout.addWidget(self.textEdit)


        self.verticalLayout_5.addLayout(self.verticalLayout)

        self.horizontalLayout_6 = QHBoxLayout()
        self.horizontalLayout_6.setObjectName(u"horizontalLayout_6")
        self.pushButton_2 = QPushButton(self.centralwidget)
        self.pushButton_2.setObjectName(u"pushButton_2")

        self.horizontalLayout_6.addWidget(self.pushButton_2)

        self.horizontalSpacer_3 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout_6.addItem(self.horizontalSpacer_3)


        self.verticalLayout_5.addLayout(self.horizontalLayout_6)

        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)

        self.tabWidget.setCurrentIndex(1)


        QMetaObject.connectSlotsByName(MainWindow)
    # setupUi

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(QCoreApplication.translate("MainWindow", u"FJSaint", None))
        self.checkBox_4.setText(QCoreApplication.translate("MainWindow", u"GPS simulator (Based on Speed)", None))
        self.comboBox_8.setItemText(0, QCoreApplication.translate("MainWindow", u"Speed", None))
        self.comboBox_8.setItemText(1, QCoreApplication.translate("MainWindow", u"RPM", None))
        self.comboBox_8.setItemText(2, QCoreApplication.translate("MainWindow", u"Temperature", None))

        self.comboBox_8.setCurrentText(QCoreApplication.translate("MainWindow", u"Speed", None))
        self.comboBox_3.setItemText(0, QCoreApplication.translate("MainWindow", u"Speed", None))
        self.comboBox_3.setItemText(1, QCoreApplication.translate("MainWindow", u"RPM", None))
        self.comboBox_3.setItemText(2, QCoreApplication.translate("MainWindow", u"Temperature", None))

        self.comboBox_3.setCurrentText(QCoreApplication.translate("MainWindow", u"Speed", None))
        self.comboBox_4.setItemText(0, QCoreApplication.translate("MainWindow", u"Speed", None))
        self.comboBox_4.setItemText(1, QCoreApplication.translate("MainWindow", u"RPM", None))
        self.comboBox_4.setItemText(2, QCoreApplication.translate("MainWindow", u"Temperature", None))

        self.comboBox_4.setCurrentText(QCoreApplication.translate("MainWindow", u"Speed", None))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_CAN), QCoreApplication.translate("MainWindow", u"CAN", None))
        self.comboBox_j1939Var1.setItemText(0, QCoreApplication.translate("MainWindow", u"Select Variable", None))
        self.comboBox_j1939Var1.setItemText(1, QCoreApplication.translate("MainWindow", u"RPM", None))
        self.comboBox_j1939Var1.setItemText(2, QCoreApplication.translate("MainWindow", u"Temperature", None))

        self.label_5.setText(QCoreApplication.translate("MainWindow", u"Period (s)", None))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_J1939), QCoreApplication.translate("MainWindow", u"J1939", None))
        self.label_2.setText(QCoreApplication.translate("MainWindow", u"Comming soon", None))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_ISO), QCoreApplication.translate("MainWindow", u"ISO", None))
        self.label.setText(QCoreApplication.translate("MainWindow", u"Saint command", None))
        self.pushButton_3.setText(QCoreApplication.translate("MainWindow", u"Send", None))
        self.label_4.setText(QCoreApplication.translate("MainWindow", u"Regex", None))
        self.pushButton_2.setText(QCoreApplication.translate("MainWindow", u"Reset", None))
    # retranslateUi

