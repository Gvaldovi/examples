import sys
import serial
from PySide6.QtCore import QThread, Signal
from PySide6.QtWidgets import QApplication, QWidget, QVBoxLayout, QTextEdit, QPushButton, QComboBox, QLabel

class SerialReaderThread(QThread):
    # Define a signal to send data from the thread to the main UI
    data_received = Signal(str)

    def __init__(self, port, baudrate):
        super().__init__()
        self.port = port
        self.baudrate = baudrate
        self.serial_connection = None
        self.running = False

    def run(self):
        """Start reading from the serial port."""
        self.serial_connection = serial.Serial(self.port, self.baudrate, timeout=1)
        self.running = True
        while self.running:
            if self.serial_connection.in_waiting > 0:
                data = self.serial_connection.readline().decode('utf-8', errors='ignore').strip()
                if data:
                    self.data_received.emit(data)

    def stop(self):
        """Stop the serial reader."""
        self.running = False
        if self.serial_connection:
            self.serial_connection.close()

class SerialGui(QWidget):
    def __init__(self):
        super().__init__()

        self.setWindowTitle("Serial Data Receiver")
        self.setGeometry(100, 100, 400, 300)

        # Create UI elements
        self.text_edit = QTextEdit(self)
        self.text_edit.setReadOnly(True)

        self.start_button = QPushButton("Start", self)
        self.start_button.clicked.connect(self.toggle_serial)

        self.baud_rate_label = QLabel("Baud rate:", self)
        self.baud_rate_combo = QComboBox(self)
        self.baud_rate_combo.addItems(["9600", "115200", "19200", "38400", "57600", "14400"])

        self.port_label = QLabel("Serial Port:", self)
        self.port_combo = QComboBox(self)
        self.port_combo.addItems(self.list_serial_ports())

        # Layout setup
        layout = QVBoxLayout()
        layout.addWidget(self.port_label)
        layout.addWidget(self.port_combo)
        layout.addWidget(self.baud_rate_label)
        layout.addWidget(self.baud_rate_combo)
        layout.addWidget(self.text_edit)
        layout.addWidget(self.start_button)
        self.setLayout(layout)

        self.serial_thread = None

    def list_serial_ports(self):
        """List available serial ports."""
        ports = []
        if sys.platform.startswith('win'):
            # Windows
            import serial.tools.list_ports
            ports = [port.device for port in serial.tools.list_ports.comports()]
        else:
            # Unix-like (Linux, macOS)
            ports = [f"/dev/ttyUSB{i}" for i in range(10)]
        return ports

    def toggle_serial(self):
        """Start or stop serial communication."""
        if self.serial_thread and self.serial_thread.isRunning():
            self.serial_thread.stop()
            self.serial_thread.wait()
            self.start_button.setText("Start")
        else:
            port = self.port_combo.currentText()
            baudrate = int(self.baud_rate_combo.currentText())
            self.serial_thread = SerialReaderThread(port, baudrate)
            self.serial_thread.data_received.connect(self.display_data)
            self.serial_thread.start()
            self.start_button.setText("Stop")

    def display_data(self, data):
        """Display data in the QTextEdit."""
        self.text_edit.append(data)

if __name__ == "__main__":
    app = QApplication(sys.argv)
    gui = SerialGui()
    gui.show()
    sys.exit(app.exec())
